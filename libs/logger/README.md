# besonders-logger

## Why?
### Simple & powerful syntax
```typescript
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(
    Logger.INFO, 
    { prefix: '[DB]' } // (optional)
);

DEBUG(`test=${1}`, extra, {objects})
LOG(`test=${1}`)
if (VERBOSE.isEnabled) VERBOSE(`won't be run:`, heavyComputation())
WARN(`test=${1}`, extra, {objects})
throw ERROR(`msg logged & returned as Error, plus formatted:`, extra, {objects})
```
### ... while still preserving correct log location
- in devtools, you see the correct line as origin
- in node.js you can print the source location in grey (optional)

### (optional) Concise relative times
![Screenshot](https://cloudflare-ipfs.com/ipfs/bafybeiex3ydtqma7iusc2n3k23fbk6fgrfenzgpncrqjxpkm7avqcu4ccu)
(only with custom devtoolsFormatters enabled)

## Installation

Add `besonders-logger` to a Javascript/Typescript project:
```terminal
pnpm add besonders-logger
yarn add besonders-logger
npm add besonders-logger

# (optional) install vscode snippets:
# Linux script, source: .vscode/install-snippets.sh
pnpm run besonders-install-snippets
# or copy manually from the source repo
```

## Usage

And in any file you want to use logger:

```typescript
// protip: use `logsetup` snippet

import { Logger } from "@ztax/logger/src";

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO); // eslint-disable-line no-unused-vars
```

### Change log-level

```typescript
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG); // eslint-disable-line no-unused-vars
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.WARN); // eslint-disable-line no-unused-vars
//... or any other level
```
### `console.groupCollapsed` [mdn](https://developer.mozilla.org/en-US/docs/Web/API/console/groupCollapsed)
```typescript
LOG.group('init', () => {
	LOG('do something')
	doSomething()
	DEBUG('something done')
})
```
The inner function will always be executed - also when the loglevel would be filtered, in which case there is no group around the inner logs.

### Temporarily override loglevel for a single line
Pro tip: add a pre-commit hook to disallow committing

```typescript
VERBOSE.force("foo") // will be printed in any loglevel
```
### Temporarily log everything
```typescript
window.FORCE_DISABLE_LOGLEVEL = true
```