import { Logger, logriniTest } from './logger'

// const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[bygMW]', performanceEnabled: false }) // eslint-disable-line @typescript-eslint/no-unused-vars

describe('logger', () => {
  it('should fail with logrini != loggeroni', () => {
    expect(logriniTest()).toEqual('loggeroni') // this fails as an illustration of how the test sys functions
  })
  it('should initialize with no options', () => {
    const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG)
    // console.log(WARN, console.warn, WARN === console.warn /* false as [Function: bound warn] !== [Function: warn] */)
    let allFunctions = true
    for (const eachShouldBeFx of [ERROR, WARN, LOG, DEBUG, VERBOSE]) {
      if (typeof eachShouldBeFx !== 'function')
        allFunctions = false
    }
    expect(allFunctions).toEqual(true)
  })
  it('group test', () => {
    const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG) // eslint-disable-line @typescript-eslint/no-unused-vars,unused-imports/no-unused-vars

    LOG.group('init', () => {
      LOG('info')
      DEBUG('debug')
      VERBOSE('verbose')
      if (VERBOSE.isEnabled)
        WARN('should not run')
    })
  })
  it('group test', () => {
    const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO) // eslint-disable-line @typescript-eslint/no-unused-vars,unused-imports/no-unused-vars

    VERBOSE.force('always')
  })
})
