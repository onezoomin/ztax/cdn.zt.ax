import { defineConfig } from 'tsup'

export default defineConfig({
    entry: ['src/*.ts'],
    format: ['cjs', 'esm'],
    platform: 'browser',
    target: ['firefox110', 'chrome110'],
    minify: true,
    // splitting: true,
    sourcemap: true,
    // clean: true, - built concurrently with types
    // noExternal: [/./], // https://github.com/egoist/tsup/issues/819#issuecomment-1479683304
    // outExtension({ format }) {
    //     // HACK to get .min.js extensions - https://tsup.egoist.dev/#output-extension
    //     console.log({format})
    //     return {
    //         js: `.min.js`,
    //     }
    // },
    // bundle: true,
    // skipNodeModulesBundle: false,
    dts: true, //HACK: https://github.com/egoist/tsup/issues/564#issuecomment-1032467124

    // esbuildOptions(options, context) {
    // 	options.define.foo = '"bar"'
    //   },
    esbuildPlugins: [
        // polyfillNode({
        // 	// https://github.com/cyco130/esbuild-plugin-polyfill-node
        // 	globals: {
        // 		buffer: false,
        // 		process: false,
        // 	},
        // 	polyfills: {
        // 		fs: true, // for ipld/car
        // 		crypto: true, // for libp2p stuff
        // 	},
        // }),
    ],
})
