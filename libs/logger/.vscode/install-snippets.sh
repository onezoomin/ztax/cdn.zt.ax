#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
REAL_PATH=$(readlink -f "${BASH_SOURCE[0]}")
REAL_PATH_DIR="$(dirname "$REAL_PATH")"
# echo "REAL_PATH: $REAL_PATH"
# echo "REAL_PATH_DIR: $REAL_PATH_DIR"
# echo "DIR: $DIR"
# echo "dirname: $(dirname "$0")"

mkdir -p .vscode
if [[ ! -f ./.vscode/javascript-ztax-logger.code-snippets ]]; then
    echo "Copying snippets file to .vscode/javascript-ztax-logger.code-snippets"
    cp "$REAL_PATH_DIR/javascript-ztax-logger.code-snippets" ./.vscode/javascript-ztax-logger.code-snippets
else 
    echo "Already exists .vscode/javascript-ztax-logger.code-snippets"
    echo "Compare: diff \"$REAL_PATH_DIR/javascript-ztax-logger.code-snippets\" .vscode/javascript-ztax-logger.code-snippets"
fi
