import { Logger } from '@ztax/logger/src'
import { expect, test } from 'vitest'
import * as mk from './mnemkey'
const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[mkTest]' }) // eslint-disable-line @typescript-eslint/no-unused-vars

test('createRandomSeries', () => {
	const arrayLength = 2
	expect(mk.createRandomSeries(arrayLength, 2048)).toHaveLength(arrayLength)
})
