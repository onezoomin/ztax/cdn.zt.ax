# @ztax/logger

in the root of a typescript project:

```
pnpm add @ztax/logger
pnpm install-snippets
```

in a file you want to use logger

```typescript
// type logsetup to use the snippet
// which will add

const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.INFO); // eslint-disable-line no-unused-vars
```

Logger needs to be imported ( // TODO needs to happen automagically with the snippet or at least with autoimport libs)

```typescript
import { Logger } from "@ztax/logger/src";
```

Change the verbosity in the setup line

```typescript
const { WARN, LOG, DEBUG, VERBOSE, ERROR } = Logger.setup(Logger.DEBUG); // eslint-disable-line no-unused-vars
```
