#!/usr/bin/env bash


mkdir -p .vscode
[[ -f ./.vscode/javascript-ztax-logger.code-snippets/sh ]] || cp "$(dirname "$0")/javascript-ztax-logger.code-snippets" ./.vscode/javascript-ztax-logger.code-snippets
