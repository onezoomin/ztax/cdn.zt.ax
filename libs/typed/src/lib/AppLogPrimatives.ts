import type { Static } from '@sinclair/typebox'
import { Type as t } from '@sinclair/typebox'

// more detailed type checking examples are available here:
// https://github.com/sinclairzx81/typebox/blob/master/example/formats/additional.ts
// and not yet typeboxified regexes:
// https://github.com/OWASP/www-community/blob/master/pages/OWASP_Validation_Regex_Repository.md

export const attributeName = t.String()
export const previousCID = t.String()
export const atomCID = t.String()
export const value = t.String()
export const entityID = t.String()
export const agentID = t.String()
export const hashOfatomCIDarray = t.String()
export const utcTimeStampMS = t.Number()
export const operation = t.Boolean()

export type attributeName = Static<typeof attributeName>
export type previousCID = Static<typeof previousCID>
export type atomCID = Static<typeof atomCID>
export type value = Static<typeof value>
export type entityID = Static<typeof entityID>
export type agentID = Static<typeof agentID>
export type hashOfatomCIDarray = Static<typeof hashOfatomCIDarray>
export type utcTimeStampMS = Static<typeof utcTimeStampMS>
export type operation = Static<typeof operation>
