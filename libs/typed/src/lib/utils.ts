export const keyBy = (array = ([] as Record<string, any>[]), key: string) => (array).reduce((r, x) => ({ ...r, [key ? x[key] : x]: x }), {})
