import { type Static, Type as t } from '@sinclair/typebox'
import { Value } from '@sinclair/typebox/value'
import { keyBy } from './utils'
import { agentID, attributeName, entityID, operation, previousCID, utcTimeStampMS, value } from './AppLogPrimatives'
import type { Immutable } from './typed'
import { CreateDuckClass, CreateGuardedClass, CreateImmutableClass, CreateStrictClass } from './typed'

export const TAtom = t.Object({
  ag: agentID,
  ts: utcTimeStampMS,
  en: entityID,
  at: attributeName,
  vl: value,
  pv: previousCID,
  op: operation,
})
export type TAtom = Static<typeof TAtom>

export const castAbleImperfectAtom = {
  ag: 'agentID',
  ts: '123',
  en: 'entityID',
  at: 'attributeName',
  vl: 'input',
  pv: 'previousCID',
  ignoredOrPassed: 'extra',
  op: false,
}
export const perfectAtom = {
  ag: 'agentID',
  ts: 123,
  en: 'entityID',
  at: 'attributeName',
  vl: 'input',
  pv: 'previousCID',
  op: false,
}

export interface AtomDuck extends TAtom { } // implicitly includes all props from TAtom
export class AtomDuck extends CreateDuckClass<TAtom>(TAtom, 'AtomDuck') { }

export interface AtomStrict extends TAtom { }
export class AtomStrict extends CreateStrictClass<TAtom>(TAtom, 'AtomStrict') { }

export interface AtomGuard extends TAtom { }
export class AtomGuard extends CreateGuardedClass<TAtom>(TAtom, 'AtomGuard') { }

export interface AtomImmutable extends Immutable<TAtom> { }
export class AtomImmutable extends CreateImmutableClass<TAtom>(TAtom, 'AtomImmutable') { }

export interface Atom extends TAtom { }
export class Atom {
  // this is explicitly implementing a constructor with the CheckAndCast pattern
  constructor(newAtomObj: TAtom) {
    if (Value.Check(TAtom, newAtomObj)) {
      Object.assign(this, newAtomObj)
    //   return this
    }
    else {
      const errors = [...Value.Errors(TAtom, newAtomObj)]
      const mappedErrors = keyBy(errors, 'path')
      console.warn('ducktyping with errors:', mappedErrors)
      Object.assign(this, Value.Cast(TAtom, newAtomObj))
    //   return this
    }
  }
}

export const atomToDagAtom = (atom: TAtom) => {
  const strictAtom = new AtomStrict(atom)
  return {
    ...strictAtom,
    cid: 'hashedAtom',
  }
}

export interface AtomUtilMethods extends TAtom { }
export class AtomUtilMethods {
  toDagAtom() {
    return atomToDagAtom(this)
  }
}

