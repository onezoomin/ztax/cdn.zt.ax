module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["@antfu", "plugin:you-dont-need-lodash-underscore/compatible"],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 'latest',
    sourceType: 'module',
    // project: './tsconfig.json',
    extraFileExtensions: ['.vue'],
    tsconfigRootDir: __dirname
  },
  plugins: [
    '@typescript-eslint',
  ],
  rules: {
    "no-console": "off",
    "@typescript-eslint/no-redeclare": "off",
    'comma-dangle': ['error', 'always-multiline'],
    'prefer-template': ['warn'],
    '@typescript-eslint/no-unused-vars': ['warn', {
      varsIgnorePattern: '^_',
      argsIgnorePattern: '^_',
      caughtErrorsIgnorePattern: '^ignore',
    }],
    '@typescript-eslint/no-non-null-assertion': 'warn', // for rIF, as type guards don't work there

    // lenient of bad code
    'operator-linebreak': ['warn', 'before'],
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/brace-style': 'off',
    '@typescript-eslint/no-floating-promises': 'warn',
    '@typescript-eslint/promise-function-async': 'off',
    '@typescript-eslint/strict-boolean-expressions': 'off',
    '@typescript-eslint/restrict-template-expressions': 'warn',
    // https://github.com/typescript-eslint/typescript-eslint/issues/2540#issuecomment-692505191
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': 'warn',
  },
}
