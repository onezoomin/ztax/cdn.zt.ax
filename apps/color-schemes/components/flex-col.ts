import type { Component } from 'hybrids'
const { define, html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface FlexCol extends HTMLElement {
  class: any
}
const classDefaults = ['fcol-g2']
const classDefaultsConnector = (defaultClasses = classDefaults) => ({
  value: '',
  connect: host => {
    host.classList.add('|defaults>|', defaultClasses)
  },
})

export const FlexCol: Component<FlexCol> = { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'flex-col',
  class: classDefaultsConnector(),
}
