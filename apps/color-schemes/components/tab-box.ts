import type { Component } from 'hybrids'

import { Logger } from 'logger'
import { ComponentDefWithClassDefaults, HTMLElementWithClass } from './utils/utils'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html } = globalThis?.hybrids ?? {}

interface TabBox extends HTMLElementWithClass {
  activeTabIndex?: any
}

const activate = (_host, { target }) => {
  document.querySelectorAll('.tab').forEach(tabEl => {
    tabEl.classList.remove('tab-active')
    target.classList.add('tab-active')
  })
}

export const TabBox: Component<TabBox> = ComponentDefWithClassDefaults(['width-min-fit'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'tab-box',
  activeTabIndex: 0,
  content: (_host: TabBox) => {
    // const { activeTabIndex } = host
    // DEBUG('rendering color-row', { accounts, rowID })

    // const nonDeletedColorVMs = accounts.filter(cvm => !cvm.isDeleted)
    // DEBUG('rendering color-row', { nonDeletedColorVMs })
    return html`
    <div class="relative h-70">
      <div class="relative overflow-visible h-0 top-0">
        <input id="tab-info" type="radio" name="tabs" class="modal-toggle peer" checked />
        <div class="invisible peer-checked:visible">
          <span text="right">
            <via-link desc="web-components" to="hybrids" href="https://hybrids.js.org" ></via-link>
            <via-link desc="atomic CSS" to="unocss" href="https://uno.antfu.me/" ></via-link>
            <via-link desc="components" to="daisyui" href="https://daisyui.com/components/" ></via-link>
            <via-link desc="edge deployment" to="fission" href="https://github.com/fission-suite" ></via-link>
            <via-link desc="edge data" to="wnfs" href="https://guide.fission.codes/developers/webnative/file-system-wnfs" ></via-link>
            <via-link desc="fast bundling" to="esbuild" href="https://esbuild.github.io/getting-started/#bundling-for-the-browser" ></via-link>
              <br />
              ~ztax
          </span>
        </div>
      </div>
      
      <div class="relative overflow-visible h-0 top-0">
        <input id="tab-stats" type="radio" name="tabs" class="modal-toggle peer"  />
        <div class="bg-secondary text-secondary-content invisible peer-checked:visible">
          <p>Stats content</p>
        </div>
      </div>
      <div class="relative overflow-visible h-0 top-0">
        <input id="tab-details" type="radio" name="tabs" class="modal-toggle peer" />
        <div class="bg-accent text-accent-content invisible peer-checked:visible">
          <p>Details content</p>
        </div>
      </div>
    </div>

    <div class="w-100% min-w-3/4 tabs p-2 justify-center">
      <div>
        <label for="tab-info" class="text-primary tab tab-bordered tab-active [tab-info:checked]:tab-active" onclick="${activate}">
          Info
        </label>
      </div>
      <div>
        <label for="tab-stats" class="text-secondary tab tab-bordered #foo-bar [tab-stats:checked]:tab-active" onclick="${activate}">
          Stats
        </label>
      </div>
      <div>
        <label for="tab-details" class="text-accent tab tab-bordered [tab-details:checked]:tab-active" onclick="${activate}">
          Details
        </label>
      </div>
    </div>
  ` // .key(`inner-row-${rowID.toString()}`)
  },
})
