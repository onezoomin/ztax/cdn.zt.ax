import type { Component } from 'hybrids'
const { define, html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface ViaLink extends HTMLElement {
  to: string
  href: string
  desc: string
  class: any
}

const classDefaults = ['w-100%']
const classDefaultsConnector = (defaultClassesArray = classDefaults) => ({
  value: defaultClassesArray.join(' '),
  connect: host => host.classList.add(...defaultClassesArray),
})

export const ViaLink: Component<ViaLink> = { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'via-link',
  desc: 'blah',
  to: 'text of link',
  href: '//where.to',

  class: classDefaultsConnector(),
  content: (host: ViaLink) => {
    const { desc, to, href } = host

    // host.classList.add(...classDefaults.split(' '), ...classafter.split(' '))

    return html`<flex-row>
      <div w="2/3" text="right">${desc}</div>
      <div w="1/3" text="left">(via <a class="link-accent" href="${href}" rel="nofollow noopener noreferrer" target="_blank">${to}</a>)</div>
    </flex-row>`
  },
}

if (globalThis.hybrids) {
  define(ViaLink)
}
