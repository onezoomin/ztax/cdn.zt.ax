import { FlexCol } from './flex-col'
import { FlexRow } from './flex-row'
import { ColorPickerFlower } from './color-picker-flower'
import { ColorPickerModal } from './color-picker-modal'
import { SchemeEditor } from './scheme-editor'
import { ColorBox } from './color-box'
import { ColorBoxHistory } from './color-box-history'
import { ColorRow } from './color-row'
import { AsofSlider } from './asof-slider'
import { ViaLink } from './via-link'
import { DataCol } from './data-col'
import { SubscriptionList } from './subscription-list'
import { TabBox } from './tab-box'
import { LayoutBase } from './layout-base'
import { gitlabCorner } from './gitlab-corner'

const { define } = globalThis?.hybrids ?? {}

// const components = [FlexCol, FlexRow, ViaLink, DataCol, LayoutBase, ColorPickerFlower, SubscriptionList, TabBox, ColorBox, ColorRow]
const components = { FlexCol, FlexRow, ViaLink, DataCol, LayoutBase, ColorPickerFlower, ColorPickerModal, SchemeEditor, SubscriptionList, TabBox, ColorBoxHistory, ColorBox, ColorRow, AsofSlider, gitlabCorner }
if (define) {
  for (const eachComponent of Object.values(components)) {
    if (eachComponent.tag?.includes('-')) define(eachComponent)
  }
}

export { components }
