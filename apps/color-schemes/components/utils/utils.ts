import { liveQuery } from 'bygonz/DexiePlus'

import { Logger } from 'logger'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const classDefaults = ['|noDefaultsSet|']
const classDefaultsConnector = (defaultClasses = classDefaults) => ({
  value: '',
  connect: (host) => { host.classList.add('|defaults>|', ...defaultClasses) },
})
export const ComponentDefWithClassDefaults = (defaultClasses: string | string[] = classDefaults, defObject: any) => {
  let splitOnSpaces
  if (typeof defaultClasses === 'string')
    splitOnSpaces = defaultClasses.split(' ')

  else
    splitOnSpaces = defaultClasses.map(eachClassString => eachClassString.split(' ')).flat()

  return {
    class: classDefaultsConnector(splitOnSpaces),
    ...defObject,
  }
}

export interface HTMLElementWithClass extends HTMLElement {
  class: any
}

/**********
 * sets up reactive rendering using dexie liveQuery subscription
 * usage: ...liveQueryFactory<SchemeEditor>('schemeID', 'colors', (val: number) => () => colTable.where('row').equals(val).toArray())
 *
 * @param {string} dynamicKeyName  the key that will be used dynamically by the liveQuery inner function (queryFxFactory)
 * @param {string} resultKeyName the key that will recieve the result - typically an array to be rendered reactively
 * @param {() => () => Promise<any>} queryFxFactory Function that returns a function that does an async query to be subcribed to
 * @returns two keys to be expandede into the host component
 *********/
export function liveQueryFactory<HostType>(dynamicKeyName = 'dynamicKeyUsedInQuery', resultKeyName = 'dataProp', queryFxFactory = val => async () => val) {
  return {
    [dynamicKeyName]: {
      get: (host: HostType, currentVal) => {
        VERBOSE('lq get', { currentVal, queryFx: queryFxFactory })
        if (!currentVal)
          return null
        return liveQuery(queryFxFactory(currentVal)).subscribe({
          next: (data) => { host[resultKeyName] = data },
          error: error => console.error(error),
        })
      },
      set: (host: HostType, value, lastValue) => {
        if (lastValue)
          lastValue.unsubscribe() // lastValue is a subscription if defined
        return value // pass the row ID to the getter
      },
      connect: (host: HostType, key) => () => host[key]?.unsubscribe(), // not sure why we need unsubscribe both here and in set
      observe() {}, // Ensures that the row property is called if the `row` is only set by the static attribute
    },
    [resultKeyName]: undefined,
  }
}

export const useLiveQueryConnction
  = function useLiveQueryConnction<HostType>(fxFactory: (host: HostType) => () => Promise<any>, requiredPredicate = (_host: HostType) => true) {
    return {
      value: undefined,
      connect: async function liveQueryConnection(host: HostType, key: string, invalidate: (force: boolean) => void) {
        if (!requiredPredicate(host))
          DEBUG('liveQueryConnection failed predicate', host[key])

        else
          VERBOSE('liveQueryConnection passed predicate', host[key])

        const generatedFx = fxFactory(host)
        const queryRes = await generatedFx() // make the query once
        const liveQueryObservable = liveQuery(generatedFx)

        const value = host[key]
        DEBUG('connect:', { generatedFx, queryRes, [key]: value, host })

        const subscription = liveQueryObservable.subscribe({
          next: (result) => {
            DEBUG('Got row result:', { [`${key}-->`]: result, before: host[key] }, host)
            host[key] = result
            invalidate(true)
          },
          error: error => console.error(error),
        })

        return () => {
        // clean up
          subscription.unsubscribe()
        }
      },
    }
  }
