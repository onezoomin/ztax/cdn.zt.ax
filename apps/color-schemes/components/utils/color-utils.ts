export const randomHexColor = () => `#${(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0')}`

// Changes the RGB/HEX temporarily to a HSL-Value, modifies that value
// and changes it back to RGB/HEX.

export function changeHue (rgb: string, degree: number) {
  const hsl = hexToHSL(rgb)
  hsl.h += degree
  hsl.h = Math.abs(hsl.h % 360)

  // if (hsl.h > 360) {
  //     hsl.h -= 360;
  // }
  // else if (hsl.h < 0) {
  //     hsl.h += 360;
  // }
  return hslToHex(hsl)
}
export function changeLightness (rgb: string, boost: number) {
  const hsl = hexToHSL(rgb)
  hsl.l = Math.min(1, boost * hsl.l)
  return hslToHex(hsl)
}
export function changeSaturation (rgb: string, boost: number) {
  const hsl = hexToHSL(rgb)
  hsl.s = Math.min(1, boost * hsl.s)
  return hslToHex(hsl)
}

export function hexToRGB (hex = '#CCC') {
  // strip the leading # if it's there
  hex = hex.replace(/^\s*#|\s*$/g, '')

  // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
  if (hex.length === 3) {
    hex = hex.replace(/(.)/g, '$1$1')
  }

  const r = parseInt(hex.substr(0, 2), 16) / 255
  const g = parseInt(hex.substr(2, 2), 16) / 255
  const b = parseInt(hex.substr(4, 2), 16) / 255
  return { r, g, b }
}
// exepcts a string and returns an object
export function hexToHSL (hex) {
  const { r, g, b } = hexToRGB(hex)
  const cMax = Math.max(r, g, b)
  const cMin = Math.min(r, g, b)
  const delta = cMax - cMin
  const l = (cMax + cMin) / 2
  let h = 0
  let s = 0

  if (delta === 0) {
    h = 0
  } else if (cMax === r) {
    h = 60 * (((g - b) / delta) % 6)
  } else if (cMax === g) {
    h = 60 * (((b - r) / delta) + 2)
  } else {
    h = 60 * (((r - g) / delta) + 4)
  }

  if (delta === 0) {
    s = 0
  } else {
    s = (delta / (1 - Math.abs(2 * l - 1)))
  }

  return {
    h,
    s,
    l,
  }
}

// expects an object and returns a string
export function hslToHex (hsl) {
  const { h, s, l } = hsl

  const c = (1 - Math.abs(2 * l - 1)) * s
  const x = c * (1 - Math.abs((h / 60) % 2 - 1))
  const m = l - c / 2
  let r; let g; let b

  if (h < 60) {
    r = c
    g = x
    b = 0
  } else if (h < 120) {
    r = x
    g = c
    b = 0
  } else if (h < 180) {
    r = 0
    g = c
    b = x
  } else if (h < 240) {
    r = 0
    g = x
    b = c
  } else if (h < 300) {
    r = x
    g = 0
    b = c
  } else {
    r = c
    g = 0
    b = x
  }

  r = normalizeRGBval(r, m)
  g = normalizeRGBval(g, m)
  b = normalizeRGBval(b, m)

  return rgbToHex(r, g, b)
}

export function normalizeRGBval (color: number, m: number) {
  color = Math.floor((color + m) * 255)
  if (color < 0) {
    color = 0
  }
  return color
}

export function rgbToHex (r: number, g: number, b: number) {
  return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
}

export function althslToHex ({ h, s, l }) {
  h /= 360
  s /= 100
  l /= 100
  let r, g, b
  if (s === 0) {
    r = g = b = l // achromatic
  } else {
    const hue2rgb = (p, q, t) => {
      if (t < 0) t += 1
      if (t > 1) t -= 1
      if (t < 1 / 6) return p + (q - p) * 6 * t
      if (t < 1 / 2) return q
      if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6
      return p
    }
    const q = l < 0.5 ? l * (1 + s) : l + s - l * s
    const p = 2 * l - q
    r = hue2rgb(p, q, h + 1 / 3)
    g = hue2rgb(p, q, h)
    b = hue2rgb(p, q, h - 1 / 3)
  }
  const toHex = x => {
    const hex = Math.round(x * 255).toString(16)
    return hex.length === 1 ? '0' + hex : hex
  }
  return `#${toHex(r)}${toHex(g)}${toHex(b)}`
}
