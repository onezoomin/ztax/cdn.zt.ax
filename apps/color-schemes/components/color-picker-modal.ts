import type { Component } from 'hybrids'
import { ColorVM } from '../data/Model/Color'
import { ComponentDefWithClassDefaults, HTMLElementWithClass } from './utils/utils'
import { canvas, colorPickerState, renderColorFlower } from './color-picker-flower'
const { html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface ColorPickerModal extends HTMLElementWithClass {
  cvm?: ColorVM
}
export const ColorPickerModal: Component<ColorPickerModal> = ComponentDefWithClassDefaults(['width-min-fit'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'color-picker-modal',
  cvm: undefined, // TODO if a cvm is passed in, show before and current choice side by side

  content: (_host: ColorPickerModal) => {
    // fabricLazy ?? void lazyLoadFabric()
    return html`
      <input wh0! type="checkbox" id="color-modal" class="modal-toggle"  />
      <label for="color-modal" class="modal bg-transparent! cursor-pointer justify-start! m-4"  onmouseover="${() => canvas ?? renderColorFlower()}" >
        <label class="modal-box relative pl-12! pt-8!" for="">
          <label onclick="${() => { colorPickerState.onChoose = null }}" for="color-modal" class="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
          <color-picker-flower></color-picker-flower>
        </label>
      </label>
    `
  },
})
