import type { Component } from 'hybrids'

// expecting these to be imported / initialized elsewhere
// import { FlexCol } from './flex-col';
// import { FlexRow } from './flex-row';

const { html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

interface LayoutBase extends HTMLElement {
  class: any
}

const classDefaults = ['flex', 'flex-row']
const classDefaultsConnector = (defaultClassesArray = classDefaults) => ({
  value: defaultClassesArray.join(' '),
  connect: (host: LayoutBase) => host.classList.add(...defaultClassesArray),
})

export const LayoutBase: Component<LayoutBase> = { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'layout-base',

  class: classDefaultsConnector(),
  content: (_: LayoutBase) => html`
    <flex-row un-cloak bg="grey-600" class="text-black gap-4 w-screen h-screen justify-between m-0 p-4 flex-wrap">
        <flex-col data-theme="night" id="info-tabs"
          b="inset-2 inset-blue-500" 
          class="gap-0 overflow-hidden grow max-w-1/4 min-w-xs p-0 rounded-t-xl mr-4">
            <span bg="blue-500" translate="z-0" class="text-left grow p-2 max-h-10">
               Color Schemes
            </span>
            <flex-col class="gap-2 grow p-2 justify-between">
              
              <tab-box un-cloak class="m-2"></tab-box>
              <subscription-list un-cloak class="m-2"></subscription-list>
              <div id="loadr-extras" class="text-primary text-sm h-10 p-2"></div>

              <asof-slider p="2" m="t-2" />
            </flex-col>

        </flex-col>
        
        <data-col class="grow min-w-1/2 max-w-2/3 max-h-40"></data-col>
    </flex-row>
    <scheme-editor />
  `,
}
