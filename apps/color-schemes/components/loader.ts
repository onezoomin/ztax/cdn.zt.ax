
import loaderCSS from './loader.css' // transformed into BlobURL by esbuild

const loaderCSSblobURL: string = loaderCSS
export const loader = () => {
  document.head.innerHTML += `<link rel="stylesheet" href="${loaderCSSblobURL}" type="text/css"/>`
  document.body.insertAdjacentHTML('afterbegin', '<div id="loadr-i" class="loadr-i">Loading...<div id="loadr-extras" class="loadr-extras">Loading...</div></div>')
}

let activeTimeout
export const loadrMessage = (msg = '', clearAfter = 2000) => {
  (document.querySelector('#loadr-extras') ?? {} as any).innerHTML = msg
  if (activeTimeout) clearTimeout(activeTimeout) // don't clear if another message was set
  activeTimeout = setTimeout(() => {
    (document.querySelector('#loadr-extras') ?? {} as any).innerHTML = ''
  }, clearAfter)
  return msg
}
