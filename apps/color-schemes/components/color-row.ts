import type { Component } from 'hybrids'
import { getInitializedColorsDB } from '../data/dexie'

import { Logger } from 'logger'
import { ComponentDefWithClassDefaults, HTMLElementWithClass, liveQueryFactory, useLiveQueryConnction } from './utils/utils'
import { schemeEditorState, SchemeEditor, SchemeEditorSingletonHost } from './scheme-editor'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html, store } = globalThis?.hybrids ?? {}

const colorsDB = await getInitializedColorsDB()
const colTable = colorsDB.Colors

interface ColorRow extends HTMLElementWithClass {
  rowID: number
  rowName?: string
  colors?: any
}

export const ColorRow: Component<ColorRow> = ComponentDefWithClassDefaults(['h-min', 'w-min'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'color-row',
  rowName: 'The Row of Number:', // : invalidateOnChange('colors', (host) => () => colTable.where('row').equals(host.rowID).toArray(), (host) => !!host.rowID),
  rowID: 0, // : invalidateOnChange('colors', (host) => () => colTable.where('row').equals(host.rowID).toArray(), (host) => !!host.rowID),
  colors: useLiveQueryConnction<ColorRow>((host) => () => colTable.where('row').equals(host.rowID).toArray(), (host) => !!host.rowID),
  // ...liveQueryFactory(colorsDB, 'rowID', 'colors', (host) => colTable.where('row').equals(host.rowID).toArray()),
  content: (host: ColorRow) => {
    const { colors = [], rowID = 1, rowName } = host
    // DEBUG('rendering color-row', { colors, rowID })
    const colorWidth = 8
    const pad = 2
    const nonDeletedColorVMs = colors.filter(cvm => !cvm.isDeleted)
    const widthMap = {
      1: `${pad + colorWidth}`,
      2: `${pad + colorWidth * 2}`,
      3: `${pad + colorWidth}`,
      4: `${pad + colorWidth * 2}`,
      5: `${pad + colorWidth * 5}`,
      6: `${pad + colorWidth * 3}`,
      7: `${pad + colorWidth}`,
      8: `${pad + colorWidth * 2}`,
      9: `${pad + colorWidth * 3}`,
    }
    const dynWidth = widthMap[nonDeletedColorVMs.length]
    // DEBUG('rendering color-row', { nonDeletedColorVMs })
    return html`
      <label for="scheme-modal" display="block" b="inset-1 inset-gray-200" w="${dynWidth}" rounded="md" 
        onclick="${() => {
          console.log('picker', rowID)
          SchemeEditorSingletonHost.schemeID = rowID
          // store.set(schemeEditorState, { schemeID: rowID })
        }}">
        <div class="max-h-min h-min! min-h-min! p-0!">
          <flex-row class="h-min w-full p-2 flex-wrap">${nonDeletedColorVMs.map(eachCVM => {
            return html`<color-box cvm="${eachCVM}"></color-box>`.key(eachCVM.ID)
          })}</flex-row>
        </div>
      </label>
    `.key(`inner-row-${rowID.toString()}`)
  },
})
