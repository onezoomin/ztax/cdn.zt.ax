import type { Component } from 'hybrids'

import { Logger } from 'logger'
import { ComponentDefWithClassDefaults, HTMLElementWithClass } from './utils/utils'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html } = globalThis?.hybrids ?? {} // expects to be initialized after hybrids is globalized
interface DataCol extends HTMLElementWithClass {
  rows?: any
}

// eslint-disable-next-line @typescript-eslint/no-redeclare
export const DataCol: Component<DataCol> = ComponentDefWithClassDefaults(['fcol-g2', 'flex-wrap'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'data-col',
  content: (_host: DataCol) => {
    // const { rows = [] } = host
    // console.log('datacol', { host })
    // host.classList.add(...classDefaults, ...classafter.split(' '))
    const rows = [1, 2, 3, 4, 3, 3, 4, 2, 3, 4, 2, 3, 4]
    return html`
      ${rows.map((eachRowID: number) => {
      DEBUG('rendering content', { eachRowID })
        return html`
        <color-row rowID=${eachRowID}></color-row>
        `.key(eachRowID)
      })}
    `
  },
})
