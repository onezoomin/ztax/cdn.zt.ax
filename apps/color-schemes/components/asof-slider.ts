import type { Component } from 'hybrids/src/template/'
import { getInitializedColorsDB } from '../data/dexie'

import { Logger } from 'logger'
import { ComponentDefWithClassDefaults, liveQueryFactory, useLiveQueryConnction } from './utils/utils'
// import { utcMsTs } from 'bygonz'
import { tsHumanNice } from 'bygonz/WebWorkerImports/Epochs'
import { globalStore } from '../data/globalStore'
import { utcMsTs } from 'bygonz'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html, store } = globalThis?.hybrids ?? {}

const colorsDB = await getInitializedColorsDB()
const appLogTable = (await colorsDB.getAppLogDB()).AppLogs

interface AsofSlider extends HTMLElement {
  asof: number
  globals: any
  class: any
  // allAppLogsAsof?: any
}
const changeAsof = async (_host, { target: { value: newPercentValue } }) => {
  const allAppLogsAsof = await appLogTable.orderBy('ts').toArray()
  const firstTs = allAppLogsAsof[0].ts
  const lastTs = allAppLogsAsof[allAppLogsAsof.length - 1].ts
  const span = (lastTs - firstTs)
  // store.set(globalStore, { asof: newPercentValue === 100 ? 0 : (firstTs + (span * newPercentValue / 100)) }) // for 0-100
  store.set(globalStore, { asof: newPercentValue ? lastTs + (span * newPercentValue) : 0 })
  DEBUG('changeAsof asof', { allAppLogsAsof, newPercentValue })
}
export const AsofSlider: Component<AsofSlider> = ComponentDefWithClassDefaults(['w-full'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'asof-slider',
  globals: store(globalStore),
  asof: ({ globals }) => globals.asof, // : invalidateOnChange('colors', (host) => () => colTable.where('row').equals(host.rowID).toArray(), (host) => !!host.rowID),
  // allAppLogsAsof: useLiveQueryConnction<AsofSlider>((host) => host.asof
  //   ? () => appLogTable.where('ts').belowOrEqual(host.asof).toArray()
  //   : () => appLogTable.orderBy('ts').toArray()),
  // ...liveQueryFactory(colorsDB, 'rowID', 'colors', (host) => colTable.where('row').equals(host.rowID).toArray()),
  content: (host: AsofSlider) => {
    const { asof/* , allAppLogsAsof = [] */ } = host
    // DEBUG('rendering asof-slider', { colors, rowID })

    // const tsOnlyArray = allAppLogsAsof.map(eachLog => eachLog.ts)
    DEBUG('rendering asof', { asof })
    return html`
      <div tabindex="0" class="collapse text-gray-200 rounded-box p-2 pb-1" border="2 gray-200" >
        <input type="range" min="-1" max="0" value="0" class="range range-secondary" step="0.01" oninput="${changeAsof}">
        <span class="mt-1 text-center">${tsHumanNice(asof || utcMsTs())}</span>
        
      </div>
    ` // .key(`inner-row-${rowID.toString()}`)
  },
})
