import type { Component } from 'hybrids'
import type { ColorVM } from './../data/Model/Color'

import { colorPickerState } from './color-picker-flower'

import { Logger } from 'logger'
import { tsHuman } from 'bygonz/WebWorkerImports/Epochs'
import { globalStore } from '../data/globalStore'
import { HTMLElementWithClass } from './utils/utils'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html, store } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported
interface ColorBoxHistory extends HTMLElementWithClass {
  cvm?: ColorVM
  globals: any
  previousColors: any
  asof: number
}

const classDefaults = ['w-min']
const classDefaultsConnector = (defaultClassesArray = classDefaults) => ({
  value: defaultClassesArray.join(' '),
  connect: host => host.classList.add(...defaultClassesArray),
})
const updatePreviousColors = async function getPreviousColors (host) {
  colorPickerState.onChoose = (col) => {
    VERBOSE(col, 'onChoose from box', host.cvm.ID, 'previousColor', host.cvm.color)
    void setColor(host.cvm, col)
  }
  host.previousColors = await host.cvm.getColorHistoryEntries()
}
const setColor = async (cvm, color) => {
  await cvm.setColor(color)
}

export const ColorBoxHistory: Component<ColorBoxHistory> = { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'color-box-history-change',

  globals: store(globalStore),
  asof: {
    get: (host) => {
      return host.globals.asof
    },
    observe: (host, val, prev) => {
      const newValue = host.globals.asof
      if (newValue) { void host.cvm.getEntityAsof<ColorVM>(newValue).then(newCvm => { host.cvm = new ColorVM(newCvm) }) }
      LOG({ cvm: host.cvm, newValue, val, prev })
    },
  },
  cvm: undefined,
  previousColors: undefined,
  class: classDefaultsConnector(),
  content: (host: ColorBoxHistory) => {
    const { cvm, asof, previousColors = [{ color: '#DDD' }, { color: '#111' }] } = host
    LOG({ asof, cvm })
    return html`
    <div class="tooltip tooltip-top" data-tip="${cvm?.color?.toUpperCase()}">
      <div  class="collapse rounded-md rounded-b-0 max-w-min! w-min" onmouseover="${async () => await updatePreviousColors(host)}" tabindex="0" >
        
        <div collapse="title" class="sq-6! rounded p-0! text-xs text-center" style="background-color: ${cvm.color};"></div>
        
        <div class="collapse-content flex flex-col my-2 gap-2 mx-0! p-0! items-start"> 
          ${previousColors.map(({ ts, vl: col, ag, asof = tsHuman(ts ?? 0).replace('_', '\n') }) => {
          return html`
          <div class="tooltip tooltip-right wrapped-tooltip" data-tip="${col}\nby:${ag}\n@${asof}">
            <button onmousedown="${() => setColor(cvm, col)}" class="btn w-3 h-3" style="background-color: ${col};"></button>
          </div>`
          })}
          <label for="color-modal" onclick="${() => console.log('picker', cvm.ID)}" class="btn btn-sm btn-outline btn-primary">pick</label>
          <div onclick="${() => cvm.setDeleted()}" class="btn btn-sm btn-outline btn-error ">delete</div>
        </div>

      </div>
    </div>`.css`
    .wrapped-tooltip:before { 
      margin-left: -2em;
      margin-top: 1em;
      text-align: left;
      white-space: pre-wrap;
      z-index: 10000;
    }`.key(cvm.ID)
  },
}
