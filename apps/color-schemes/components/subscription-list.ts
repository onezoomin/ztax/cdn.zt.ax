import { Subscription } from 'bygonz/WebWorkerImports/Subscriptions'
import type { Component } from 'hybrids'
import { getInitializedColorsDB } from '../data/dexie'
import { appNameObj } from '../data/webnative-common'
import { ComponentDefWithClassDefaults, HTMLElementWithClass, useLiveQueryConnction } from './utils/utils'

import { Logger } from 'logger'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html } = globalThis?.hybrids ?? {}

const colorsDB = await getInitializedColorsDB()
const subTable = await colorsDB.getSubscriptionTable()

interface SubscriptionList extends HTMLElementWithClass {
  accounts?: any
}

const addSubscription = (accountName) => void subTable.add(new Subscription({
  type: 'wnfs',
  info: {
    appCreator: appNameObj.creator,
    appName: appNameObj.name,
    accountName,
  },
}))

const submitSubscription = () => {
  document.getElementById('subsHeading').focus()
  document.getElementById('subsHeading').blur()
  const inputElement = document.getElementById('newUserInput') as HTMLInputElement
  const inputValue = inputElement.value
  if (!inputValue) return LOG('not adding', inputValue, document.getElementById('newUserInput'))
  DEBUG('adding', inputValue, document.getElementById('newUserInput'))
  addSubscription(inputValue)
  inputElement.value = ''
}

const submitOnEnter = (_host: SubscriptionList, kbEv: KeyboardEvent) => { if (kbEv.key === 'Enter') submitSubscription() }

const triggerSync = (_host: SubscriptionList, _evt) => {
  void colorsDB.triggerSubscriptionSync()
}

export const SubscriptionList: Component<SubscriptionList> = ComponentDefWithClassDefaults(['width-min-fit'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'subscription-list',
  accounts: useLiveQueryConnction<SubscriptionList>((_host) => () => subTable.toArray()),
  content: (host: SubscriptionList) => {
    const { accounts = [] } = host
    // DEBUG('rendering color-row', { accounts, rowID })

    // const nonDeletedColorVMs = accounts.filter(cvm => !cvm.isDeleted)
    // DEBUG('rendering color-row', { nonDeletedColorVMs })
    return html`
      <h3 tabindex="1" id="subsHeading" class="text-gray-200 text-center">Subscriptions:</h3>
     
      <flex-row un-cloak class=" p-2 pb-0 text-right border border-2 border-gray-200 rounded-box">
        <div class="dropdown dropdown-right -mt-2 ml-2 self-center">
          <label i="bstrap-plus-circle-dotted" tabindex="0" class="btn btn-accent  h-10! w-10!"></label>
          <div tabindex="0" class="dropdown-content menu -mt-2 p-2 bg-base-100 rounded-box w-52">
            <input id="newUserInput" onkeydown="${submitOnEnter}" type="text" placeholder="type username here" class="input input-bordered input-accent w-full max-w-xs" />
            <button i="bstrap-check-square-fill" class="absolute right-4 mt-2 btn btn-accent btn-sm " onclick="${submitSubscription}"></button>
          </div>
        </div>
        
        <div un-cloak  class="flex-grow">
            ${accounts.map(eachSub => html`
            <div class="gap-2 mb-2">
              ${eachSub.info.accountName}
              <button onclick="${triggerSync}" class="btn btn-xs badge badge-secondary px-1!" >
              ${eachSub.logCount ?? '?'} <label class="ml-1" i="ph-recycle-bold"  />
              </button>
            </div>
            `)}
        </div>
      </flex-row>
    ` // .key(`inner-row-${rowID.toString()}`)
  },
})
