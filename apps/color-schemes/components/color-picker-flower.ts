import type { Component } from 'hybrids'
import type { ColorVM } from '../data/Model/Color'
import { fabric } from 'fabric'
import { althslToHex as hslToHex } from './utils/color-utils'
import { ComponentDefWithClassDefaults, HTMLElementWithClass } from './utils/utils'
const { html } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

export const colorPickerState = {
  onChoose: undefined,
}

interface ColorPickerFlower extends HTMLElementWithClass {
  cvm?: ColorVM
}
export let canvas
const CANVAS_ID = 'color-flower-canvas'
export const renderColorFlower = () => {
  if (canvas) return console.warn('already rendered colorflower')
  // if (!fabric) fabric = await lazyLoadFabric()

  canvas = new fabric.Canvas(CANVAS_ID, {
    selection: false,
  })
  console.log('rendered colorflower', canvas)

  fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center'
  fabric.Object.prototype.objectCaching = false
  canvas.setBackgroundColor(undefined)

  let x; let y; let l; let r
  const c = 200

  makeColorCircle(c, c, 0, 0, 0, c)
  makeColorCircle(c, c, 0, 0, 100, c / 2)

  let ang = -68.75

  const grain = 3
  const sz = 6 + grain * 1.8
  const factor = 78
  for (let n = 0; n < 410; n += grain) {
    r = Math.sqrt(n * factor)
    x = c + r * Math.cos(ang * 0.0174532925)
    y = c + r * Math.sin(ang * 0.0174532925)
    l = Math.max(0, 100 - r / 2)

    makeColorCircle(x, y, ang % 360, 38.2, l, sz / 0.618)
    makeColorCircle(x, y, ang % 360, 61.8, l, sz)

    makeColorCircle(x, y, ang % 360, 100, l, sz * 0.618 * 0.618)
    ang += 137.5
  }
  // console.log(canvas.toSVG())

  const textbox = new fabric.IText('#FFFFFF', {
    left: 200,
    top: 460,
    width: 400,
    textAlign: 'left',
    fontFamily: 'Impact',
    fontSize: 24,
    radius: 230,
    rotate: 0,
    selectable: true,
    hasControls: false,
    effect: 'STRAIGHT',
    charSpacing: 10,
    spacing: 40,
  })
  canvas.add(textbox)

  function makeColorCircle (left, top, h, s, l, rad) {
    const r = rad || 8
    const hx = hslToHex({ h, s, l })
    const c = new fabric.Circle({
      left,
      top,
      strokeWidth: 0,
      radius: r,
      fill: hx,
    })
    c.selectable = false
    canvas.add(c)
    c.on('mouseup', function (e) {
      const obj = e.target
      // console.log(obj.fill, onChooseStore)
      textbox.exitEditing()
      textbox.set('text', obj.fill.toString())
      textbox.enterEditing()
      textbox.selectAll()

      colorPickerState.onChoose(obj.fill)
    })
    return c
  }

  canvas.renderAll()
}

export const ColorPickerFlower: Component<ColorPickerFlower> = ComponentDefWithClassDefaults(['width-min-fit'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'color-picker-flower',
  cvm: undefined, // TODO if a cvm is passed in, show before and current choice side by side

  content: (_host: ColorPickerFlower) => {
    // fabricLazy ?? void lazyLoadFabric()
    return html`<canvas id="${CANVAS_ID}" height="500" width="400"></canvas>`
  },
})
