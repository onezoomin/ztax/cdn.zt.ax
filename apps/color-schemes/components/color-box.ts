import type { Component } from 'hybrids'

import { Logger } from 'logger'
// import { tsHuman } from 'bygonz/WebWorkerImports/Epochs'
import { globalStore } from '../data/globalStore'
import { colorPickerState } from './color-picker-flower'
import { ColorVM } from './../data/Model/Color'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html, store } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported
interface ColorBox extends HTMLElement {
  cvm?: ColorVM
  globals: any
  previousColors: any
  asof: number
  class: any
}

const classDefaults = ['w-min']
const classDefaultsConnector = (defaultClassesArray = classDefaults) => ({
  value: defaultClassesArray.join(' '),
  connect: host => host.classList.add(...defaultClassesArray),
})
const updatePreviousColors = async function getPreviousColors(host) {
  colorPickerState.onChoose = (col) => {
    VERBOSE(col, 'onChoose from box', host.cvm.ID, 'previousColor', host.cvm.color)
    void setColor(host.cvm, col)
  }
  host.previousColors = await host.cvm.getColorHistoryEntries()
}
const setColor = async (cvm, color) => {
  await cvm.setColor(color)
}

export const ColorBox: Component<ColorBox> = { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'color-box',

  globals: store(globalStore),
  asof: {
    get: (host) => {
      return host.globals.asof
    },
    observe: (host, val, prev) => {
      const newValue = host.globals.asof
      if (newValue)
        void host?.cvm?.getEntityAsof<ColorVM>(newValue).then((newCvm) => { host.cvm = new ColorVM(newCvm) })
      VERBOSE({ cvm: host.cvm, newValue, val, prev })
    },
  },
  cvm: undefined,
  previousColors: undefined,
  class: classDefaultsConnector(),
  content: (host: ColorBox) => {
    const { cvm, asof, previousColors = [{ color: '#DDD' }, { color: '#111' }] } = host
    VERBOSE({ asof, cvm, previousColors })
    return html`<div class="w-6! h-6! min-h-6! min-w-6! rounded p-0! text-xs text-center" style="background-color: ${cvm.color};"></div>`.key(cvm.ID)
  },
}
