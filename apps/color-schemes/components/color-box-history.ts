import type { Component } from 'hybrids'
import { ColorVM } from './../data/Model/Color'

import { colorPickerState } from './color-picker-flower'

import { Logger } from 'logger'
import { tsHuman } from 'bygonz/WebWorkerImports/Epochs'
import { globalStore } from '../data/globalStore'
import { ComponentDefWithClassDefaults, HTMLElementWithClass } from './utils/utils'
import { setActiveColorID } from './scheme-editor'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html, store } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported
interface ColorBoxHistory extends HTMLElementWithClass {
  cvm?: ColorVM
  globals: any
  previousColors: any
  asof: number
  active: boolean
}

const updatePreviousColors = async function getPreviousColors (host: ColorBoxHistory) {
  colorPickerState.onChoose = (col) => {
    VERBOSE(col, 'onChoose from box', host.cvm.ID, 'previousColor', host.cvm.color)
    void setColor(host.cvm, col)
  }
  setActiveColorID(host.cvm.ID)
  host.previousColors = await host.cvm.getColorHistoryEntries()
}
const setColor = async (cvm, color) => {
  await cvm.setColor(color)
}

export const ColorBoxHistory: Component<ColorBoxHistory> = ComponentDefWithClassDefaults(['w-min'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'color-box-history',
  active: false,
  globals: store(globalStore),
  asof: {
    get: (host: ColorBoxHistory) => {
      return host.globals.asof
    },
    observe: (host: ColorBoxHistory, val, prev) => {
      const newValue = host.globals.asof
      if (newValue) { void host.cvm.getEntityAsof<ColorVM>(newValue).then(newCvm => { host.cvm = new ColorVM(newCvm) }) }
      VERBOSE({ cvm: host.cvm, newValue, val, prev })
    },
  },
  cvm: undefined,
  previousColors: undefined,
  content: (host: ColorBoxHistory) => {
    const { cvm, asof, active, previousColors = [{ color: '#DDD' }, { color: '#111' }] } = host
    VERBOSE({ asof, cvm })

    return html`
    <div class="dropdown dropdown-top mt-12 ml-2 self-center" onmouseover="${async () => await updatePreviousColors(host)}">
      <label tabindex="0" class="sq-8! inline-block rounded p-0! text-xs text-center${active ? ' glow-4 shadow-amber-200' : ''}" style="background-color: ${cvm.color};"></label>
      <div tabindex="0" class="dropdown-content menu -mt-2 p-2 bg-base-100 rounded-box w-52">
        <flex-row class=" my-2mx-0! p-0! items-start"> 
          ${previousColors.map(({ ts, vl: col, ag, asof = tsHuman(ts ?? 0).replace('_', '\n') }) => {
            const colorStyle = { backgroundColor: col } // style="background-color: ${col as string};"
            return html`
            <div class="tooltip tooltip-top wrapped-tooltip" data-tip="${col}\nby:${ag}\n@${asof}">
              <button onmousedown="${() => setColor(cvm, col)}" class="btn sq-6!" style="${colorStyle}" ></button>
            </div>` // .style`background-color: ${col as string}`
          })}
          <!-- <label for="color-modal" onclick="${() => console.log('picker', cvm.ID)}" class="btn btn-sm btn-outline btn-primary">pick</label> -->
          <div onclick="${() => cvm.setDeleted()}" class="btn btn-sm btn-outline btn-error ">delete</div>
        </flex-row>
      </div>
    </div>`.css`
    .wrapped-tooltip:before { 
      margin-left: -2em;
      margin-top: 1em;
      text-align: left;
      white-space: pre-wrap;
      z-index: 10000;
    }`.key(cvm.ID)
  },
})
