import type { Component } from 'hybrids'

import { getInitializedColorsDB } from '../data/dexie'
import { canvas, colorPickerState, renderColorFlower } from './color-picker-flower'
import { ComponentDefWithClassDefaults, HTMLElementWithClass, liveQueryFactory } from './utils/utils'

import { Logger } from 'logger'
import { Color } from '../data/Model/Color'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const { html, store } = globalThis?.hybrids ?? {} // import { define,html } from 'hybrids'; // will be async imported

export const schemeEditorState = {
  activeColorID: '',
}
export const setActiveColorID = (colID: string) => store.set(schemeEditorState, { activeColorID: colID })

const colorsDB = await getInitializedColorsDB()
const colTable = colorsDB.Colors

interface SchemeEditor extends HTMLElementWithClass {
  colors?: any
  activeColorID?: string
  schemeID?: number
}

const addColorToScheme = async (schemeID) => {
  const colorsDB = await getInitializedColorsDB()
  const colTable = colorsDB.Colors
  void colTable.add(new Color({ row: schemeID, color: '#DEF' }))
}
export let SchemeEditorSingletonHost: SchemeEditor
export const SchemeEditor: Component<SchemeEditor> = ComponentDefWithClassDefaults(['width-min-fit'], { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'scheme-editor',
  ...liveQueryFactory<SchemeEditor>('schemeID', 'colors', (val: number) => () => colTable.where('row').equals(val).toArray()),
  activeColorID: () => store.get(schemeEditorState).activeColorID,
  content: (host: SchemeEditor) => {
    // fabricLazy ?? void lazyLoadFabric()
    SchemeEditorSingletonHost = host
    const { colors = [], schemeID, activeColorID } = host
    const nonDeletedColorVMs = colors.filter(cvm => !cvm.isDeleted)
    const hackSchemeID = nonDeletedColorVMs[0]?.row // the getter for schemeID
    LOG({ schemeID, nonDeletedColorVMs })
    return html`
      <input wh0! type="checkbox" id="scheme-modal" class="modal-toggle"  />
      <label for="scheme-modal" class="modal cursor-pointer m-4"  onmouseover="${() => canvas ?? renderColorFlower()}" >
        <label class="modal-box" for="">
          <label onclick="${() => { colorPickerState.onChoose = null }}" for="scheme-modal" class="btn btn-sm btn-circle absolute right-2 top-2">✕</label>
          ${schemeID && html`<color-picker-flower />`}
          <flex-row class="max-w-3/4 min-h-10">${nonDeletedColorVMs.map(eachCVM => {
            return html`<color-box-history active="${!!(eachCVM.ID === activeColorID)}" cvm="${eachCVM}"></color-box-history>`.key(eachCVM.ID)
          })}
          <div i="bstrap-plus-circle-dotted" tabindex="0" class="btn btn-accent  h-6! w-6!" onclick="${() => addColorToScheme(hackSchemeID)}"></div>
          </flex-row>
        </label>
      </label>
    `
  },
})
