import type { Component } from 'hybrids'

const classDefaults = ['frow-g2']
const classDefaultsConnector = (defaultClasses = classDefaults) => ({
  value: ['|defaults>|', ...defaultClasses].join(' '),
  connect: host => { host.classList.add('|defaults>|', defaultClasses) },
})

interface FlexRow extends HTMLElement {
  class: any
}
export const FlexRow: Component<FlexRow> = { // eslint-disable-line @typescript-eslint/no-redeclare
  tag: 'flex-row',
  class: classDefaultsConnector(),
}
