/* eslint-disable @typescript-eslint/restrict-template-expressions */

import { Logger } from 'logger'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const attributifyOptions = {
  /* options  https://github.com/unocss/unocss/tree/main/packages/preset-attributify */
  // prefix: 'un-',
}

const presetIconsConfig = {
  cdn: 'https://esm.sh/',
  collections: {
    custom: {
      circle: '<svg viewBox="0 0 120 120"><circle cx="60" cy="60" r="50"></circle></svg>',
      /* ... */
    },
    bstrap: async (iconName) => {
      // all bootstrap icons available via unoicons
      // search https://icons.getbootstrap.com/#icons or https://icon-sets.iconify.design/bi/
      // use like so: <label tabindex="0" class="i-bstrap-plus-circle-dotted btn btn-accent btn-circle p-1  text-2xl" />
      return await fetch(`https://icons.getbootstrap.com/assets/icons/${iconName}.svg`).then(res => res.text())
    },
  },
}

const presetColorSchemes = (configEntriesToMerge) => ({
  name: 'color-schemes-custom',
  safelist: 'mb-2 m-2 w-min w-6 h-6 min-w-6 min-h-6'.split(' '),
  rules: [
    [/^glow-(\d)?$/, ([, s = 6]: [any, string]) => ({
      '--un-shadow': `var(--un-shadow-inset) 0 0 ${s * 2}px ${s}px var(--un-shadow-color, rgba(0,0,0,0.1))`,
      'box-shadow': 'var(--un-ring-offset-shadow), var(--un-ring-shadow), var(--un-shadow);',
    })],
    [/^b-inset-(\d)$/, ([, w]: [any, string]) => ({ 'box-shadow': `inset 0 0 0 ${w}px var(--un-shadow-color)` })],
    // [/^foo-(.*)$/, ([, rule]: [any, string, string, string]) => `#foo {--at-apply: ${rule};}`],
    // [/^#foo-(.*)$/, ([, rule]: [any, string, string, string]) => '#foo {color: #BA5;}'],
    [/^\[(.*):(.*)\]:(.*)$/, ([, id, state, rule]: [any, string, string, string], { _rawSelector, _currentSelector, variantHandlers, _theme }) => {
      const retrule = `#${id}:${state} ~ * label[for=${id}] { --at-apply: ${rule}; background-color: #33d; }
      `
      // console.log({ id, state, rule, retrule })
      // discard mismatched rules
      // if (name.includes('something')) { return }

      // this works
      // label[for=tab-stats] {
      //     background-color: aqua;
      // }

      // if you want, you can disable the variants for this rule
      if (variantHandlers.length) { return }
      // return a string instead of an object
      return retrule
    }],

  ],
  shortcuts: [
    [/^b-inset-([^\d]*-[\d]{3})$/, ([, c]: [any, string]) => `shadow-${c}`],
    [/^sq-([\d]+)$/, ([, sz]: [any, string]) => `w-${sz} h-${sz} min-w-${sz} min-h-${sz}`],
    [/^#tab-(.*):(.*)$/, ([, _tab, rule]: [any, string, string]) => `${rule}`],

    {
      'frow-g2': 'flex flex-row justify-between gap-2',
      'fcol-g2': 'flex flex-col justify-start gap-2',
      mt2: 'mt-2',
      mb2: 'mb-2',
      wh0: 'w-0 h-0 max-w-0 max-h-0 min-w-0 min-h-0',
      // btn: 'py-2 px-4 font-semibold rounded-lg shadow-md',
    },
    // dynamic shortcuts:

  ],
  ...configEntriesToMerge, // be aware, if any of the above are included they will be overidden
})

let runOnce
const onMount = async ({ packages: packageObject }) => {
  if (runOnce) return console.warn('already runOnce')
  runOnce = true

  const { initColorSchemeData } = await import('./data/data')
  await initColorSchemeData() //  this will first initWebnative quickly without the fs to get the fission username

  // get expected packages from the bundle
  const { unocss, hybrids } = packageObject
  globalThis.hybrids = hybrids // ensure hybrids is on global so that the components will find it
  const { initUnocssRuntime, presetMini, presetUno, presetDaisy, presetAttributify, injectTailwindCSSreset, injectDaisyUnoCSS, injectDaisyCDNCSS, presetIcons, transformerDirective } = unocss
  injectTailwindCSSreset()

  const compImports = await import('./components')
  const components = { ...compImports.components }
  const { gitlabCorner } = components
  DEBUG('imported components', components)

  const configEntriesToMerge = {
    transformers: [
      transformerDirective(),
    ],
  }

  const unocssConfig = {
    defaults: {
      presets: [
        presetUno(),
        presetDaisy(),
        presetAttributify(attributifyOptions),
        presetColorSchemes(configEntriesToMerge),
        presetIcons(presetIconsConfig),
      ],
    },
  }
  // __unocss_runtime.uno.setConfig(unocssConfig) // this is needed if you don't wrap the RuntimeOptions in a defaults prop https://github.com/unocss/unocss/blob/main/packages/runtime/src/index.ts#L90

  injectDaisyCDNCSS()
  initUnocssRuntime(unocssConfig)

  setTimeout(() => {
    const loadrToRemove = document.querySelector('#loadr-i')
    loadrToRemove && document.body.removeChild(loadrToRemove)

    document.body.insertAdjacentHTML('beforeend', '<layout-base un-cloak > </layout-base>')
    document.body.insertAdjacentHTML('beforeend', gitlabCorner('https://gitlab.com/onezoomin/ztax/ztax/-/tree/trunk/apps/color-schemes'))

    // if (!wnfs) { // maybe already init from initColorSchemeData
    //   void (async () => {
    //     const p = performance.now()
    //     LOG('initializing wn after app mount')
    //     await initWebnative()
    //     LOG(`took ${(performance.now() - p).toFixed(1)}ms to initialize as`, getUsername())
    //   })()
    // }
  }, 250)
}

export { onMount }
