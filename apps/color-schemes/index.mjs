import _ from './polyfill'
import { onMount } from './app'
import { updateInterval } from './data/data'
import { loader } from './components/loader'
export { onMount, loader, updateInterval }
