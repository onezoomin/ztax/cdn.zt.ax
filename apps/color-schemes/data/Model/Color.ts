import { Mixin } from 'ts-mixer'
import { WithHashID, WithHistory, ModWho, TimeStampedBase } from 'bygonz'
import { hexToHSL } from '../../components/utils/color-utils'

export type CompoundKeyNumStr = [number, string]

// ColorParams can be used for a js obj that will be cast to Color or ColorVM
export interface ColorParams {
  ID?: string

  creator?: string
  created?: number

  modifier?: string
  modified?: number

  isDeleted?: boolean
  row?: number
  color: string
}

export class Color extends Mixin(TimeStampedBase, ModWho, WithHashID) {
  color: string
  isDeleted?: boolean
  row?: number

  constructor (obj: ColorParams) {
    super(obj) // all Mixedin supers are called in left to right order
    Object.assign(this, obj)
  }
}

export class ColorVM extends Mixin(Color, WithHistory) {
  public get hsl () {
    return hexToHSL(this.color)
  }

  async getColorHistoryEntries () {
    return (await this.getEntityHistory()).filter((eachLog) => eachLog.at === 'color')
  }

  async getColorHistoryValues () {
    return Object.values(await this.getAttributeHistory('color'))
  }

  async setColor (color) {
    console.log('set', this.ID, color)
    const { getInitializedColorsDB } = await import('../dexie')
    const colorsDB = await getInitializedColorsDB()
    void colorsDB.Colors.update(this.ID, { color })
  }

  async setDeleted () {
    console.log('del', this.ID)
    const { getInitializedColorsDB } = await import('../dexie')
    const colorsDB = await getInitializedColorsDB()
    void colorsDB.Colors.update(this.ID, { color: '-deleted-', isDeleted: true })
  }

  public get hslCss () {
    const { h, s, l, hr = h.toFixed(1), sr = s.toFixed(1), lr = l.toFixed(1) } = hexToHSL(this.color)
    return `hsl(${hr},${sr},${lr})`
  }
}

export const initialColors = [
  new Color({ row: 0, color: '#FFFFFF', creator: '0xB14ckne55' }),
]
