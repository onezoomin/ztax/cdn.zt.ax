import { BygonzDexie, Publication, Subscription } from 'bygonz'
import type { DexiePlusParams, Table } from 'bygonz'
import { defaultOptions } from 'bygonz/DexiePlus'

import type { ColorParams } from './Model/Color'
import { ColorVM } from './Model/Color'
import { appNameObj } from './webnative-common'

const activeAgent = 'defaultFixThisSoItNeverShowsUpAnywhere'

const stores = {
  Colors: 'ID, color, row, created, modified, owner, modifier',
  // Subscriptions: 'id++, account',
  // Schemes: 'ID, name, created, modified, owner, modifier',
}

const mappings = {
  Colors: ColorVM,
  // Schemes: SchemeVM,
}
// const maybeCid = false
// const subscriptions: Subscription[] = [
//   new Subscription({
//     type: 'ipfs',
//     info: {
//       // appCreator: appNameObj.creator,
//       // appName: appNameObj.name,
//       accountName: 'bygonzcolors',
//       // logId: 'testlog-one',
//       // ipns: 'bafzaajaiaejcbsu2yiourqrqvwbj6mqix3zn7uiepo2g3dh5ugnan5dmcmsr5exz',
//       // ipns: '/record/L2lwbnMvACQIARIgidd7fVrKmjvZnBDq1b7w_c9miOEzfeV8r-eMUZGc2nU',
//       ipns: `/record/${maybeCid}`,
//     }
//   }),
// ]
const publications: Publication[] = [
  new Publication({
    type: 'ipfs',
    info: {
      // appCreator: appNameObj.creator,
      // appName: appNameObj.name,
      accountName: 'bygonzcolors',
      // logId: 'testlog-one',
      query: '*',
    },
  }),
]
const options = {
  ...defaultOptions,
  activeAgent,
  // subscriptions,
  publications,
}

export class ColorsDB extends BygonzDexie {
  // Declare implicit table properties. (just to inform Typescript. Instanciated by Dexie in stores() method)
  // {entity}Params | {entity}VM allows for partial objects to be used in add and put and for the class of retrieved entities to include getters
  public Colors: Table<ColorParams | ColorVM, string>
  // public Subscriptions: Table<{id?: number, info: {accountName: string}, fileCount?: number}, number>
  // public Schemes: Table<TaskParams | TaskVM, CompoundKeyNumStr>

  async init(activeAgent) {
    if (self.document !== undefined)
      await this.setup(activeAgent) // in super
  }

  constructor(...params: DexiePlusParams) {
    super(...params)
    this.doMappings()
    // super(params[0]) // reactivity works if extending Dexie (not loaded from CDN) and using these normal instantiations
    // this.version(1).stores(stores)
  }
}

let colorsDB: ColorsDB
export const getInitializedColorsDB = async (activeAgent: string) => {
  if (colorsDB)
    return colorsDB
  colorsDB = new ColorsDB(appNameObj.name, stores, mappings, { ...options, activeAgent })
  await colorsDB.init(activeAgent)
  return colorsDB
}
