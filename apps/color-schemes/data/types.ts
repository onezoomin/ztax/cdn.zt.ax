import { ArrayModel, Model, ObjectModel } from 'objectmodel'

const TEXT = Model(String)
TEXT.prototype.sqlType = 'TEXT'
const INT = Model(Number).assert(Number.isInteger)
INT.prototype.sqlType = 'INT'

const ts = INT.extend().as('ts') as unknown as number
const un = TEXT.extend().as('un') as unknown as string
const en = TEXT.extend().as('en') as unknown as string
const at = TEXT.extend().as('at') as unknown as string
const vl = TEXT.extend().as('vl') as unknown as string

const TxRecordOMarr = [ts, un, en, at, vl]
const TxRecordOM = ArrayModel(TxRecordOMarr)
  .assert(arr => arr.length === TxRecordOMarr.length) as unknown as [typeof ts, typeof un, typeof en, typeof at, typeof vl]

const fieldNamesDB = ['ts', 'un', 'en', 'at', 'vl']
const fieldNames = ['timeStamp', 'userName', 'entityID', 'attributeName', 'value']
// const TxDBFieldNames = ArrayModel([String]).default(fieldNamesDB)
// const TxFieldNames = ArrayModel([String]).default(fieldNames)

export interface TxVM { timeStamp: typeof ts, userName: typeof un, entityID: typeof en, attributeName: typeof at, value: typeof vl }

export class TxVM
  extends ObjectModel({ values: TxRecordOM }) {
  get fieldnames () { return fieldNames }
  get fieldNamesDB () { return fieldNamesDB }

  constructor (args: typeof TxRecordOM) {
    super({ values: args })
    console.log('construcing', this)

    for (const eachField of fieldNames) {
      Object.defineProperty(this, eachField, {
        enumerable: true,
        get: function () {
          return this.values[fieldNames.indexOf(eachField)]
        },
      })
    }
  }
}
const tx1 = new TxVM([Date.now(), 'gotjosh', '01-02', ':createdOn', Date.now().toFixed(0)])

// TODO explore using TxRecordOMarr{name,sqlType} instead of fieldNamesDB
const tableDef = (tableName: string, fields = fieldNamesDB, extras = ['PRIMARY KEY(ts,un,en,at)']) => (`
CREATE TABLE IF NOT EXISTS ${tableName}(
  ${fields.join(',\n')}
  ${extras.length && ',\n'}
  ${extras.join(',\n')}
);`)
console.log({ TxVM, TxRecordOMarr, tx1, tableDef: tableDef('txs') })
