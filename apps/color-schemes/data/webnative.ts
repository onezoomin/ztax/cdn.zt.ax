import type Twn from 'webnative'

import { loadrMessage } from './../components/loader'

import { allPromises } from 'bygonz/WebWorkerImports/Utils'

import { Logger } from 'logger'
import { appNameObj } from './webnative-common'

const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars
export let wn: typeof Twn, saturateAppLogsAndColorsFromAllPublicUsers, saturateAppLogsFromAllPublicUsers, publicUsers: string[],
  pathString, apDir, colorsDirPub, pubAppDir, pubAppArray, linksDirPub, lsResults, colorsLS, state, username, p, isInitRunning, wnfs

export const getUsername = () => username ?? 'default_wnative'

// const webnativeCDNURL = 'https://ztax-cdn.fission.app/webnative/index.min.mjs'
const webnativeCDNURL = 'https://unpkg.com/webnative@0.34.2/dist/index.js'

export const initWebnative = async () => {
  if (isInitRunning) return WARN('initWebnative already running - hold your horses')
  isInitRunning = true
  wn = globalThis.webnative
  if (!wn) {
    const importRes = await import(webnativeCDNURL/*! @vite-ignore */)
    wn = importRes?.webnative
      ? importRes.webnative
      // @ts-expect-error
      : importRes?.initialize ? importRes : self.webnative // flexible module finding for various esm & umd publishing styles
    if (!wn) {
      return WARN('missing wn', importRes)
    }
  }
  DEBUG(loadrMessage('Initializing webnative...', 30000))

  pubAppArray = ['public', appNameObj.creator, appNameObj.name]
  pubAppDir = wn.path.directory(...pubAppArray)
  colorsDirPub = wn.path.directory(...pubAppArray, 'Colors')
  linksDirPub = wn.path.directory(...pubAppArray, 'links')

  // const publicDirectoryArray = [wn.path.root(), wn.path.directory(appNameObj.creator, appNameObj.name), wn.path.directory(appNameObj.creator, appNameObj.name, 'links')]
  // the above line adds zero functionality and delays initialization by 30-60sec (likely due to complex DAG tree in the specific directories)
  const publicDirectoryArray = [wn.path.root()]

  const permissions = {
    app: appNameObj,
    fs: { // Ask the user permission for additional filesystem paths
      public: publicDirectoryArray,
    },
  }

  DEBUG('calling wn.initialise after setting up directory info', 'without loadFileSystem so its faster')
  state = await wn.initialise({ // Will use the lobby to ask the user for permission to store app data in eg `private/Apps/onezoomin/color-schemes`
    permissions,
    loadFileSystem: false,
  })
  DEBUG(loadrMessage(`initialized as ${state?.username as string}`), { state })

  switch (state.scenario) {
    case wn.Scenario.AuthCancelled: // User was redirected to lobby, but cancelled the authorisation
      break

    case wn.Scenario.AuthSucceeded:
    case wn.Scenario.Continuation: // ☞ We can now interact with our file system
      // State:
      // state.authenticated    -  Will always be `true` in these scenarios
      // state.newUser          -  If the user is new to Fission
      // state.throughLobby     -  If the user authenticated through the lobby, or just came back.
      // state.username         -  The user's username.

      username = state.username
      globalThis.ucan = wn.ucan

      break

    case wn.Scenario.NotAuthorised:
      // await here is important as we want to delay app logic until we have username from webnative
      await wn.redirectToLobby(state.permissions)
      break
  } // </ switch on state scenarios: lobby if needed and then populate wnfs and username

  p = performance.now()

  const { getInitializedColorsDB } = await import('./dexie')
  const ColorsDB = await getInitializedColorsDB(username)
  const ColorsTableNonBygonz = (ColorsDB.nonBygonzDB as typeof ColorsDB).Colors
  const colorDBcount = await ColorsTableNonBygonz.count()
  const isColorDBempty = (colorDBcount === 0)

  void ColorsDB.sendWebnativePerms(state.permissions, username)

  const allSubscriptions = await ColorsDB.getSubscriptionArray()

  DEBUG('ColorDB', { colorDBcount, allSubscriptions })

  const saturateColorsViaLocalAppLogs = async () => {
    DEBUG('getEntitiesAsOf')
    const entitiesResult = await ColorsDB.getEntitiesAsOf()
    const mappedColorObjs = entitiesResult.entitiesGroupedByTablename.Colors
    DEBUG('reduced in bygonz', { entitiesResult, mappedColorObjs })

    // using nonBygonzDB for initialising from other user's appLog
    if (await ColorsTableNonBygonz.count() === 0) {
      LOG('putting', mappedColorObjs.length)
      await ColorsTableNonBygonz.bulkPut(mappedColorObjs)
      LOG('put', mappedColorObjs)
    } else { // </ only do a put if we don't have any colors
      const promisesForAll = []
      LOG('add or update', mappedColorObjs.length)
      const updates = []
      const adds = []
      for (const eachMappedColorObj of mappedColorObjs) {
        const existingObj = await ColorsTableNonBygonz.get(eachMappedColorObj.ID)
        if (existingObj) {
          updates.push(eachMappedColorObj)
          promisesForAll.push(ColorsTableNonBygonz.update(eachMappedColorObj.ID, eachMappedColorObj))
        } else {
          adds.push(eachMappedColorObj)
          promisesForAll.push(ColorsTableNonBygonz.add(eachMappedColorObj))
        }
      }
      const resArray = await allPromises(promisesForAll)
      LOG('add or update', { resArray, updates, adds, mappedColorObjs })
    } // </ otherwise update existing and add new
  }

  saturateAppLogsAndColorsFromAllPublicUsers = async () => {
    // const totalAdded = await saturateAppLogsFromAllPublicUsers()
    const colorsDB = await getInitializedColorsDB()
    const totalAdded = await colorsDB.triggerSubscriptionSync()
    totalAdded && await saturateColorsViaLocalAppLogs()
  }

  if (isColorDBempty) {
    LOG(loadrMessage('waiting for saturation as ColorsDB is empty', 30000))
    await saturateAppLogsAndColorsFromAllPublicUsers()
  } else {
    LOG(loadrMessage('continuing with delayed sync as ColorsDB has colors', 15000), colorDBcount)
    setTimeout(() => void saturateAppLogsAndColorsFromAllPublicUsers(), 1000)
  }
  isInitRunning = false
  return { wnfs, wn, state, username }
}
