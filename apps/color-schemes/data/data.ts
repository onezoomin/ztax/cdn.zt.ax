import { groupBy } from 'lodash-es'
import { utcMsTs } from 'bygonz'
import { Logger } from 'logger'
import { changeHue, changeLightness, changeSaturation, randomHexColor } from '../components/utils/color-utils'
import { getUsername, initWebnative } from './webnative'
import { Color } from './Model/Color'
import { getInitializedColorsDB } from './dexie'

const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[Colors/data]', performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const DO_MOCK_CHANGES = false

const ENOUGH = 35000
const MOCK_FREQUENCY = 5000
const WAIT_TILL_FIRST_MOCK = 20000
const initColor = randomHexColor()

let ch = 0; let row = 0; let startTS = Date.now() // eslint-disable-line max-statements-per-line

const exampleRow = (modified = utcMsTs(), baseColor = changeHue(initColor, (ch += 36))) => {
  let creator = getUsername()
  if (!creator) {
    creator = `0xExData${randomHexColor()}`
    WARN('random creator set', creator)
  }
  return {
    ID: ++row,
    elements: [
      new Color({ row, creator, color: baseColor, modified }),
      new Color({ row, creator, color: changeLightness(baseColor, 1.1), modified }),
      new Color({ row, creator, color: changeLightness(baseColor, 1.3), modified }),
      new Color({ row, creator, color: changeLightness(baseColor, 1.5), modified }),
    ],
  }
}

let runOnce, randomChangeInterval, ColStruct, colorsDB

export const initColorSchemeData = async () => {
  if (runOnce)
    return WARN('initColorSchemeData already run(ning)')
  runOnce = true

  LOG('initializing wn from initColorSchemeData')
  await initWebnative()
  LOG('initialed as', getUsername())

  colorsDB = await getInitializedColorsDB(getUsername())
  const colTable = colorsDB.Colors

  const currentColors = await colTable.toArray()
  const rows = Object.values(groupBy(currentColors, 'row')).map((elements, i) => ({
    ID: i + 1,
    elements,
  }))
  if (rows.length > 1) {
    ColStruct = { rows }
    DEBUG('ColStruct from dexie', ColStruct)
  }
  else {
    ColStruct = {
      rows: [
        exampleRow(),
        exampleRow(),
        exampleRow(),
        exampleRow(),
      ],
    }
    for (const eachRow of ColStruct.rows) {
      const colorsToAdd = eachRow.elements
      for (const eachCol of colorsToAdd)
        await colTable.add(eachCol)

      DEBUG({ colorsToAdd, fromDB: await colTable.toArray() })
    }
    DEBUG('Created new ColStruct', ColStruct)
  }
  if (DO_MOCK_CHANGES) {
    setTimeout(() => {
      startTS = Date.now()
      randomChangeInterval = randomChangeInterval ?? setInterval(() => {
        void (async () => {
          await randomChange()
        })()
      }, MOCK_FREQUENCY)
    }, WAIT_TILL_FIRST_MOCK)
  }
  return ColStruct
}

const MAX_ROW_LENGTH = 6

export const finishUpRandomChangeInterval = async () => {
  console.log('clearing', { randomChangeInterval })
  clearInterval(randomChangeInterval)
  setTimeout(() => console.log('finishing enough for now'), 750)
}
export const randomChange = async () => {
  const isEnoughForNow = ((Date.now() - startTS) > ENOUGH)
  if (isEnoughForNow)
    void finishUpRandomChangeInterval()

  const colTable = colorsDB.Colors
  const agentID = getUsername() ?? `0xRndData${randomHexColor()}`
  const { rows } = ColStruct
  let op = ['create', 'create', 'update', 'update', 'update', 'delete'][Math.floor(Math.random() * 5.99)]
  const row = Math.floor(Math.random() * rows.length)
  if (op === 'create' && rows[row].elements.length === MAX_ROW_LENGTH)
    op = 'update'
  const elIndex = Math.floor(Math.random() * (rows[row].elements.length - 1))
  const { ID, color: elcolor, row: _prevRow, ...restEl } = rows[row].elements[elIndex]
  let color, colorObj
  switch (op) {
    case 'create':
      color = randomHexColor()
      colorObj = new Color({ color, row, creator: agentID })
      await colTable.add(colorObj)
      rows[row].elements.push(colorObj)
      return DEBUG('create', { row, elIndex, ColStruct })
    case 'update':
      color = Math.random() > 0.5 ? changeLightness(elcolor, 0.8) : changeSaturation(elcolor, 1.2)
      colorObj = new Color({ ID, ...restEl, color, modified: utcMsTs(), modifier: agentID }) // could add row to also enable moving to different row, but ugly
      ColStruct.rows[row].elements[elIndex] = colorObj

      await colTable.update(ID, colorObj)
      return DEBUG('update', { row, elIndex, ColStruct })
    case 'delete':
      colorObj = new Color({ ID, ...restEl, isDeleted: true, color: '-deleted-', row: -1, modified: utcMsTs(), modifier: agentID })
      ColStruct.rows[row].elements[elIndex] = colorObj
      await colTable.update(ID, colorObj)
      return DEBUG('delete', { row, elIndex, rows })

    default:
      return DEBUG('no-op')
  }
}

export { randomChangeInterval as updateInterval }
