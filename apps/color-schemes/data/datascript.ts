let { datascript: ds, SQLdb } = globalThis

// create DB schema, a regular JS Object
const schema = {
  aka: { ':db/cardinality': ':db.cardinality/many' },
  friend: { ':db/valueType': ':db.type/ref' },
}

// define initial datoms to be used in transaction
const datomDefaults = [
  {
    ':db/id': -1,
    name: 'Ivan',
    age: 18,
    aka: ['X', 'Y'],
  },
  {
    ':db/id': -2,
    name: 'Igor',
    aka: ['Grigory', 'Egor'],
  },
  // use :db/add to link datom -2 as friend of datom -1
  [':db/add', -1, 'friend', -2],
]
let conn
const reports = []
export const doQuery = () => {
  if (!ds) ds = globalThis.datascript
  // Use JS API to create connection and add data to DB
  // create connection using schema
  if (!conn) conn = ds.create_conn(schema)

  // setup listener called main
  // pushes each entity (report) to an Array of reports
  // This is just a simple example. Make your own!
  ds.listen(conn, 'main', report => {
    reports.push(report)
  })

  // Tx is Js Array of Object or Array
  // pass datoms as transaction data
  ds.transact(conn, datomDefaults, 'initial info about Igor and Ivan')
  // Fetch names of people who are friends with someone 18 years old
  // query values from conn with JS API
  const result = ds.q('[:find ?n :in $ ?a :where [?e "friend" ?f] [?e "age" ?a] [?f "name" ?n]]', ds.db(conn), 18)

  // print query result to console!
  console.log(result) // [["Igor"]]
}

export const checkDB = () => {
  console.log({ reports, conn })
}

const dataLogElements = { now: {}, then: {} }

export const logChanges = (objectsWithUniqueIds: any[]) => {
  if (!SQLdb) SQLdb = globalThis.SQLdb
  // console.log(objectsWithUniqueIds)
  console.log({ SQLdb })

  // Insert two rows: (1,111) and (2,222)

  const p = performance.now()
  for (const eachEl of objectsWithUniqueIds) {
    const { modified: asOf, id, ...nonTx } = eachEl
    for (const eachKey of Object.keys({ ...nonTx })) {
      if (eachKey === 'isDeleted' && !eachEl[eachKey]) continue // don't bother storing all the false isDeleted attrs

      const datalogEatKey = `${id}:element/${eachKey}`

      const thenEntry = dataLogElements.then[datalogEatKey] = dataLogElements.then[datalogEatKey] ?? []
      const nowEntry = dataLogElements.now[datalogEatKey]
      const oldVal = nowEntry?.[':att/is']
      if (!eachEl.isDeleted && nowEntry) {
        // console.log('same?', oldVal, eachEl[eachKey], nowEntry[':att/is'] === eachEl[eachKey])
        if (nowEntry[':att/is'] !== eachEl[eachKey]) {
          // if a change happened move to then (equivalent to setting op to false in datomic terms)
          // TODO avoid duplicate delete entries
          thenEntry.push({
            ':att/was': oldVal,
            until: asOf,
          })
        }
      }
      if (eachEl.isDeleted) {
        if (!thenEntry.length) {
          if (eachKey === 'isDeleted') {
            thenEntry.push({
              ':att/is': 'deleted',
              asOf,
            })
          } else {
            thenEntry.push({
              ':att/was': oldVal,
              until: asOf,
            })
          }
        }
        delete dataLogElements.now[datalogEatKey] // eslint-disable-line @typescript-eslint/no-dynamic-delete
      } else {
        // db.run(`CREATE TABLE IF NOT EXISTS txs(
        //   ts,
        //   un,
        //   en,
        //   at,
        //   vl,
        //   PRIMARY KEY(ts,un,en,at)
        // );`)
        const ins = `INSERT INTO txs VALUES (${asOf}, 'username', '${id}', '${eachKey}', '${eachEl[eachKey]}')
          ON CONFLICT(ts,un,en,at) DO UPDATE SET vl=excluded.vl;`
        console.log('insert ', { ins })
        const insRes = SQLdb.exec(ins)
        // console.log('insert res', insRes)
        dataLogElements.now[datalogEatKey] = {
          ':att/is': eachEl[eachKey],
          asOf,
        } // frequent idempotent reassignment
      }
    }
  }
  console.log(`checked ${objectsWithUniqueIds.length} elements in ${(performance.now() - p).toFixed(2)}ms`, dataLogElements)
  // Prepare a statement
  // const { columns, values } = SQLdb.exec('SELECT * FROM txs')
}
