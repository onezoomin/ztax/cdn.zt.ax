# cdn.zt.ax

wip repo for bundling useful ztax (web app development stacks) into modules,  
especially geared towards far edge 'serverless' deployment 

all ztax from the src directory are auto deployed to a fission app:
[ztax-cdn.fission.app](https://ztax-cdn.fission.app/)

edge deployed apps can import these bundled mjs modules 
 - multiple libraries with a single request
 - comfortable esnext syntax

## Hacking
install rustup
switch to rustup nightly via `rustup default nightly`
cargo install wasm-bindgen-cli
```
git clone git@gitlab.com:onezoomin/forks/rust-ipfs-client-defluencer ../rust-defluencer
pnpm i  
time pnpm build  
pnpm dev
```

## Contributing

we are hoping for many PRs in the future, for now please start with opening an issue and [engaging with our RFCs](https://gitlab.com/onezoomin/ztax/cdn.zt.ax/-/issues?label_name=type%3A%3Arfc)

