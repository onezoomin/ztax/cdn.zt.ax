const SHOULD_PREFIX_JETBRAINS_URL = false

export function ifNode () {
  // if(!process) globalThis.process = {env:{}}
  const colorStyles = require('colors/lib/styles')
  const colors = require('colors/safe') as typeof Tcolors
  DEFAULT_CONFIG.serverInspectOptions = {
    // https://nodejs.org/api/util.html#util_util_inspect_object_options
    depth: 5, // default: 2
    colors: colors.enabled,
  }
  // on the server, we can return a custom function, and even add custom stack trace and colors to it - cool right?
  // and we don't need performance info as the log is already prefixed with the current time
  return (...args) => {
    const stack = new Error().stack
    let origin = Logger.getOriginFromStack(stack)
    // const originFilename = origin.split('\\').pop().split('/').pop(); // eslint-disable-line
    if (SHOULD_PREFIX_JETBRAINS_URL) origin = `http://localhost:63342/api/file/${origin}` // shortened <a link would be sexier but doesn't work - maybe custom formatter?
    if (config.printOriginOnServer) args.push(colors.grey(` ${origin}`))
    if (config.printStackLinesOnServer) {
      args.push(
          `\n${colors.gray(
            stack
              .split('\n')
              .slice(3, config.printStackLinesOnServer === true ? undefined /* print all */ : +config.printStackLinesOnServer + 3) // 0=Error, 1=Logger, 2=origin(already added above), 3...
              .join('\n'),
          )}`,
      )
    }
    // if (config.customFormattersOnServer) {
    //   // if custom formatters are disabled, format objects (deeply)
    //   args = args.map(arg => {
    //     if (_.isPlainObject(arg)) return _.mapValues(arg, value => applyCustomFormatters(value, config));
    //     return applyCustomFormatters(arg, config);
    //     return util.inspect(arg, config.serverInspectOptions);
    //   });
    // }
    if (config.serverInspectOptions) {
      args = args.map(arg => (isString(arg) ? arg : util.inspect(arg, config.serverInspectOptions)))
    }
    if (config.dim) {
      if (typeof args[0] === 'string') args[0] = `${(colorStyles.dim.open)}${args[0]}` // eslint-disable-line @typescript-eslint/restrict-template-expressions
      else args.unshift(colorStyles.dim.open)
      args.push(colorStyles.dim.close)
    }
    return console.log.apply(console, args)
  }
}
