import { isBrowser, isNode } from 'browser-or-node'
import { defaults } from 'lodash-es'

const isChrome = isBrowser && navigator.userAgent.includes('Chrome')

/// /////////
// CONFIG //
/// /////////

let TIME_PREFIX_ENABLED = true
let PERFORMANCE_PREFIX_ENABLED = true
if (isNode) {
  TIME_PREFIX_ENABLED = process?.env?.LOGGER_TIME_PREFIX ? process?.env?.LOGGER_TIME_PREFIX === 'true' : true
  PERFORMANCE_PREFIX_ENABLED = process?.env?.LOGGER_PERF_PREFIX ? process?.env?.LOGGER_PERF_PREFIX === 'true' : true
}
const DEFAULT_CONFIG = {
  prefix: undefined as String | undefined,
  performanceEnabled: PERFORMANCE_PREFIX_ENABLED,
  dimDebugOnServer: true,
  printOriginOnServer: true,
  serverInspectOptions: {},
  customFormattersOnServer: true,
  // Depth of stacktrace to show on server
  // - integer saying how many lines
  // - false to disable, true for all
  printStackLinesOnServer: false,
}
export type Config = Partial<typeof DEFAULT_CONFIG>

export const StringFormatters = [
  // Format to string (Client console - currently not on server)
  // {
  //   match: obj => obj instanceof Model,
  //   format: m => {
  //     const { displayName } = m
  //     return `${m.constructor.name}#${m.ID}${displayName ? `{${displayName}}` : ''}`
  //   },
  // },
]

// let applyCustomFormatters;
// applyCustomFormatters = (value, config) => {
//   for (const formatter of StringFormatters) {
//     if (formatter.match(value)) return formatter.format(value);
//   }
//   if (!_.isString(value)) {
//     return util.inspect(value, config.inspectOptionsOnServer);
//   }
//   return value;
// };

/// ////////////////
// RUNTIME STUFF //
/// ////////////////

export class P {
  sinceFirst: number = null
  sinceLast: number = null
  constructor (sinceFirst, sinceLast) {
    Object.assign(this, { sinceFirst, sinceLast })
  }

  toString () {
    const sinceFirstNum = (this.sinceFirst * 0.001)
    const sinceFirst = sinceFirstNum.toFixed(sinceFirstNum < 5 ? 2 : 1) /* .padStart(3, ' ') */
    const sinceLast
      = this.sinceLast < 1000 // when >= 1s, format as seconds
        ? `${this.sinceLast.toFixed(0)}`.padStart(5, ' ')
        : `${`${(this.sinceLast * 0.001).toFixed(this.sinceLast < 10000 ? 1 : 0)}`.padStart(4, ' ')}s`
    return `[${[TIME_PREFIX_ENABLED ? sinceFirst : null, PERFORMANCE_PREFIX_ENABLED ? sinceLast : null].join(',')}]`
  }
}
// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export class Style {
  constructor (style, string) {
    Object.assign(this, { style, string })
  }
}
let formattersEnabled = false // for tracking if chrome formatters are enabled (don't change manually)

export class Logger { // eslint-disable-line @typescript-eslint/no-extraneous-class
  static ERROR: 2 = 2
  static WARN: 4 = 4
  static INFO: 6 = 6 // alias for LOG
  static LOG: 6 = 6
  static DEBUG: 8 = 8
  static VERBOSE: 10 = 10

  /** @type {number|null} */
  static firstLog = null
  /** @type {number|null} */
  static lastLog = null

  /**
   * This is a clever way to log without the extra check on each log line.
   * Use it like this:
   * ```
   * const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO); // eslint-disable-line no-unused-vars
   * LOG('message', data);
   * DEBUG(this.collectionName, '...')
   * if (!VERBOSE.isDisabled) {
   *   const verboseStuff = generateVerboseStuff(); // computationally intensive
   *   VERBOSE('Here it is:', verboseStuff)
   * }
   * ```
   *
   * @ param {number} level See {@link LVs}
   * @ param {object} config see DEFAULT_CONFIG
   * @ typedef {Function & {isDisabled: boolean}} LogFunc
   * @ returns {{LOG: LogFunc, VERBOSE: LogFunc, DEBUG: LogFunc, WARN: LogFunc, ERROR: LogFunc}}
   */
  static setup (level: 2 | 4 | 6 | 8 | 10, config: Config = {}) {
    defaults(config, DEFAULT_CONFIG) // fyi this is like the reverse of Object.assign
    const Meteor = globalThis.Meteor ?? {}
    const debugLogForStub = this.wrapLog(console.debug, { ...config, dim: config.dimDebugOnServer }) // do it once to cache
    const stub = (...args) => {
      // @ts-expect-error
      if (Meteor?.isClient && window.ARM_IT_NOW) {
        // allows to set window.ARM_IT_NOW to enable DEBUG logging EVERYWHERE
        debugLogForStub(...args)
      }
    } // For disabled log-levels, we just return a stub function
    stub.isDisabled = true // this makes it possible to check, e.g. `DEBUG.isDisabled`
    const groupC = this.wrapLog(console.groupCollapsed, config)
    const addExtraFxs = (func: <T>(...T) => void) =>/* : ((...T) => void & {isDisabled: boolean}) */
      Object.assign(func, {
        isDisabled: func === stub,
        group: async function (...args) {
          const inside = args[args.length - 1]
          if (func === stub) return inside()
          groupC(...args.slice(0, -1))
          try {
            const result = inside()
            return result instanceof Promise ? await result : result
          } finally {
            console.groupEnd()
          }
        },
      })

    const ERRFX = addExtraFxs(level >= Logger.ERROR ? this.wrapLog(console.error, config) : stub)
    // disabled bc. dangerous: (JSON.stringify chokes on circular references)
    // const ERROR = (...params) => {
    //   ERRFX(...params)
    //   return new Error(JSON.stringify(params)) // so we can throw it
    // }

    return {
      ERROR: ERRFX,
      WARN: addExtraFxs(level >= Logger.WARN ? this.wrapLog(console.warn, config) : stub),
      LOG: addExtraFxs(level >= Logger.INFO ? this.wrapLog(console.log, config) : stub),
      DEBUG: addExtraFxs(level >= Logger.DEBUG ? this.wrapLog(console.log, { ...config, dim: config.dimDebugOnServer }) : stub), // not using console.debug as that requires dev tools verbose to be enabled
      VERBOSE: addExtraFxs(level >= Logger.VERBOSE ? this.wrapLog(console.debug, { ...config, dim: config.dimDebugOnServer }) : stub),
    }
  }

  static wrapLog (logFunc, config) {
    if (!isNode) { // isBrowser is not true for web workers
      // Would be nice to get formtted links, but <a tags are not allowed
      // const stack = new Error().stack;
      // const origin = Logger.getOriginFromStack(stack);
      // const originFilename = origin.split('\\').pop().split('/').pop(); // eslint-disable-line
      // if (origin) fixedArgs.push({ link: originFilename, href: `http://localhost:63342/api/file/${origin}` });

      // on the client, we need to return the original log function, as otherwise the origin of the log is set to here
      // But luckily we can bind fixed parameters on the function:
      const fixedArgs = []
      // if the custom formatters are enabled, and we want performance logging - we add an instance of P as a prefix
      if (config.performanceEnabled) {
        const p = isChrome
          ? ['', Logger.perf()] // this is a placeholder that gets replaced by the custom formatter
          : [] // [{ get perfNiceTry () { return Logger.perf().toString() } }] // this (of course) shows the time when you click the getter
        fixedArgs.push(...p) // if we start with a non-string, all strings are rendered with quotes :/
      }
      if (config.prefix) {
        if (formattersEnabled)fixedArgs.push(new Style('dim', config.prefix))
        else fixedArgs.push(config.prefix)
      }
      // console.log('logger-log', { config, fixedArgs })
      return logFunc.bind(console, ...fixedArgs)
    }
  }

  // static log(msg, logPerformance = true) {
  //   const p = this.perf();

  //   return logPerformance ? `${p} ${msg}` : msg;
  // }

  static perf (returnAsString = 0): P | string | undefined {
    if (!performance.now) return undefined // server: p = { now: require('performance-now') };
    const now = performance.now()
    if (!Logger.firstLog) {
      Logger.firstLog = now
      Logger.lastLog = now
    }
    const p = new P(now - Logger.firstLog, now - Logger.lastLog)
    // console.log(now)
    Logger.lastLog = now
    return returnAsString ? p.toString() : p
  }

  // ~ https://stackoverflow.com/a/47296370/1633985
  static getOriginFromStack (stack): string {
    let skippedFirst = false // the first one is Logger.js
    for (const line of stack.split('\n')) {
      const matches = line.match(/^\s+at\s+(.*)/)
      if (matches) {
        if (skippedFirst) {
          // first line - current function
          // second line - caller (what we are looking for)
          return (
            matches[1]
              .trim() // Removes spaces
              // .replace(/^module /, '') // Removes 'module ' at beginning
              // .replace(/^at /, '') // Removes 'at ' at beginning
              // .replace(/^ \(/, '') // Removes parentheseses around file
              // .replace(/\)$/, '') // -||-
              .replace(__dirname, '') // Removes script folder path (if exists)
          )
        }
        skippedFirst = true
      }
    }
  }
}

if (isBrowser) { // isBrowser is not true for web workers
  // CLIENT or Worker //

  // @ts-expect-error
  if (!window.devtoolsFormatters) window.devtoolsFormatters = []
  // noinspection JSUnusedGlobalSymbols
  // @ts-expect-error
  window.devtoolsFormatters.push(
    {
      // Custom formatters
      header: obj => {
        for (const formatter of StringFormatters) {
          if (formatter.match(obj)) return ['span', {}, formatter.format(obj)]
        }
        return null
      },
      hasBody: () => true,
    },
    {
      // Performance object //
      // This is a sneaky way to show performance info withouth any extra calls when logging:
      // LOG(`...`)
      // it would work without this if we would have some way to intercept the call:
      // L.INFO('...') - a getter on L that adds the current time as prefix
      // LOG('...')() - returns the log function with all argumends bound
      // But instead we are adding a custom formatter that replaced the placeholder object of class P with the performance info
      header: obj => {
        if (!(obj instanceof P)) return null
        if (!formattersEnabled) formattersEnabled = true
        const p = Logger.perf()
        return [
          'span',
          { style: 'font-weight: light; color: grey' },
          p?.toString(),
          // ['a', { href: 'vscodium://file/home/manu/dev/tamera/bookr/imports/lib/utils/Logger.js' }, 'WOW'],
        ]
      },
      hasBody: () => false,
    },
    {
      // Style object
      header: obj => {
        if (!(obj instanceof Style)) return null
        if (!formattersEnabled) formattersEnabled = true
        // @ts-expect-error
        return ['span', { style: 'font-weight: light; color: grey; '/* font-style: italic; */ }, obj.string]
      },
      hasBody: () => false,
    },
  )
} else if (isNode) {
  // SERVER //
  // node stuff removed as it breaks/bloats webworker build
}
