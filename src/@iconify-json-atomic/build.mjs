import { log, warn } from 'console'
import { build } from 'esbuild'
import path from 'path'
import fs from 'fs'

// files for each collection are here:
// https://esm.sh/@iconify-json/carbon@1.1.6/icons.json

// TODO auto download
// TODO auto bump

const jsonSplitPlugin = {
  name: 'jsonSplit',
  setup: (build) => {
    build.onLoad({ filter: /icons\.json$/ }, async (args) => {
      // Load the file from the file system
      const source = await fs.promises.readFile(args.path, 'utf8')

      const filename = path.relative(process.cwd(), args.path)
      const dirName = path.dirname(filename)
      log(args, filename)
      const { icons } = JSON.parse(source)
      const iconNames = Object.keys(icons)
      const collectionOutPath = `../../dist/@iconify-json-atomic/${dirName}`
      // log(icons[iconNames[0]], iconNames.length)
      fs.mkdirSync(`${collectionOutPath}/atomic`, { recursive: true })

      // write the full json file
      await fs.writeFile(`${collectionOutPath}/icons.json`, source, 'utf8', (err) => {
        err && warn(err)
      })
      for (const eachIconName of iconNames) {
        const eachFilePath = `${collectionOutPath}/atomic/${eachIconName}.json`
        await fs.writeFile(eachFilePath, JSON.stringify(icons[eachIconName]), 'utf8', (err) => {
          // When a request is aborted - the callback is called with an AbortError
          err && warn(err)
        })
      }

      // returning will prevent
      // return { contents: source }

      // Convert Svelte syntax to JavaScript
      //   try {
      //     const { js, warnings } = svelte.compile(source, { filename })
      //     const contents = js.code + '//# sourceMappingURL=' + js.map.toUrl()
      //     return { contents, warnings: warnings.map(convertMessage) }
      //   } catch (e) {
      //     return { errors: [convertMessage(e)] }
      //   }
    })
  },
}

// const entryPoints = ['logos/icons.json']
const entryPoints = fs.readdirSync('./', { withFileTypes: true })
  .filter(dirent => dirent.isDirectory() && dirent.name !== 'node_modules')
  .map(eachDir => `${eachDir.name}/icons.json`)
log(entryPoints)

build({
  entryPoints,
  format: 'esm',
  // bundle: true,
  // loader: {
  //   '.json': 'file',
  // },
  outdir: '../../dist/',
  outbase: '../',
  outExtension: { '.js': '.mjs' },
  plugins: [jsonSplitPlugin],
}).catch(() => process.exit(1))
