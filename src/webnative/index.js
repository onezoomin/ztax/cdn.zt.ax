
import * as webnativeMod from 'webnative'
import { doTest } from './test'
const webnative = { ...webnativeMod, doTest }
globalThis.webnative = webnative

console.debug({ doTest, webnative, webnativeMod })
export { doTest, webnative }
