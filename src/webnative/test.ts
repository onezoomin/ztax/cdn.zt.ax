import type { Links } from 'webnative/fs/types'
import type { DirectoryPath } from 'webnative/path'
import type { FileSystem as Twnfs } from 'webnative/fs/filesystem'
let runOnce,
  wnfs: Twnfs,
  testDir: DirectoryPath,
  lsResults: Links

const doTest = async ({ webnative }, force = false) => {
  if (runOnce && !force) return console.warn('already runOnce')
  const wn = webnative ?? globalThis.webnative
  if (!wn) return console.warn('missing wn')

  /**
   * webnative
   */
  const dirName = 'testDir'

  const target = document.querySelector('#test')
  target?.insertAdjacentHTML('beforeend', '<div class="w-1/2" bg="teal-200">wnfs test</div>')
  const state = await wn.initialise({
    permissions: {
      // Will ask the user permission to store
      // your apps data in `private/Apps/onezoomin/nested-data-grid`
      app: {
        name: 'webnative-test',
        creator: 'ztax',
      },

      // Ask the user permission for additional filesystem paths
      fs: {
        publicPaths: ['/'],
      },
    },
  })

  switch (state.scenario) {
    case wn.Scenario.AuthCancelled:
      // User was redirected to lobby,
      // but cancelled the authorisation
      break

    case wn.Scenario.AuthSucceeded:
    case wn.Scenario.Continuation:
      // State:
      // state.authenticated    -  Will always be `true` in these scenarios
      // state.newUser          -  If the user is new to Fission
      // state.throughLobby     -  If the user authenticated through the lobby, or just came back.
      // state.username         -  The user's username.
      //
      // ☞ We can now interact with our file system (more on that later)
      wnfs = globalThis.wnfs = state.fs
      globalThis.ucan = wn.ucan

      lsResults = await wnfs.ls(wnfs.appPath())
      testDir = wn.path.directory(...wnfs.appPath().directory, dirName)

      if (!Object.keys(lsResults).includes(dirName)) {
        await wnfs.mkdir(testDir)
        lsResults = await wnfs.ls(wnfs.appPath())
        await wnfs.publish()
      }

      //   const rowsLS = await wnfs.ls(rowsDir)
      //   console.log('first fs', { lsResults, rowsLS, state, apDir, pathString, wnfs, wn })

      break

    case wn.Scenario.NotAuthorised:
      wn.redirectToLobby(state.permissions)
      break
  }
  console.log(lsResults)
  if (wnfs) {
    lsResults = await wnfs.ls(testDir)
    const filePath = wn.path.file(...testDir.directory, 'test.json') // add will need  { file: string[] }
    if (!Object.keys(lsResults).length) {
      // ensure at least one file
      await wnfs.add(filePath, 'test test 1,2')
      await wnfs.publish()
    }
    console.log({ lsResults, filePath, wnfs })
  }

  target?.insertAdjacentHTML('beforeend', `<div class="w-1/2" bg="teal-400">${JSON.stringify(lsResults, null, 2)}</div>`)
  target?.insertAdjacentHTML('beforeend', '<div><a target="_blank" rel="noopener" href="https://drive.fission.codes/#/1zm/Apps/ztax/webnative-test/testDir/">Check on drive.fission.codes</a></div>')

  runOnce = true
}

export { doTest }
