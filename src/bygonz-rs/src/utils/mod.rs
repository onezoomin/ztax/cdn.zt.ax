use anyhow::{anyhow, Result};
use std::fmt::Debug;

#[allow(dead_code)]
pub fn map_vec_into<'input, I: 'input, O>(vec: &'input Vec<I>) -> Vec<O>
where
    // I: Into<O> - same as:
    O: From<&'input I>,
{
    vec.into_iter().map(|item| item.into()).collect()
}

// pub fn map_vec_try_into<'input, I: 'input, O>(vec: &'input Vec<I>) -> Result<Vec<O>>
// where
//     // I: Into<O> - same as:
//     O: TryFrom<&'input I>,
// {
//     let iter_map = vec.into_iter().map(|item| item.try_into()?);
//     iter_map.collect()
// }
