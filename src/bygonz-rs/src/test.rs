use anyhow::{anyhow, bail, Context, Error, Result};
use std::future::Future;
use tokio; // 1.15.0

struct Transaction {}

async fn open_trx<'func_life, T, F, RFuture, FuncReturn>(trx_func: F) -> Result<FuncReturn, Error>
where
    T: AsRef<str>,
    RFuture: 'func_life + Future<Output = Result<FuncReturn, Error>>,
    F: 'func_life + FnOnce(&'func_life Transaction) -> RFuture,
{
    let trx = rexie.transaction(tables, mode);
    match trx {
        Err(err) => Err(anyhow!("Failed to open transaction: {:?}", err)),
        Ok(trx) => {
            async move {
                let trx: Transaction = trx;
                let result = trx_func(&trx)
                    .then(async move |result| {
                        let trx_end_result = match result {
                            Ok(_) => trx.done().await,
                            Err(_) => trx.abort().await,
                        }?;
                        result
                    })
                    .await;

                result
            }
            .await
        }
    }
}

#[tokio::main]
async fn main() {
    open_trx(
        rexie,
        &[SUBS_TABLE],
        TransactionMode::ReadWrite,
        async move |trx| {
            let result = async {
                let sub_store = trx
                    .store(SUBS_TABLE)
                    .expect(format!("should open store '{}'", SUBS_TABLE).as_str());
                if let Err(err) = sub_store.get(&JsValue::from("123")).await {
                    bail!("Failed to get from store: {}", err)
                }
                Ok(())
            }
            .await;
            match &result {
                Ok(_) => close_trx(trx, TrxAction::Commit).await,
                Err(_) => close_trx(trx, TrxAction::Abort).await,
            }?;
            result
        },
    )
    .await
    .context("transaction")?;
}
