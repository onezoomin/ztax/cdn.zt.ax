use std::{marker::PhantomData, str::FromStr};

use anyhow::{anyhow, bail, Result};
use cid::Cid;
use log::{info, warn};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::ipfs::DagLink;

// Attempt to work around https://github.com/rustwasm/wasm-bindgen/issues/1591
// #[wasm_bindgen(typescript_custom_section)]
// const TS_APPEND_CONTENT: &'static str = r#"
// export function current_applog(): () => Atom[];
// "#;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DagAppLog {
    pub atom: DagLink<DagAtom>,
    pub tx: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DagAtom {
    // pub tx: String,
    // pub ha: DagLink,
    pub ag: String,
    pub ts: u64,
    pub pv: Option<DagLink<DagAtom>>,
    pub en: String,
    pub at: String,
    pub vl: Value,
    pub op: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Atom {
    pub ag: String,
    pub ts: u64,
    pub pv: Option<String>,
    pub en: String,
    pub at: String,
    pub vl: serde_json::Value,
    pub op: bool,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
// #[wasm_bindgen]
// #[wasm_bindgen(getter_with_clone, inspectable)]
pub struct AppLog {
    pub ag: String,
    pub ts: u64,
    pub pv: Option<String>,
    pub tx: String,
    pub ha: String,
    pub en: String,
    pub at: String,
    pub vl: serde_json::Value,
    pub op: bool,
}

impl TryFrom<&Atom> for DagAtom {
    type Error = anyhow::Error;
    fn try_from(a: &Atom) -> Result<Self, Self::Error> {
        Ok(DagAtom {
            ag: a.ag.clone(),
            en: a.en.clone(),
            at: a.at.clone(),
            ts: a.ts,
            pv: match a.pv.clone() {
                Some(pv) => Some(Cid::from_str(pv.as_str())?.into()),
                None => None,
            },
            vl: a.vl.clone(), /*.into_serde().expect("should be able to convert JsValue -> rust Value") */
            op: a.op.clone(),
        })
    }
}

// impl TryFrom<&AppLog> for DagAppLog {
//     type Error = anyhow::Error;
//     fn try_from(a: &AppLog) -> Result<Self, Self::Error> {
//         Ok(DagAppLog {
//             tx: a.tx.clone(),
//             atom: Atom {
//                 ag: a.ag.clone(),
//                 // ha: DagLink {
//                 //     cid: Cid::from_str(a.ha.as_str())?,
//                 // },
//                 en: a.en.clone(),
//                 at: a.at.clone(),
//                 ts: a.ts,
//                 pv: match a.pv.clone() {
//                     Some(pv) => Some(DagLink {
//                         cid: Cid::from_str(pv.as_str())?,
//                         type_for_later_todo: PhantomData,
//                     }),
//                     None => None,
//                 },
//                 vl: a.vl.clone(), /*.into_serde().expect("should be able to convert JsValue -> rust Value") */
//             },
//         })
//     }
// }

// impl From<&DagAppLog> for AppLog {
//     fn from(log: &DagAppLog) -> Self {
//         AppLog {
//             ag: log.atom.ag.clone(),
//             tx: log.tx.clone(),
//             ha: log.ha.cid.to_string(),
//             pv: log.pv.and_then(|pv| Some(pv.cid.to_string())),
//             en: log.en.clone(),
//             at: log.at.clone(),
//             ts: log.ts.clone(),
//             vl: /* JsValue::from_serde( &*/log.vl.clone()/* ).expect("should be able to convert dag Value -> JsValue") */,
//         }
//     }
// }

impl From<&AppLog> for Atom {
    fn from(log: &AppLog) -> Self {
        Atom {
            ag: log.ag.clone(),
            pv: log.pv.clone(),
            en: log.en.clone(),
            at: log.at.clone(),
            ts: log.ts.clone(),
            vl: /* JsValue::from_serde( &*/log.vl.clone()/* ).expect("should be able to convert dag Value -> JsValue") */,
            op: log.op.clone(),
        }
    }
}
impl From<&DagAtom> for Atom {
    fn from(atom: &DagAtom) -> Self {
        Atom {
            ag: atom.ag.clone(),
            pv: atom.pv.as_ref().map(|pv| pv.cid.to_string()),
            en: atom.en.clone(),
            at: atom.at.clone(),
            ts: atom.ts.clone(),
            vl: /* JsValue::from_serde( &*/atom.vl.clone()/* ).expect("should be able to convert dag Value -> JsValue") */,
            op: atom.op.clone(),
        }
    }
}

// #[wasm_bindgen]
// pub fn current_applog() -> Result<JsValue, JsValue> {

// }
