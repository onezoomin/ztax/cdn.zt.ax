use crate::{Publication, Subscription};
use anyhow::{anyhow, bail, ensure, Context, Error, Result};
use async_once_cell::{Lazy, OnceCell};
use async_trait::async_trait;
use cid::Cid;
use futures_util::{future::AbortHandle, future::FutureExt, join, Future, StreamExt, TryFutureExt};
use log::{debug, trace};
// use once_cell::sync::OnceCell;
// use anyhow::{Context, Result};
use gloo_utils::format::JsValueSerdeExt;
use serde::{de::DeserializeOwned, Deserialize};
use serde_json::Value;
use std::{
    borrow::BorrowMut,
    collections::HashMap,
    fmt::{Debug, Display},
    ops::Deref,
    pin::Pin,
    sync::{atomic, Arc, Mutex, MutexGuard},
};

pub mod inmemory;
use crate::datalog::AppLog;

#[async_trait(?Send)]
// #[async_trait]
pub trait ApplogDB {
    async fn get_pub(&self, publ: Option<&str>) -> Result<Option<Publication>>;

    async fn get_sub(&self, sub_id: &str) -> Result<Option<Subscription>>;

    // async fn get_sub_raw<T>(&self, sub_id: &String) -> Result<Option<T>>
    // where
    //     T: ?Sized + DeserializeOwned;

    async fn update_pub(
        &self,
        sub_id: &str,
        updater: Box<dyn FnOnce(HashMap<String, Value>) -> HashMap<String, Value>>,
    ) -> Result<()>;

    async fn update_sub(
        &self,
        sub_id: &str,
        updater: Box<dyn FnOnce(HashMap<String, Value>) -> HashMap<String, Value>>,
    ) -> Result<()>;

    async fn save_applogs(&self, applogs: &Vec<AppLog>) -> Result<Vec<AppLog>>;
}
