use std::{collections::HashMap, rc::Rc};

use anyhow::Result;
use async_trait::async_trait;
use dashmap::DashMap;
use serde_json::Value;

use crate::{datalog::AppLog, Publication, Subscription};

use super::ApplogDB;
pub struct InmemoryDB {
    subscriptions: DashMap<String, Subscription>,
}

pub async fn create_inmemory_db() -> Result<InmemoryDB> {
    Ok(InmemoryDB {
        subscriptions: DashMap::new(),
    })
}

impl InmemoryDB {
    pub fn add_sub(&mut self, sub: Subscription) {
        self.subscriptions.insert(sub.ID.clone(), sub);
    }
}

#[async_trait(?Send)]
// #[async_trait]
impl ApplogDB for InmemoryDB {
    async fn get_pub(&self, _publ: Option<&str>) -> Result<Option<Publication>> {
        todo!()
    }

    async fn get_sub(&self, sub_id: &str) -> Result<Option<Subscription>> {
        Ok(self.subscriptions.get(sub_id).map(|s| s.clone()))
    }

    // async fn get_sub_raw<T>(&self, sub_id: &String) -> Result<Option<T>>
    // where
    //     T: ?Sized + DeserializeOwned{

    async fn update_pub(
        &self,
        _sub_id: &str,
        _updater: Box<dyn FnOnce(HashMap<String, Value>) -> HashMap<String, Value>>,
    ) -> Result<()> {
        todo!()
    }

    async fn update_sub(
        &self,
        _sub_id: &str,
        _updater: Box<dyn FnOnce(HashMap<String, Value>) -> HashMap<String, Value>>,
    ) -> Result<()> {
        todo!()
    }

    async fn save_applogs(&self, _applogs: &Vec<AppLog>) -> Result<Vec<AppLog>> {
        todo!()
    }

    /* fn get_pub<'life0, 'life1, 'async_trait>(
        &'life0 self,
        publ: Option<&'life1 String>,
    ) -> core::pin::Pin<
        Box<
            dyn core::future::Future<Output = Result<Option<crate::Publication>>>
                + core::marker::Send
                + 'async_trait,
        >,
    >
    where
        'life0: 'async_trait,
        'life1: 'async_trait,
        Self: 'async_trait,
    {
        todo!()
    }

    fn get_sub<'life0, 'life1, 'async_trait>(
        &'life0 self,
        sub_id: &'life1 String,
    ) -> core::pin::Pin<
        Box<
            dyn core::future::Future<Output = Result<Option<crate::Subscription>>>
                + core::marker::Send
                + 'async_trait,
        >,
    >
    where
        'life0: 'async_trait,
        'life1: 'async_trait,
        Self: 'async_trait,
    {
        todo!()
    }

    fn update_pub<'life0, 'life1, 'life2, 'async_trait>(
        &'life0 self,
        pub_id: &'life1 String,
        updater: Box<
            dyn FnOnce(&'life2 mut std::collections::HashMap<String, serde_json::Value>) -> (),
        >,
    ) -> core::pin::Pin<
        Box<dyn core::future::Future<Output = Result<()>> + core::marker::Send + 'async_trait>,
    >
    where
        'life0: 'async_trait,
        'life1: 'async_trait,
        'life2: 'async_trait,
        Self: 'async_trait,
    {
        todo!()
    }

    fn update_sub<'life0, 'life1, 'life2, 'async_trait>(
        &'life0 self,
        sub_id: &'life1 String,
        updater: Box<
            dyn FnOnce(&'life2 mut std::collections::HashMap<String, serde_json::Value>) -> (),
        >,
    ) -> core::pin::Pin<
        Box<dyn core::future::Future<Output = Result<()>> + core::marker::Send + 'async_trait>,
    >
    where
        'life0: 'async_trait,
        'life1: 'async_trait,
        'life2: 'async_trait,
        Self: 'async_trait,
    {
        todo!()
    }

    fn save_applogs<'life0, 'life1, 'async_trait>(
        &'life0 self,
        applogs: &'life1 Vec<crate::datalog::AppLog>,
    ) -> core::pin::Pin<
        Box<
            dyn core::future::Future<Output = Result<Vec<crate::datalog::AppLog>>>
                + core::marker::Send
                + 'async_trait,
        >,
    >
    where
        'life0: 'async_trait,
        'life1: 'async_trait,
        Self: 'async_trait,
    {
        todo!()
    } */
}
