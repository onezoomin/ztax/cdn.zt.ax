#![allow(unused_imports)]
#![feature(is_some_and, async_closure, slice_pattern)]
//async_fn_in_trait

use async_trait::async_trait;
use core::fmt;
use futures_util::future::join_all;
use futures_util::stream;
use futures_util::stream::Collect;
use futures_util::Future;
use ipfs::IpfsOptions;
use prokio::spawn_local;
use rand::{self, Rng};
use serde_json::Value;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::panic;
use std::rc::Rc;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::RwLock;
use std::thread::spawn;

use anyhow::{anyhow, bail, Context, Result};
use async_once_cell::{self, OnceCell};
use cid::Cid;
use datalog::AppLog;
use db::ApplogDB;
use futures_util::lock::Mutex;
use futures_util::StreamExt;
use futures_util::TryFutureExt;
use ipfs::IpfsDag;
use js_sys;
use log::{debug, error, info, warn};
use log::{trace, Level};
use once_cell;
use serde::{Deserialize, Serialize};
use thiserror;

use crate::datalog::{Atom, DagAppLog, DagAtom};
use crate::db::inmemory::create_inmemory_db;
use crate::ipfs::{DagLink, PublicationUpdate};
use crate::utils::map_vec_into;

pub mod datalog;
pub mod db;
pub mod ipfs;
mod pb;
pub mod utils;
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Options {
    pub ipfs: Option<IpfsOptions>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[allow(non_snake_case)]
pub struct Publication {
    pub ID: String,
    pub info: PubInfo,
    pub pos: Option<String>,
    pub ipns: Option<String>,
}

impl Publication {
    pub fn key_id(&self) -> String {
        format!("bygonz_{}", self.ID)
    }
}
#[derive(Serialize, Deserialize, Debug, Clone)]
#[allow(non_snake_case)]
pub struct PubInfo {
    pub query: String,
}
#[derive(Serialize, Deserialize, Debug, Clone)]
#[allow(non_snake_case)]
pub struct Subscription {
    pub ID: String,
    pub info: SubInfo,
    pub pos: Option<String>,
}
#[derive(Serialize, Deserialize, Debug, Clone)]
#[allow(non_snake_case)]
pub struct SubInfo {
    pub ipns: String,
}

// thread_local! {
// static INSTANCE: Bygonz = Bygonz {
//     db: OnceCell::new(),
//     ipfs: OnceCell::new(),
// };
// }

pub struct Bygonz<'a> {
    mutex: Mutex<BygonzMutexInternals<'a>>,
    ipfs: IpfsDag,
}
struct BygonzMutexInternals<'a> {
    publications: Vec<String>,
    // db: OnceCell<&'a (dyn ApplogDB + Sync)>, /*  + Send + Sync> */
    db: &'a dyn ApplogDB,
}

pub async fn init_bygonz(options: Options, db: &dyn ApplogDB) -> Result<Bygonz> {
    info!("🦀 Initializing Bygonz WASM X6");

    let ipfs = ipfs::init(options.ipfs).await.context("IPFS init")?;

    ipfs.test_connection()
        .await
        .context("IPFS connection test")?;

    let bygonz = Bygonz {
        ipfs,
        mutex: Mutex::new(BygonzMutexInternals {
            publications: vec![],
            db,
        }),
    };
    info!("Bygonz initialized 🎉");
    Ok(bygonz)
}

impl Bygonz<'_> {
    pub async fn get_applogs(&self) -> Result<Vec<AppLog>> {
        let atoms: Vec<AppLog> = vec![];
        Ok(atoms)
    }

    pub async fn publish_applogs(&self, applogs: Vec<AppLog>) -> Result<()> {
        trace!("[publish_applogs] awaiting mutex");
        let internals = self.mutex.lock().await;
        let db = &internals.db;
        let ipfs = &self.ipfs;
        debug!("Publishing new applogs: {:#?}", applogs);

        for publ_id in internals.publications.iter() {
            let matching_atoms = applogs.clone(); // HACK: for PoC - should actually check against query gate

            let publ = db
                .get_pub(Some(&publ_id))
                .await
                .expect("lookup pub in db")
                .expect(format!("should find pub in db: {}", publ_id).as_str());
            debug!("Current pub state: {:?}", publ);
            let prev = publ.pos.as_ref().and_then(|pos| {
                Some(
                    Cid::from_str(&pos.as_str())
                        .expect("Decode CID from DB")
                        .into(),
                )
            });

            let applogs: Vec<DagAppLog> = stream::iter(matching_atoms) //TODO: paralellize?
                .then(async move |applog| {
                    let atom = Atom::from(&applog);
                    let dag_atom: DagAtom = (&atom)
                        .try_into()
                        .with_context(|| format!("atom to dag_atom: {:?}", atom))?;
                    let atom_cid = ipfs
                        .dag_put(&dag_atom)
                        .await
                        .with_context(|| format!("put atom in ipfs: {:?}", dag_atom))?;
                    Ok(DagAppLog {
                        atom: atom_cid.into(),
                        tx: applog.tx,
                    })
                })
                .collect::<Vec<Result<DagAppLog>>>()
                .await
                .into_iter()
                .collect::<Result<Vec<DagAppLog>>>() // https://www.reddit.com/r/rust/comments/hc7tuo/how_to_turn_vecresult_into_resultvec/
                .expect("should put all atoms referenced by applogs");

            let cid = self
                .ipfs
                .put_entry(&PublicationUpdate { prev, applogs })
                .await
                .expect("put publication update in ipfs");

            self.ipfs
                .publish(&cid, &publ.key_id())
                .await
                .expect("should publish to ipfs");

            db.update_pub(
                &publ.ID,
                Box::new(move |mut publ_obj| {
                    publ_obj.insert("pos".into(), Value::String(cid.to_string()));
                    publ_obj
                }),
            )
            .await
            .expect("save pub pos");

            info!("Publication {:?} updated: {:?}", publ, cid);
        }

        Ok(())
    }

    // #[wasm_bindgen(typescript_type = "Publication")]
    pub async fn init_publication(&'static self, pub_id: &str) -> Result<()> {
        trace!("[init_publication] awaiting mutex");
        let mut internals = self.mutex.lock().await;
        let db = internals.db/* .read().expect("db rwlock") */;
        let publ = db
            .get_pub(Some(pub_id))
            .await
            .expect("should be able to lookup pub in DB")
            .expect("should find pub in DB");
        debug!("Initializing publication: {:#?}", publ);

        let ipns_addr = self
            .ipfs
            .ensure_key(&publ.key_id())
            .await
            .expect("should ensure ipfs key for pub");

        let old_addr = &publ.ipns;
        let pub_id = &publ.ID;
        let db = internals.db/* .read().expect("db rwlock") */;
        let update_addr = async move |new_addr| {
            debug!(
                "Updating IPNS addr of publication: {:?} -> {}",
                old_addr, new_addr
            );
            db.update_pub(
                pub_id,
                Box::new(|mut publ_obj| {
                    publ_obj.insert("ipns".into(), Value::String(new_addr));
                    publ_obj
                }),
            )
            .await
            .expect("should update pub with new ipns");
        };
        match publ.ipns {
            None => update_addr(ipns_addr).await,
            Some(ref old) => {
                if *old != ipns_addr {
                    warn!(
                        "IPNS address for '{}' has changed: {} -> {}",
                        &publ.ID, old, ipns_addr
                    );
                    update_addr(ipns_addr).await;
                }
            }
        }

        internals.publications.push(publ.ID);

        // debug!("Checking if publication {} is up-to-date", publ.name);
        // let db = DB.get().expect("DB not initialized");
        // let current_pub = db.get_pub(&publ.name).await.expect("get pub from db");
        // debug!("Current pub state: {:?}", current_pub);

        Ok(())
    }

    pub async fn init_subscription(
        &'static self,
        sub_id: String,
    ) -> Result<(
        std::sync::mpsc::Receiver<()>,
        impl Future<Output = ()> /* + Send */ + '_,
    )> {
        trace!("[init_subscription] awaiting mutex");
        let internals = self.mutex.lock().await;
        let db = internals.db/* .read().expect("db rwlock") */;
        // let self_ref = self.clone();
        let sub = db
            .get_sub(&sub_id)
            .await
            .expect("should be able to lookup sub in DB")
            .expect("should find sub in DB");
        debug!("Init subscription: {:#?}", sub);

        let (sub_update_tx, sub_update_rx) = std::sync::mpsc::channel();
        // let sub_update_tx_rc = Arc::new(Mutex::new(tx));
        let ipfs = &self.ipfs;

        let (msg_rx, msg_parse_job) = ipfs.listen_pubsub(sub.info.ipns, true).await?;
        spawn_local(msg_parse_job);

        let handle_msg_job = {
            // let on_update = on_update.clone();
            // let sub_id = sub_id.clone();
            // let tx_rc = tx_rc.clone();
            async move {
                loop {
                    debug!("Waiting for pubsub msg in handle_msg_job");
                    match msg_rx.recv() {
                        Ok((received_cid, received_entry)) => {
                            trace!("[init_publication.on_update] awaiting mutex");
                            let internals = self.mutex.lock().await;
                            debug!("PubSub: {} {:#?}", received_cid, received_entry);

                            // let on_update_rc = on_update.as_ref().clone();
                            // let on_update_here = on_update_rc.clone();
                            let ipfs: & /* 'static */ IpfsDag = &self.ipfs;
                            let db = &internals.db;
                            let received_cid = received_cid.clone();
                            let sub_id = sub_id.clone();
                            let sub_update_tx = sub_update_tx.clone();
                            let result: Result<()> = async move {
                                debug!(
                                    "Received new entry, is it new to us? {}",
                                    received_cid.to_string()
                                );
                                let current_pub = db.get_pub(None).await.expect("get pub from db");
                                let current_sub =
                                    db.get_sub(&sub_id).await.expect("get sub from db");
                                trace!(
                                    "Current state: pub={:?},sub={:?}",
                                    current_pub,
                                    current_sub
                                );
                                let mut current_pos_list: Vec<Cid> = vec![];
                                if let Some(sub_pos) = current_sub
                                    .as_ref()
                                    .and_then(|s| s.pos.clone())
                                    .and_then(|p| {
                                        Some(
                                            Cid::from_str(p.as_str())
                                                .expect("Deserialize current sub pos"),
                                        )
                                    })
                                {
                                    current_pos_list.push(sub_pos.clone());
                                }
                                if let Some(pub_pos) =
                                    current_pub.and_then(|s| s.pos).and_then(|p| {
                                        Some(
                                            Cid::from_str(p.as_str())
                                                .expect("Deserialize current pub pos"),
                                        )
                                    })
                                {
                                    current_pos_list.push(pub_pos.clone());
                                }
                                if current_pos_list.contains(&&received_cid) {
                                    debug!("Same as existing pos: {:?}", received_entry);
                                    return Ok(());
                                }

                                // Start resolving DAG
                                let mut encountered_prev_sub_pos = false;
                                let mut all_logs = received_entry.applogs.clone();
                                let mut current_entry = received_entry;
                                while let Some(prev_cid) =
                                    current_entry.prev.and_then(|link| Some(link.cid))
                                {
                                    if current_pos_list.contains(&&prev_cid) {
                                        trace!("Found previous pos");
                                        encountered_prev_sub_pos = true;
                                        break;
                                    }
                                    debug!("Also fetching previous: {:?}", prev_cid);
                                    current_entry =
                                        ipfs.load(prev_cid).await.with_context(|| {
                                            format!(
                                                "Loading {} (ancestor of {})",
                                                prev_cid, received_cid
                                            )
                                        })?;
                                    all_logs.append(&mut current_entry.applogs.clone());
                                }
                                if !encountered_prev_sub_pos {
                                    if let Some(pos) = current_sub.and_then(|s| s.pos) {
                                        warn!(
                                            "Did not encounter previous sub position {} while traversing {}",
                                            pos.to_string(),
                                            received_cid.to_string()
                                        )
                                    }
                                }
                                let resolved_applogs: Vec<AppLog> = stream::iter(all_logs)
                                    .then(async move |log| {
                                        let dag_atom = ipfs
                                            .dag_get::<DagAtom>(log.atom.cid)
                                            .await
                                            .with_context(|| {
                                                format!(
                                                    "Loading atom {} (tx: {})",
                                                    log.atom.cid.to_string(),
                                                    log.tx
                                                )
                                            })?;
                                        let atom: Atom = (&dag_atom).into();
                                        Ok(AppLog {
                                            tx: log.tx,
                                            ha: log.atom.cid.to_string(),
                                            ag: atom.ag.clone(),
                                            pv: atom.pv.clone(),
                                            en: atom.en.clone(),
                                            at: atom.at.clone(),
                                            ts: atom.ts.clone(),
                                            vl: atom.vl.clone(),
                                            op: atom.op.clone(),
                                        })
                                    })
                                    .collect::<Vec<Result<AppLog>>>()
                                    .await
                                    .into_iter()
                                    .collect::<Result<Vec<AppLog>>>() // https://www.reddit.com/r/rust/comments/hc7tuo/how_to_turn_vecresult_into_resultvec/
                                    .with_context(|| {
                                        format!(
                                            "Fetch DagAtoms referenced in applogs of pub update {}",
                                            received_cid
                                        )
                                    })?;

                                // Save all to DB and check if they're new
                                let new_atoms = db.save_applogs(&resolved_applogs).await?;
                                debug!(
                                    "Assimilated {} applogs from publication update ({} were new to us)",
                                    resolved_applogs.iter().count(),
                                    new_atoms.iter().count()
                                );
                                db.update_sub(
                                    &sub_id,
                                    Box::new(move |mut sub_obj| {
                                        sub_obj.insert(
                                            "pos".into(),
                                            Value::String(received_cid.to_string()),
                                        );
                                        sub_obj
                                    }),
                                )
                                .await?; //TODO: check if actually newer (as opposed to older) - i.e. merge with ours?

                                if !new_atoms.is_empty() {
                                    // on_update_here();
                                    sub_update_tx /* _rc.lock().await */
                                        .send(())
                                        .expect("send tx_rc");
                                }
                                Ok(())
                            }
                            .await;
                            if let Err(err) = result {
                                error!("Failed to process pubsub event: {:?}", err);
                            }
                        }
                        Err(err) => {
                            error!("Subscription update receive error {}", err);
                            break;
                        }
                    }
                }
            }
        };

        Ok((sub_update_rx, handle_msg_job))
    }
}

// #[wasm_bindgen]
// #[derive(Debug)]
// pub struct BygonzError {
//     display: String,
// }
// impl std::error::Error for BygonzError {}

// impl fmt::Display for BygonzError {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         f.write_str(self.display.as_str())
//     }
// }
// impl From<anyhow::Error> for BygonzError {
//     fn from(value: anyhow::Error) -> Self {
//         BygonzError {
//             display: value.to_string(),
//         }
//     }
// }

// // #[wasm_bindgen]
// #[derive(thiserror::Error, Debug)]
// #[error(transparent)]
// pub struct BygonzError(#[from] anyhow::Error);

#[async_trait]

trait AsyncRuntime {
    fn spawn_background(&self, future: Box<dyn Future<Output = ()> + Send + 'static>);
}
