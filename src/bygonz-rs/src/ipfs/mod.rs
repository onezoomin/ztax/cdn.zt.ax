use crate::datalog::{Atom, DagAppLog};
use crate::{datalog::AppLog, utils::map_vec_into};
use anyhow::{bail, ensure, Context, Result};
use base64::URL_SAFE_NO_PAD;
use cid::{Cid, CidGeneric};
use core::slice::SlicePattern;
use futures_util::future::ready;
use futures_util::{future::AbortHandle, future::FutureExt, join, StreamExt};
use futures_util::{Future, TryFutureExt};
use ipfs_api::DEFAULT_URI;
use ipfs_api::{
    responses::{Codec, PinMode},
    IPNSAddress, IpfsService,
};
use log::{debug, error, info, trace, warn};
use multibase::decode;
use multibase::Base;
use once_cell::sync::Lazy;
use std::fmt;

use prokio::spawn_local;
use protobuf::Message;
use regex::Regex;
use serde::de::DeserializeOwned;
use serde::{de, ser};
use serde::{Deserialize, Serialize};
use serde_ipld_dagcbor;
use serde_json::Value;
use std::collections::HashMap;
use std::marker::PhantomData;
use std::ops::Deref;
use std::str::FromStr;
use std::sync::{Arc, Mutex};

use crate::pb::ipns::{self, ipns_entry, IpnsEntry};

static REGEX_IPNS_RECORD_VAL: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"^/ipfs/([A-Za-z0-9+=]+)$").unwrap());

#[derive(Serialize, Deserialize, Debug, Clone)]
#[allow(non_snake_case)]
pub struct IpfsOptions {
    rpcUrl: Option<String>,
    // channel: &'static str,
}

pub struct IpfsDag {
    client: IpfsService,
    // channel: &'static str,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PublicationUpdate {
    pub prev: Option<DagLink<PublicationUpdate>>,
    pub applogs: Vec<DagAppLog>,
}

pub async fn init(options: Option<IpfsOptions>) -> Result<IpfsDag> {
    debug!("ipfs init");
    let url = match options.and_then(|opt| opt.rpcUrl) {
        Some(url) => {
            let url = url.trim_end_matches('/');
            format!("{}/api/v0/", url)
        }
        None => DEFAULT_URI.into(),
    };
    Ok(IpfsDag {
        // channel: &"bygonz_test",
        client: IpfsService::new(url.as_str()).expect("IPFS client init"),
    })
    // static INSTANCE: Lazy<IpfsDag> = Lazy::new(|| IpfsDag {
    //     channel: &"bygonz_test",
    //     client: IpfsService::default(),
    // });

    // spawn_local(async move {

    // });

    // debug!("ipfs init done");
    // Ok(&INSTANCE)
}

impl IpfsDag {
    pub async fn listen_pubsub(
        &self,
        ipns_addr: String,
        initial_lookup: bool,
        // on_new_entry: F,
    ) -> Result<(
        std::sync::mpsc::Receiver<(Cid, PublicationUpdate)>,
        impl Future<Output = ()> /* + Send */ + '_,
    )>
//-> Result<std::sync::mpsc::Receiver<(Cid, PublicationUpdate)>>
        //-> Result<impl Future<Output = ()> + 'static + Send>
        // where
        //     F: Fn(Cid, PublicationUpdate) -> () + 'static,
    {
        let ipns_cid =
            Cid::from_str(&ipns_addr).with_context(|| format!("parse ipns address {}", ipns_addr));
        if let Err(err) = ipns_cid {
            bail!("Failed to parse ipns address '{}': {}", ipns_addr, err);
            // return;
        }
        let ipns_cid = ipns_cid.expect("we checked")/* .into_v1() */;

        // I hacked this (ipns pubsub decoding) together from:
        // - https://github.com/aschmahmann/ipns-utils/issues/1
        // - https://github.com/tennox/ipns-utils/blob/master/main.go#L548
        // - https://github.com/libp2p/go-libp2p-pubsub-router/blob/065ef577a7f342f13bc8c5a7a1df00d8a921f233/pubsub.go#L72
        // - ChatGPT (which did not prove to be true in any meaningful way, but helped me with the right syntax at least 😅)
        let mut ipns_raw = "/ipns/".as_bytes().to_vec();
        ipns_raw.append(&mut ipns_cid.hash().to_bytes());
        let encoded_ipns = Base::Base64Url.encode(ipns_raw);
        let topic = format!("/record/{}", encoded_ipns);
        trace!("PubSub encoded /ipns/{} => '{}'", ipns_addr, topic);

        let (tx, rx) = std::sync::mpsc::channel::<(Cid, PublicationUpdate)>();

        // Spawn PubSub listener
        let (msg_tx, msg_rx) = std::sync::mpsc::channel();
        spawn_local({
            let client = self.client.clone();
            let ipns_addr = ipns_addr.clone();
            async move {
                let mut stream = /* Arc::new(Mutex::new( */
                client
                    .pubsub_sub(topic.as_bytes().to_owned()) // .take(1),
                    .boxed_local()/* ,
            )) */;
                debug!("PubSub subscription started for '{}'", ipns_addr);

                while let Some(msg) = stream.next().await {
                    // let ref on_new_entry = on_new_entry;
                    match msg {
                        Ok(msg) => {
                            msg_tx.send(msg).expect("should send on msg_tx");
                        }
                        Err(err) => error!("Failed to processpubsub entry - {:?}", err),
                    }
                }
                warn!("PubSub listener Stopped");
            }
        });

        let job = {
            async move {
                if initial_lookup {
                    let lookup_result = self.resolve_current_entry(&ipns_addr).await;
                    debug!("current pubsub entry: {:?}", lookup_result);
                    match lookup_result {
                        Ok((current_cid, current_entry)) => tx
                            .send((current_cid, current_entry))
                            .expect("tx->send pubsub update"),
                        Err(err) => error!(
                            "Failed to get current pubsub entry for '{}': {}",
                            ipns_addr, err
                        ),
                    }
                }

                loop {
                    debug!("Waiting for pubsub in internal loop");
                    match msg_rx.recv() {
                        Ok(msg) => {
                            debug!("PubSub: {:#?}", msg);

                            let process_result = self.parse_pubsub(msg).await;
                            match process_result {
                                Ok((current_cid, current_entry)) => tx
                                    .send((current_cid, current_entry))
                                    .expect("tx->send pubsub update"),
                                Err(err) => {
                                    error!("Failed to process pubsub msg because of {:?}", err)
                                }
                            };
                        }
                        Err(err) => {
                            error!("PubSub update receive error {}", err);
                            break;
                        }
                    }
                }
            }
        };

        Ok((rx, job))
    }

    async fn parse_pubsub(
        &self,
        msg: ipfs_api::responses::PubSubMessage,
    ) -> Result<(CidGeneric<64>, PublicationUpdate)> {
        let ipns_record =
            IpnsEntry::parse_from_bytes(&msg.data.as_slice()).context("parse pubsub message");
        trace!("Received pubsub msg from {}: {:?}", msg.from, ipns_record);
        trace!("Is it me? {:?}", self.client.key_list().await);
        //TODO: skip if me

        Ok(match ipns_record {
            Ok(record) => match record.value {
                Some(value) => {
                    let ipns_value = String::from_utf8(value.clone()).with_context(|| {
                        format!("utf8 decode ipns value: {}", base64::encode(value))
                    })?;
                    let re_match = REGEX_IPNS_RECORD_VAL
                        .captures(ipns_value.as_str())
                        .with_context(|| format!("regex parse the ipns value: {}", ipns_value))?;
                    let cid_str = re_match
                        .get(1)
                        .map(|m| m.as_str())
                        .with_context(|| format!("get first group from regex: {}", ipns_value))?;
                    let cid = Cid::try_from(cid_str)
                        .with_context(|| format!("decode CID from record value: {}", cid_str))?;
                    let entry: PublicationUpdate = self
                        .load(cid)
                        .await
                        .with_context(|| format!("Load publication update: {}", cid.to_string()))?;
                    trace!("new entry: {:?}", entry);
                    (cid, entry)
                }
                None => bail!("No value on ipns record"),
            },
            Err(err) => {
                // TODO: remove unused debugging steps
                let str = String::from_utf8(msg.data.clone());
                debug!("decode str? {:?}", str);
                debug!(
                    "decode cbor? {:?}",
                    serde_ipld_dagcbor::from_slice::<HashMap<String, Value>>(msg.data.as_slice())
                );
                debug!(
                    "decode json? {:?}",
                    serde_json::from_slice::<HashMap<String, Value>>(&msg.data.as_slice())
                );
                if let Ok(str) = str {
                    debug!("decode multibase? {:?}", decode(&str));
                }
                let ipns_entry = IpnsEntry::parse_from_bytes(&msg.data.as_slice());
                debug!("decode protobuf? {:#?}", ipns_entry);
                if let Ok(entry) = ipns_entry {
                    debug!(
                        "IPNS value - string ?{:?}",
                        entry.value.clone().map(|val| String::from_utf8(val))
                    );
                    debug!(
                        "IPNS value - cid ?{:?}",
                        entry.value.clone().map(|val| Cid::try_from(val))
                    );
                }

                bail!(
                    "Failed to process pubsub msg because of {:?}\nmsg(from={}): {:?}",
                    err,
                    msg.from,
                    base64::encode(msg.data)
                )
            }
        })
    }

    pub async fn resolve_current_entry(
        &self,
        ipns_addr: &String,
    ) -> Result<(Cid, PublicationUpdate)> {
        let current_cid = self
            .client
            .name_resolve(
                IPNSAddress::try_from(ipns_addr.clone())
                    .with_context(|| format!("Parsing pubsub channel address {}", ipns_addr))?,
            )
            .await
            .with_context(|| format!("Look up current pubsub value {}", ipns_addr))?;
        debug!("Found current pubsub value: {}", current_cid);
        Ok((
            current_cid,
            self.load(current_cid)
                .await
                .with_context(|| format!("Load publication update: {}", current_cid.to_string()))?,
        ))
    }

    pub async fn load(&self, cid: Cid) -> Result<PublicationUpdate> {
        let entry = self
            .dag_get(cid)
            .await
            .with_context(|| format!("load publication update: {}", cid.to_string()))?;

        trace!("Loaded LogEntry: {:?}", entry);
        // let atoms: Vec<Atom> = map_vec_into(&log.atoms);
        // trace!("Converted dagAtoms to atoms {:?}", atoms);

        // let head_cid = (&log["heads"][0]["/"])
        //     .as_str()
        //     .expect("find head cid in log dag");
        // debug!("Log Head: {:?}", head_cid);
        // let head = self.dag_get(head_cid).await.expect("should get head");

        Ok(entry)
    }

    pub async fn put_entry(&self, entry: &PublicationUpdate) -> Result<Cid> {
        debug!("Putting entry {:?}", entry);
        // debug!("Converted atoms to dagAtoms {:?}", atoms);
        let cid = self.dag_put(entry).await.expect("put entry in dag");
        trace!("Entry dag cid: {:?}", cid);
        Ok(cid)
    }

    pub async fn publish(&self, cid: &Cid, key_id: &String) -> Result<()> {
        debug!("Publishing update for '{}' {:?}", key_id, cid);

        // self.client
        //     .pubsub_pub(self.channel, cid.to_bytes())
        //     .await
        //     .expect("should successfully publish new log entry");
        // trace!("Published log dag");

        debug!("Publishing new IPNS record: {}", cid.to_string());
        let ipns_result = self
            .client
            .name_publish(
                *cid,
                key_id.clone(),
                Some(&[
                    ("lifetime", "1h"),
                    // ("offline", "true"),
                ]),
            )
            .await
            .expect("should successfully publish new log entry"); //TODO: don't panic on titanic (whole wasm crashes otherwise)
        debug!("IPNS update result: {:?}", ipns_result);
        Ok(())
    }

    pub async fn ensure_key(&self, key_id: &String) -> Result<String> {
        trace!("Ensuring key: '{}'", key_id);
        let keys = self
            .client
            .key_list()
            .await
            .expect("should get IPFS key list");

        trace!("IPFS key list: {:?}", keys);
        match keys.get(key_id) {
            Some(value) => {
                debug!(
                    "key already exists '{}': {:?} (pubsub={})",
                    key_id,
                    value,
                    value.to_pubsub_topic()
                );
                Ok(value.to_string())
            }
            None => {
                let ipns_result = self
                    .client
                    .key_gen(key_id.clone())
                    .await
                    .with_context(|| format!("generate key for {}", key_id))?;
                debug!("IPNS key gen result: {:?}", ipns_result);
                Ok(ipns_result.id)
            }
        }
    }

    pub async fn dag_get<T>(&self, cid: Cid) -> Result<T>
    where
        T: ?Sized + DeserializeOwned,
    {
        let value = self
            .client
            .dag_get::<&str, Value>(cid, Option::<&str>::None)
            .await
            .with_context(|| format!("dag_get.pre-json: {}", cid.to_string()))?;

        debug!("[dag_get] {} => {:?}", cid.to_string(), value);
        Ok(serde_json::from_value(value.clone())
            .with_context(|| format!("dag_get.decode: {:?}", value))?)
        /* Ok(self
        .client
        .dag_get::<&str, T>(cid, Option::<&str>::None)
        .await
        .with_context(|| format!("dag_get: {}", cid.to_string()))?) */
    }
    pub async fn dag_put<T>(&self, data: &T) -> Result<Cid>
    where
        T: ?Sized + Serialize + std::fmt::Debug,
    {
        // let serialized = serde_json::to_string(&data).expect("should serialize atoms to JSON");
        let cid = self
            .client
            .dag_put(&data, Codec::DagJson)
            .await
            .with_context(|| format!("dag_put: {:?}", data))?;
        // let keys: Vec<&String> = cid.keys().collect();
        // if keys.len() != 1 {
        //     bail!("Unexpected result of dag_put: {:?}", keys)
        // }
        // let cid = cid
        //     .get("/")
        //     .expect(format!("Missing '/' in cid result: {:?}", keys).as_str());
        Ok(cid)
    }

    pub async fn test_connection(&self) -> Result<()> {
        // debug!("Converted atoms to dagAtoms {:?}", atoms);
        let client_info = self.client.peer_id().await.context("IPFS get peer id")?;
        debug!("IPFS client: {:?}", client_info);
        Ok(())
    }
}

#[derive(Copy, Clone)]
pub struct DagLink<T> {
    type_for_later_todo: PhantomData<T>, //TODO: figure out how not to do this - https://www.google.com/search?hl=en&q=rust%20struct%20Unused%20type%20parameters
    pub cid: Cid,
}

impl<T> fmt::Debug for DagLink<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "DagLink{{{}}}", self.cid)
    }
}

// impl<T> DagLink<T> {
//     fn type_for_later_todo() -> Result<T> {
//         unimplemented!("TODO")
//     }
// }

impl<T> From<CidGeneric<64>> for DagLink<T> {
    fn from(value: CidGeneric<64>) -> Self {
        DagLink {
            type_for_later_todo: PhantomData,
            cid: value,
        }
    }
}

impl<T> Serialize for DagLink<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map: HashMap<String, String> = HashMap::new();
        map.insert("/".into(), self.cid.to_string());
        map.serialize(serializer)
    }
}
impl<'de, T> Deserialize<'de> for DagLink<T> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let mut map: HashMap<String, String> = HashMap::deserialize(deserializer)?;

        let mut cid: Option<Cid> = None;
        for (k, v) in map.drain().take(1) {
            assert!(k == "/", "DagLink key != '/': {}", k);
            if cid.is_some() {
                return Err(de::Error::custom("DagLink map has multiple entries"));
            }
            cid = Some(Cid::from_str(v.as_str()).expect("Deserialize DagLink value"));
        }
        match cid {
            Some(cid) => Ok(DagLink {
                cid,
                type_for_later_todo: PhantomData,
            }),
            None => Err(de::Error::custom("DagLink map has no entries")),
        }
    }
}

// fn atoms_to_dag(atoms: Vec<Atom>) -> Vec<DagAtom> {
//     atoms.into_iter().map(|a: Atom| a.into()).collect()
// }
// fn dag_to_atoms(atoms: Vec<DagAtom>) -> Vec<Atom> {
//     atoms.into_iter().map(Atom::from).collect()
// }
