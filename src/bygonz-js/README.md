# Bygonz

Remember everything, until you don't want to.

## Offline First History Tracking

| Features | Demos |
| ------ | ------ |
| - Extends Dexie with "appLog" modification History | - [Stackblitz using vitesse](https://stackblitz.com/edit/vitesse-lite-bygonz?file=src%2Fcomponents%2FFriendList.vue)
| - Syncronizes modifications via webnative          | - [Main Folder in monorepo](https://gitlab.com/onezoomin/ztax/cdn.zt.ax/-/tree/trunk/src/bygonz-js)
|   - Syncronizes state via modification log         | - [Demo with webnative](https://f.cdn.zt.ax/apps/color-schemes/)
|   - point in time "as of" querying                 | - [Test demo](https://f.cdn.zt.ax/bygonz-js/test.html) - check the console and IndexDB tables


![Bygonz_whitebg_withFissionLogo](https://gitlab.com/onezoomin/ztax/ztax/uploads/ce4e1dd83b4544d1c03a38af9cc24bf1/Bygonz_whitebg.png)

## Usage (wip)
Bygonz aims to give you easily accessible entity history "out of the box",   
without the need for any out of the box thinking.

DB class can [extend BygonzDexie](https://gitlab.com/onezoomin/ztax/cdn.zt.ax/-/blob/trunk/apps/color-schemes/data/dexie.ts#L43)

ViewModel Entity Classes can [extend WithHistory](https://gitlab.com/onezoomin/ztax/cdn.zt.ax/-/blob/trunk/apps/color-schemes/data/Model/Color.ts#L35)

ViewModel Instances can use [getEntityAsof(timeStamp)](https://gitlab.com/onezoomin/ztax/cdn.zt.ax/-/blob/trunk/apps/color-schemes/components/color-box.ts#L46)

the API is barely alpha level stability...  
feel free to play and give feedback,  
but please don't rely on anything yet.
