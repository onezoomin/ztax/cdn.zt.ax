import type { Table } from 'dexie'
import { Mixin } from 'ts-mixer'
import { Logger } from '@ztax/logger'
import { BygonzDexie, WithHashID, WithHistory } from './index'
import type { DexiePlusParams } from './index'

const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export const dbName = 'bygonz_test_FriendsDB'
// const { BygonzDexie, WithHashID, WithHistory } = bygonz // this style creates a private name ts error?? https://stackoverflow.com/questions/36308973/public-property-of-exported-class-has-or-is-using-private-name

export class Friend extends Mixin(WithHashID) { // TimeStampedBase, ModWho,
  name: string
  age: number

  constructor(obj: any) {
    super(obj) // all Mixedin supers are called in left to right order (so including the timestamp first before hashing withh prevent key collision)
    Object.assign(this, obj)
  }
}

export class Connection extends Mixin(WithHashID) { // TimeStampedBase, ModWho,
  fromFriend: string
  toFriend: string

  constructor(obj: any) {
    super(obj)
    Object.assign(this, obj)
  }
}
export class ConnectionVM extends Mixin(Connection, WithHistory) {
  // get tablePath () { return `${dbName}_Connections` }
}
export class FriendVM extends Mixin(Friend, WithHistory) {
  // get tablePath () { return `${dbName}_Friends` }

  get ageClass() {
    return this.age > 80
      ? 'oldtimer'
      : this.age > 50
        ? 'mature'
        : this.age > 18 ? 'adult' : 'young'
  }

  async getAllAppLogsForName() {
    const fullNameHistory = await this.getAttributeHistory<FriendVM>('name', ['*'])
    VERBOSE('test for TS intellisense type . after => eachEntry to check it out', fullNameHistory.map(eachEntry => eachEntry))
    return fullNameHistory
  }

  async getNameHistory() {
    return await this.getAttributeHistory('name')
  }

  async getPreviousNamesArray() {
    return await this.getAttributeHistoryArray('name')
  }
}

export class FriendsDB extends BygonzDexie {
  // Declare implicit table properties. (just to inform Typescript. Instanciated by Dexie in stores() method)
  // {entity}Params | {entity}VM allows for partial objects to be used in add and put and for the class of retrieved entities to include getters
  public Friends: Table<FriendVM, 'ID'>
  public Connections: Table<Connection, 'ID'>

  constructor(...params: DexiePlusParams) {
    super(...params)
    this.doMappings()
  }
}

const stores = {
  Friends: 'ID, name, age',
  Connections: 'ID, fromFriend, toFriend',
}
const mappings = {
  Friends: FriendVM,
  Connections: ConnectionVM,
}

export const friendsDB = new FriendsDB(dbName, stores, mappings)
