import type { Table } from 'dexie'
import { Logger } from '@ztax/logger'
import { sleep } from './WebWorkerImports/Utils'
import type { Subscription, SubscriptionTable } from './WebWorkerImports/Subscriptions'
import { saturateAppLogsFromAllSubscriptions } from './WebWorkerImports/Subscriptions'
import type { PublicationTable } from './WebWorkerImports/Publications'
import type { AppLogDB, AppLogObj, AppLogVM } from './WebWorkerImports/AppLog'
import type { DexiePlusParams } from './DexiePlus'

import { DexiePlus } from './DexiePlus'
import { getBygonzMiddlwareFor } from './BygonzMiddleware'
import { getAppLogDB } from './WebWorkerImports/AppLog'
// @ ts-expect-error
// import BygonzWebWorker from './BygonzWebWorker.worker.ts' // depends on esbuild-plugin-inline-worker
// import './BygonzWebWorker.ts'

import type { SituationTable } from './WebWorkerImports/Situations'
import type { AppLogUpdateBroadcastMessage, ErrorBroadcastMessage } from './BygonzWebWorker'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

// const BygonzWorker = BygonzWebWorker() // depends on esbuild-plugin-inline-worker
let BygonzWorker
const instantiateWebWorker = () => {
  if (BygonzWorker)
    return VERBOSE('additional call to instantiateWebWorker')
  const metaUrl = import.meta.url // Hacky workaround for some extra-smart compiler optimization that changes import.meta.url to self.location when it's used inside new URL(...)
  const workerUrl = new URL('./BygonzWebWorker.mjs', metaUrl)
  DEBUG('Instantiatning WebWorker:', workerUrl, {
    metaUrl: import.meta.url,
    testManual: new URL('./BygonzWebWorker.mjs', 'http://localhost:5173/@fs/home/.../index.mjs'),
  })
  BygonzWorker = new Worker(workerUrl, {
    type: 'module',
  })
  DEBUG('Instantiated WebWorker:', BygonzWorker)
}

// const BygonzWorker = new Worker('../../bygonz/BygonzWebWorker.mjs', { type: 'module' }) // requires that build tool places app and lib in known dir structure

export class BygonzDexie extends DexiePlus {
  appLogDB: AppLogDB
  nonBygonzDB: DexiePlus
  appLogTables: { [tableName: string]: Table<AppLogVM, string> } = {}
  subscriptionTable: SubscriptionTable
  publicationsTable: PublicationTable
  situationTable: SituationTable
  updateListeners: Array<(message: AppLogUpdateBroadcastMessage) => void> = []

  constructor(...params: DexiePlusParams) {
    const [dbName, ...restParams] = params
    super(dbName, ...restParams)
    this.nonBygonzDB = new DexiePlus(dbName, ...restParams)
    if (self.document !== undefined)
      this.use(getBygonzMiddlwareFor(this))
    // doMappings() and this.setup() should be called by the end consumer constructor

    void this.getAppLogDB()
  }

  async triggerSubscriptionSync() {
    WARN('TODO triggerSubscriptionSync')
    // return await saturateAppLogsFromAllSubscriptions(this)
    // await BygonzWorker.triggerSync()
    // this.workerApi.postMessage({ cmd: 'sync' })
    // this.workerApi.
  }

  async sendWebnativePerms(permissions, username): Promise<void> { // HACK move to wnfs specific location
    this.workerApi.postMessage({ cmd: 'webnativePermissions', permissions, username })
  }

  async getSituationTable(): Promise<SituationTable> {
    if (!this.appLogDB)
      this.appLogDB = await getAppLogDB()
    if (!this.situationTable)
      this.situationTable = this.appLogDB?.Situations ?? this.appLogDB?._allTables?.Situations
    if (!this.situationTable)
      throw new Error('couldnt find this.situationTable')
    return this.situationTable
  }

  async getSubscriptionTable(): Promise<SubscriptionTable> {
    if (!this.appLogDB)
      this.appLogDB = await getAppLogDB() // TODO avoid duplicate code
    if (!this.subscriptionTable)
      this.subscriptionTable = this.appLogDB?.Subscriptions ?? this.appLogDB?._allTables?.Subscriptions
    if (!this.subscriptionTable)
      throw new Error('couldnt find this.subscriptionTable')
    return this.subscriptionTable
  }

  async getPublicationTable(): Promise<SubscriptionTable> {
    if (!this.appLogDB)
      this.appLogDB = await getAppLogDB() // TODO avoid duplicate code
    if (!this.publicationsTable)
      this.publicationsTable = this.appLogDB?.Publications ?? this.appLogDB?._allTables?.Publications
    if (!this.publicationsTable)
      throw new Error('couldnt find this.subscriptionTable')
    return this.publicationsTable
  }

  async getSubscriptionArray() {
    return await (this.subscriptionTable ?? (await this.getSubscriptionTable())).toArray()
  }

  async getPublicationsArray() {
    return await (this.publicationsTable ?? (await this.getPublicationTable())).toArray()
  }

  async getAppLogDB() {
    if (this.appLogDB)
      return this.appLogDB
    this.appLogDB = await getAppLogDB(this)
    return this.appLogDB
  }

  async getEntitiesAsOf(asOf?: number) {
    return await (await this.getAppLogDB()).getEntitiesAsOf(asOf)
  }

  async setup(agentID: string) {
    DEBUG('setup')
    if (self.document === undefined)
      return VERBOSE('avoiding infinite loop by not setting up worker if not in main thread')
    if (!agentID)
      WARN('Apps should be agent aware and pass agentID to setup(), now defaulting to', agentID = 'defaultAgentFromSetup')

    this.activeAgent = agentID
    DEBUG('main thread, setting up middleware and spawning worker', { thisRef: this })

    instantiateWebWorker()

    this.subscriptionTable = (await this.getAppLogDB()).Subscriptions
    this.publicationsTable = (await this.getAppLogDB()).Publications

    if (!this.subscriptionTable)
      WARN('no subsTable', { thisRef: this, appLogDBTables: this.appLogDB._allTables })

    else
      this.options?.subscriptions?.length && !(await this.subscriptionTable.count()) && (await this.subscriptionTable.bulkAdd(this.options.subscriptions))

    if (!this.publicationsTable)
      WARN('no pubsTable', { thisRef: this, appLogDBTables: this.appLogDB._allTables })

    else
      this.options?.publications?.length && !(await this.publicationsTable.count()) && (await this.publicationsTable.bulkAdd(this.options.publications))

    if (this.options.prePopulateTables) {
      const prePop = this.options.prePopulateTables
      for (const eachTable of Object.keys(prePop)) {
        const withArray = prePop[eachTable]
        if (Object.keys(this.stores).includes(eachTable)) {
          DEBUG('Prepopulate', { eachTable, withArray })
          await this._allTables[eachTable].bulkAdd(withArray)
        }
        else {
          WARN('Prepopulate stores does not include Table', { eachTable, withArray })
        }
      }
    }

    // }
    // for (const eachTable of Object.keys(this.stores)) {
    //   DEBUG('bygUI trying to get log table', eachTable)
    //   this.appLogTables[eachTable] = await this.appLogDB.getTable(`${this.name}_${eachTable}`)
    // }

    // const { default: BygonzWorker } = await import('./BygonzWebWorker.ts?worker&inline')
    // this.workerApi = new Worker(BygonzWorkerURL, { type: 'module' })
    this.workerApi = BygonzWorker
    this.workerApi.onmessage = this.onWorkerMsg.bind(this)
    this.workerApi.postMessage({
      cmd: 'init',
      dbName: this.name,
      stores: this.stores,
      options: {
        activeAgent: agentID,
        subscriptions: this.options.subscriptions,
        publications: this.options.publications,
        ipfs: this.options.ipfs,
      },
    }) // init
    // console.log('spawned', this.workerApi)
  }

  async addSubscription(subscription: Subscription) {
    await (await this.getAppLogDB()).Subscriptions.add(subscription)
    this.workerApi.postMessage({
      cmd: 'addSubscription',
      subscription,
    }) // init
  }

  async updateDBOnNewAppLogs(_event: AppLogUpdateBroadcastMessage) {
    throw new Error('ABSTRACT')
  }

  async onWorkerMsg(msgEvent) {
    DEBUG('MSG from Worker', msgEvent)
    if (msgEvent.data.event === 'updated') {
      try {
        const msg = (msgEvent.data as AppLogUpdateBroadcastMessage)
        await this.updateDBOnNewAppLogs(msg)
        for (const listener of this.updateListeners) {
          DEBUG('Calling listener with', msg.data, { listener })
          await listener(msg) // TODO: convert to AppLogVM?
        }
      }
      catch (err) { ERROR('Failed to handle update event:', err, { msg: msgEvent }) }
    }
    else if (msgEvent.data.event === 'error') {
      try {
        const { cmd: errorCmd, error } = (msgEvent.data as ErrorBroadcastMessage).data
        ERROR(`WebWorker failed failed to run command '${errorCmd}':`, error)
        await this.onError(
          error,
        )
      }
      catch (err) { ERROR('Failed to handle update event:', err, { msg: msgEvent }) }
    }
    else { throw new Error(`Invalid WW event ${msgEvent.data.event}`) }
  }

  async onError(error) {
    WARN('Unhandled bygonz error since BygonzDexie.onError is not overridden', error)
  }

  addUpdateListener(listener: (data: { added: AppLogVM[] }) => Promise<void> | void) {
    this.updateListeners.push(listener)
  }
}

const _notes = `

https://github.com/dexie/Dexie.js/blob/master/src/live-query/observability-middleware.ts
https://github.com/dexie/Dexie.js/blame/master/src/hooks/hooks-middleware.ts
https://github.com/dexie/Dexie.js/blob/master/src/live-query/live-query.ts

https://github.com/dexie/Dexie.js/blob/master/src/live-query/enable-broadcast.ts

`
