import * as IpldBlock from 'multiformats/block'
import { sha256 } from 'multiformats/hashes/sha2'
import * as DagJson from '@ipld/dag-json'
import { CID } from 'multiformats/cid'
import { base32 } from 'multiformats/bases/base32'
import { difference } from 'lodash-es'
import { Logger } from '@ztax/logger'
import type { Atom } from './WebWorkerImports/AppLog'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export const ATOM_FIELDS = ['ag', 'ts', 'pv', 'en', 'at', 'vl', 'op']

/**
 * Encode as Dag-Json
 *
 *  Return value:
 *  - `block.value` == value
 *  - `block.bytes` --> Uint8Array
 *  - `block.cid` --> CID() w/ sha2-256 hash address and dag-cbor codec
 *  - cid string: `block.cid.toString()`
 *
 * @param value
 * @returns
 */
export async function encodeDagJson(value: unknown) {
  // From: https://github.com/multiformats/js-multiformats#creating-blocks
  return await IpldBlock.encode({
    value,
    codec: DagJson,
    hasher: sha256,
  })
}

export function atomToDagAtom(atom: Atom) {
  const fieldCheckDiff = difference(Object.keys(atom), ATOM_FIELDS)
  if (fieldCheckDiff.length)
    throw new Error(`Invalid atom fields: ${Object.keys(atom).toString()} - difference: ${fieldCheckDiff.toString()}`)
  DEBUG('dagAtom for', atom)
  return {
    ...atom,
    pv: atom.pv && CID.parse(atom.pv, base32),
  }
}
