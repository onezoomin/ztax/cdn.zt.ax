import { DexiePlus } from './DexiePlus'
import { AppLogDB, getAppLogDB } from './WebWorkerImports/AppLog'
import { msgHandlers } from './WebWorkerImports/Handlers'
import { handleWebnativePermissionsFromUI } from './WebWorkerImports/SubPub/webnative-utils'
import { BYGONZ_MUTATION_EVENT_NAME } from './WebWorkerImports/Utils'
// import { createWnfsSubHandler } from './WebWorkerImports/SubPub'
import { Logger } from '@ztax/logger'
import { Publication } from './WebWorkerImports/Publications'
import { BygonzPub, BygonzSub } from './WebWorkerImports/SubPub/BygonzSubPub'
import { createIpfsPubHandler, createIpfsSubHandler } from './WebWorkerImports/SubPub/ipfs-wasm'
import { Subscription } from './WebWorkerImports/Subscriptions' // eslint-disable-line @typescript-eslint/no-unused-vars

// import { BygonzDexie } from './Bygonz'

export interface BroadcastMessageType {
  event: 'updated' | 'error'
}
export interface ErrorBroadcastMessage extends BroadcastMessageType {
  event: 'error'
  data: {
    cmd: string | undefined
    error: any
  }
}
export interface AppLogUpdateBroadcastMessage extends BroadcastMessageType {
  event: 'updated'
  data: {}
}

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[WW]', performanceEnabled: false })

// eslint-disable-next-line no-var, @typescript-eslint/no-unused-vars
var window = globalThis

let targetDB: DexiePlus | undefined
let appLogDB: AppLogDB | undefined
let userName
let wnfs
let isKnownMsgType
const subHandlers = new Map<String, BygonzSub>() // TODO: use name as key?
const pubHandlers = new Map<String, BygonzPub>()
const appLogTables = []

DEBUG('Startup', { window })
try {
  self.onmessage = async (messageEvent) => {
    if (messageEvent.data.type === 'application/x-postmate-v1+json') return VERBOSE('Ignoring logseq postmate message:', messageEvent)
    try {
      await DEBUG.group('incoming message:', messageEvent?.data?.cmd, messageEvent, async () => {
        try {
          const { data: msgData, data: { cmd } } = messageEvent
          const knownHandlers = Object.keys(msgHandlers)

          if ((isKnownMsgType = knownHandlers.includes(cmd))) {
            DEBUG('knownHandlers includes', cmd, isKnownMsgType)
          }
          if (cmd === 'setUsername') {
            userName = msgData?.options.userAddress
          }
          if (cmd === 'webnativePermissions') {
            if (wnfs) {
              WARN('received another webnativePermissions msg')
            } else {
              wnfs = await handleWebnativePermissionsFromUI(msgData)
              DEBUG('wnfs is intact', { wnfs })
            }
          }

          if (cmd === 'init') {
            LOG('init', msgData)
            // await setAuthor(data?.options.author)
            userName = msgData?.options.userAddress
            const { dbName, stores, options } = msgData
            if (!targetDB) {
              if (!(await DexiePlus.exists(dbName))) {
                WARN('RED FLAG - DB does not exist')
              }
              const nonBygonzDB = new DexiePlus(dbName, stores, {}, options)

              targetDB = nonBygonzDB
              DEBUG('just opened ->', { targetDB })
            } else {
              DEBUG('already opened ->', { targetDB })
            }

            if (!appLogDB) {
              appLogDB = await getAppLogDB(targetDB)
              // for (const eachTable of Object.keys(stores)) {
              //   DEBUG(p(), ...checkWorker('trying to get table', eachTable))
              //   appLogTables[eachTable] = await appLogDB.getTable(`${dbName}_${eachTable}`)
              // }
              // for (const eachTableName in appLogDB._allTables) {
              //   appLogDB[eachTableName] = appLogDB._allTables[eachTableName]
              // }
              DEBUG('just opened appLogDB with tables ->', { appLogDB, appLogTables })
            } else {
              DEBUG('already opened appLogDB ->', { appLogDB })
            }

            // const ignoredAttributes = ['ID', 'creator', 'created', 'modifier', 'modified']
            // const commitOp = async (modLogEntry) => {
            //   const opsToPut: AppLogObj[] = []
            //   if (!modLogEntry.en) { ERROR('Malformed OP', { modLogEntry }); throw new Error('Missing entity') }
            //   if (!modLogEntry.ag) { ERROR('Malformed OP', { modLogEntry }); throw new Error('Missing ag') }
            //   const { obj, tableName, en, ag = userName ?? '0x123', ts = obj.modified ?? utcMsTs() } = modLogEntry
            //   // if (tableName) obj.tableName = tableName // this is added only for add events to the object in the middleware
            //   const individualVerboseTraces = []
            //   for (const at of Object.keys(obj)) {
            //     const vl = obj[at]
            //     if (vl === undefined || ignoredAttributes.includes(at)) continue
            //     const newOpObj = new AppLogObj({
            //       at,
            //       vl,
            //       ag,
            //       en,
            //       ts,
            //     })
            //     opsToPut.push(newOpObj)
            //     // if (writeFile) writeFile(JSON.stringify(newOpObj), newOpObj.tx, tableName)
            //     individualVerboseTraces.push([{ tableName, obj, newOpObj }])
            //   }

            //   const opTable = appLogDB.AppLogs // appLogTables[tableName]
            //   DEBUG('bulkPut new AppLogs', { opsToPut })
            //   await opTable.bulkPut(opsToPut)
            //   if (!DEBUG.isDisabled && opsToPut.length) {
            //     if (!VERBOSE.isDisabled) {
            //       console.groupCollapsed(...checkWorker(opsToPut?.length, 'detailed appLogs via bc ->'))
            //       for (const eachTrace of individualVerboseTraces) {
            //         VERBOSE(...eachTrace)
            //       }
            //       console.groupEnd()
            //     } else {
            //       DEBUG('new appLogs via bc', opsToPut)
            //     }
            //   }

            //   // await dgraphMod(modLogEntry)
            //   // await fetchAndApplyMods(modDB, targetDB)
            // }

            // void fetchAndApplyMods(modDB, targetDB, true)

            // https://github.com/dexie/Dexie.js/blob/master/src/globals/global-events.ts
            // const DEXIE_STORAGE_MUTATED_EVENT_NAME = 'storagemutated' as 'storagemutated'
            // const STORAGE_MUTATED_DOM_EVENT_NAME = 'x-storagemutated-1'

            // https://github.com/dexie/Dexie.js/blob/master/src/live-query/enable-broadcast.ts
            const bc = new BroadcastChannel(BYGONZ_MUTATION_EVENT_NAME)

            bc.onmessage = async (ev) => {
              try {
              // const arrivalTs = utcMsTs()

                const { data: bcMsgData } = ev
                const { tx } = bcMsgData // each worker should only
                DEBUG('new tx', tx)
                const logs = await appLogDB.AppLogs.where({ tx }).toArray()
                // void debouncedPersist(appLogDB, logs)
                const publications = targetDB.options.publications
                if (!publications?.length) return DEBUG("can't persist - no publications", { targetDB })
                for (const pub of publications) {
                  try {
                    const handler = pubHandlers.get(pub.ID)
                    DEBUG('Persisting', pub, 'using', handler, { logs })
                    await handler.persist(appLogDB, logs)
                  } catch (err) {
                    ERROR('Failed to persist', { pub, err })
                  }
                }
              } catch (err) {
                ERROR('onmessage error:', err, { ev })
              }
            // LOG(...checkWorker('bygonz afterEffect', dbName, `${(dbName === dbNameFromMsg) ? '==' : '!='}`, dbNameFromMsg, { ev, bcTook: arrivalTs - ev?.data?.ts ?? arrivalTs + 1 }))
            // dbNameFromMsg === dbName && void commitOp(bcMsgData)
            }

            // https://isomorphic-git.org/docs/en/webworker
            // const MagicPortal = await im port('https://unpkg.com/magic-portal')
            // const gitWorker = new Worker('./WebWorkerI mports/git-worker.js')
            // const portal = new MagicPortal(gitWorker)
            // gitWorker.addEventListener('message', ({ data }) => console.log(data))
            // gitWorker.postMessage('init')

            // Initialize subscriptions
            // await ensureWasmInit(dbName, targetDB.options) // can be lazily initialized only if there is a sub/pub using it
            await initSubs(dbName, targetDB.options, await appLogDB.Subscriptions.toArray())
            await initPubs(dbName, targetDB.options, await appLogDB.Publications.toArray())
          } // </init

          if (cmd === 'addSubscription') {
            const { subscription } = msgData
            LOG('Adding subscription', subscription)
            if (!targetDB) throw new Error('targetDB is not initialized :(')
            await initSubscription(subscription, targetDB.name, targetDB.options)
          }
        } catch (err) {
          ERROR(`INSDIE Error handling message '${messageEvent?.data?.cmd}':`, err, { messageEvent, targetDB, subHandlers, pubHandlers })
          const msg: ErrorBroadcastMessage = { event: 'error', data: { cmd: messageEvent?.data?.cmd, error: err } }
          self.postMessage(msg)
        }
      })
    } catch (err) {
      ERROR(`Error handling message '${messageEvent?.data?.cmd}':`, err, { messageEvent, targetDB, subHandlers, pubHandlers })
      const msg: ErrorBroadcastMessage = { event: 'error', data: { cmd: messageEvent?.data?.cmd, error: err } }
      self.postMessage(msg)
    }
  }
} catch (err) {
  ERROR('[WW] OOPS something went wrong in setup', err)
}

export const initSubs = async (dbName: string, options: any, subscriptions: Subscription[]) => {
  if (subscriptions.length === 0) return DEBUG('no need to init - no subscriptions', { targetDB })
  for (const sub of subscriptions) {
    await initSubscription(sub, dbName, options)
  }
}

export const initPubs = async (dbName: string, options: any, publications: Publication[]) => {
  if (publications.length === 0) return DEBUG('no need to init - no publications', { targetDB })
  for (const publication of publications) {
    await initPublication(publication, dbName, options)
  }
}

export async function initPublication (publication: Publication, dbName: string, options: any) {
  try {
    let newHandler
    /* if (sub.type === 'wnfs') {
        newHandler = await createWnfsSubHandler(sub)
        } else */ if (publication.type === 'ipfs') {
      newHandler = await createIpfsPubHandler(dbName, options, publication)
    } else { throw new Error(`Unknown publication type: ${publication.type}`) }
    pubHandlers.set(publication.ID, newHandler)
    DEBUG('Initialized publication handler', { newHandler, publication })
    // if (newHandler?.identity) {
    //   DEBUG('set identity on the bygonz dexie', newHandler?.identity.publicKey)
    //   targetDB.setActiveAgent(newHandler?.identity.publicKey)
    // }
  } catch (err) {
    ERROR('Failed to init publication', err, { publication })
  }
}

export async function initSubscription (sub: Subscription, dbName: string, options: any) {
  let newHandler
  try {
    if (sub.type === 'ipfs') {
      newHandler = await createIpfsSubHandler(dbName, options, sub, async (/* newLogs */) => {
        try {
          // DEBUG('IPFS update -> loading into DB', { newHandler, newLogs })
          // await appLogDB.AppLogs.bulkPut(newLogs) // the put is handled in WASM
          DEBUG('sending update event after Wasm wrote appLogs' /* , {newLogs} */) // we just send an event
          const msg: AppLogUpdateBroadcastMessage = {
            event: 'updated',
            data: { /* added: newLogs */ },
          }
          self.postMessage(msg)
          // Dexie liveQuery magic won't work as we are 1. in WW and 2. not writing via dexie
        } catch (err) {
          ERROR('[saturate on update]', err)
        }
      })
    } else { throw new Error(`Unknown subscription type: ${sub.type}`) }
    subHandlers.set(sub.ID, newHandler)
    DEBUG('Initialized subscription handler', { newHandler, sub })
    if (newHandler?.identity) {
      DEBUG('set identity on the bygonz dexie', newHandler?.identity.publicKey)
      targetDB.setActiveAgent(newHandler?.identity.publicKey)
    }
  } catch (err) {
    ERROR('Failed to init subscription', err, { sub })
  }
}

export const handleBygonzBroadcastMsg = async (ev) => {
  try {
    // const arrivalTs = utcMsTs()

    const { data: bcMsgData } = ev
    const { tx } = bcMsgData // each worker should only
    DEBUG('new tx', tx)
    const logs = await appLogDB.AppLogs.where({ tx }).toArray()
    // void debouncedPersist(appLogDB, logs)
    const publications = targetDB.options.publications
    if (!publications?.length) return DEBUG("can't persist - no publications", { targetDB })
    for (const pub of publications) {
      try {
        const handler = pubHandlers.get(pub.ID)
        DEBUG('Persisting', pub, 'using', handler, { logs })
        await handler.persist(appLogDB, logs)
      } catch (err) {
        ERROR('Failed to persist', { pub, err })
      }
    }
  } catch (err) {
    ERROR('onmessage error:', err, { ev })
  }
}
