import type { KeyStore } from 'keystore-idb/lib/types'

import { Dexie } from 'dexie'

import { attributeOverwriteSituation, attributeTwiceInSameTxSituation } from './WebWorkerImports/Situations'

import { Logger } from '@ztax/logger'
const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export const defaultOptions = {
  situationFinders: [
    attributeOverwriteSituation('FiveSeconds', 5000),
    attributeTwiceInSameTxSituation(),
  ],
  // author: { // TODO find dependencies on this and purge them
  //   name: 'userAddress',
  //   email: 'userAddress@publickey.eth',
  // },
  activeAgent: 'defaultActiveAgent',
  ipfs: {
    rpcUrl: 'http://127.0.0.1:5001/',
  },
}

export type DexiePlusParams = [name: string, stores: Record<string, string>, mappings?: Record<string, any>, options?: typeof defaultOptions, version?: number]

class DexiePlus extends Dexie {
  public workerApi
  public stores
  public mappings
  public options
  public activeAgent: string
  public activeAgentKeystore: KeyStore

  public doMappings () {
    VERBOSE('befor tables?', this)
    for (const eachTable of this.tables) {
      VERBOSE('before constructor table assignment', this[eachTable.name])
      this[eachTable.name] = eachTable // ensure there is a reference to each table "on" the db instance
      VERBOSE('after constructor table assignment', this[eachTable.name], this)

      const mappingsClass = this.mappings[eachTable.name]
      if (mappingsClass) {
        VERBOSE(this[eachTable.name], 'mapping', eachTable, 'to', mappingsClass)
        eachTable.mapToClass(mappingsClass)
      }
    }
    DEBUG('After doMappings', this)
  }

  /**
   * setActiveAgent
   */
  public setActiveAgent (agentPublicKey) {
    this.activeAgent = agentPublicKey
  }

  /**
   * setActiveAgent
   */
  public setActiveAgentKeystore (keystore: KeyStore) {
    DEBUG('setting', { keystore })
    this.activeAgentKeystore = keystore
  }

  constructor (name: string, stores: Record<string, string>, mappings: Record<string, any> = {}, options = defaultOptions, version = 1) {
    if (!name || !stores) {
      ERROR('DexiePlus requires params (name, {stores})')
      throw new Error('DexiePlus requires params (name, {stores})')
    }
    super(name)

    VERBOSE('removing HooksMiddleware in DexiePlus')
    this.unuse({ stack: 'dbcore', name: 'HooksMiddleware' }) // must happen before calling stores otherwise the tables will still have the Middleware
    // this._middlewares.dbcore = this._middlewares.dbcore.filter(eachMW => eachMW.name !== 'HooksMiddleware')

    this.version(version).stores(stores)
    this.options = options
    this.setActiveAgent(this.options.activeAgent ?? 'noActiveAgentSet')
    this.stores = stores
    this.mappings = mappings
    // this.doMappings() // needs to be called in the final child class that extends BygonzDexie
  }
}

export type { Table } from 'dexie'
const { liveQuery } = Dexie
export { liveQuery, Dexie, DexiePlus }
