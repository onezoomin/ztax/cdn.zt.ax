import { Subscription } from '../Subscriptions'
import { Publication } from '../Publications'
import type { BygonzSub, BygonzPub } from '../SubPub/BygonzSubPub'
import { createIpfsPubHandler, createIpfsSubHandler } from '../SubPub/ipfs-wasm'

import { DexiePlus } from '../../DexiePlus'
import { AppLogDB, getAppLogDB } from '../AppLog'
import { BYGONZ_MUTATION_EVENT_NAME } from '../Utils'

import { Logger } from '../Common'
import { AppLogUpdateBroadcastMessage } from '../../BygonzWebWorker';
const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

// export let targetDB: DexiePlus | undefined
// export let appLogDB: AppLogDB | undefined
// export const subHandlers = new Map<String, BygonzSub>() // TODO: use name as key?
// export const pubHandlers = new Map<String, BygonzPub>()
