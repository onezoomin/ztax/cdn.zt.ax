import { Logger } from '@ztax/logger'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: false }) // eslint-disable-line @typescript-eslint/no-unused-vars
const p = Logger.perf.bind(Logger, 1)

export const msgHandlers = {
  setFileSystemHandle: function msgHandlersSetFileSystemHandle (msgData) {
    DEBUG(p(), msgData)
    const fsHandle = msgData?.dirHandle
    const writeFileFx = async (contents, fileName, _tableName = 'Colors') => {
      DEBUG({ fileName, contents })
      // In an existing directory, create a new directory named "My Documents".
      const tableDirectoryHandle = fsHandle // await fsHandle.getDirectoryHandle(_tableName, { create: true })
      // In this new directory, create a file named "My Notes.txt".
      const newFileHandle = await tableDirectoryHandle.getFileHandle(fileName, { create: true })
      const writable = await newFileHandle.createWritable()
      // Write the contents of the file to the stream.
      await writable.write(contents)
      // Close the file and write the contents to disk.
      await writable.close()
    }
    DEBUG(p(), { fsHandle, writeFileFx })
    return { fsHandle, writeFileFx }
  },
}
