import type { BygonzDexie } from '../Bygonz'
import type { Table } from 'dexie'
import { WithAppLogDB } from './AppLog'

import { WithHashID } from './Common'

import { Logger } from '@ztax/logger'
import { Mixin } from 'ts-mixer'

const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const _subscriptionConfigExample = {
  type: 'wnfs',
  info: {
    accountName: 'bygonztest',
    logId: 'testlog-one',
    query: '*',
  },
}

export class Publication extends Mixin(WithHashID, WithAppLogDB) {
  type: 'wnfs' | 'ipfs' | 'other'
  info: Record<string, string>

  tablePath?: string
  logCount?: number

  ipns?: string
  pos?: string

  async setLogCount (newCount: number) {
    !this.db && await this.fetchAppLogDB()
    VERBOSE('Setting LogCount', { logDB: this.db, newCount, prevCount: !VERBOSE.isDisabled && (await this.db.Publications.get(this.ID)).logCount })
    // @ts-  expect-error
    await this.db.Publications?.update(this.ID, { logCount: newCount })
  }

  constructor (obj: any) {
    super(obj)
    Object.assign(this, obj)
  }
}

export type PublicationTable = Table<Publication, 'ID'>

export const saturateAllPublicationsFromAppLogs = async (targetDB: BygonzDexie) => {
  const totalAdded = 0
  if (!targetDB) throw new Error('requires targetDB')
  const allSubs = await targetDB.getPublicationsArray()

  for (const eachSub of allSubs) {
    // totalAdded += (await (eachSub as BygonzSubPub).publish(eachSub, targetDB) as number) ?? 0 // TODO fix functionality and typings
  }
  return totalAdded
}
