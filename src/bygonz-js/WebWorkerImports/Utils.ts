export type GenericObject = Record<string, any>

/* eslint-disable no-fallthrough */
export const overlayHack = () => {
  // hack to avoid overlay.ts's dom assumptions
  // @ts-expect-error
  self.HTMLElement = function () {
    return {}
  }

  self.customElements = {
    // @ts-expect-error
    get () {
      return []
    },
  }
}
export const checkWorker = (...msg) => {
  if (self.document === undefined) {
    return ['WW:', ...msg]
  } else {
    return [...msg] // ' sad trombone. not worker ',
  }
}
export const frozen = async (obj: any) => JSON.parse(JSON.stringify(obj))
const appStartTimeStamp = Date.now()
const p = performance.now()
let precision = 0
// milliseconds since epoch (100nanosecond "precision")
export function utcMsTs (): number {
  const now = new Date()
  const newPrecision = Math.round(performance.now() - p)
  precision = newPrecision === precision ? newPrecision + 1 : newPrecision // ensure 1ms difference - basically
  return appStartTimeStamp + precision + (now.getTimezoneOffset() * 60 * 1000)
}

export const BYGONZ_MUTATION_EVENT_NAME = 'bygonzmutation'

export const allPromises = Promise.all.bind(Promise) // https://stackoverflow.com/a/48399813/2919380

export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms)) // https://stackoverflow.com/a/39914235/2919380
/**
 * JS Implementation of MurmurHash3 (r136) (as of May 20, 2011)
 *
 * https://github.com/garycourt/murmurhash-js/blob/master/murmurhash3_gc.js
 *
 * @author <a href="mailto:gary.court@gmail.com">Gary Court</a>
 * @see http://github.com/garycourt/murmurhash-js
 * @author <a href="mailto:aappleby@gmail.com">Austin Appleby</a>
 * @see http://sites.google.com/site/murmurhash/
 *
 * @param {string} key ASCII only
 * @param {number} seed Positive integer only
 * @return {number} 32-bit positive integer hash
 */
export function murmurHash (key: string, seed: number, _rippedFrom = 'https://github.com/garycourt/murmurhash-js/blob/master/murmurhash3_gc.js') {
  let remainder, bytes, h1, h1b, c1, c2, k1, i

  remainder = key.length & 3 // key.length % 4
  bytes = key.length - remainder
  h1 = seed
  c1 = 0xcc9e2d51
  c2 = 0x1b873593
  i = 0

  while (i < bytes) {
    k1 = ((key.charCodeAt(i) & 0xff))
      | ((key.charCodeAt(++i) & 0xff) << 8)
      | ((key.charCodeAt(++i) & 0xff) << 16)
      | ((key.charCodeAt(++i) & 0xff) << 24)

    ++i

    k1 = ((((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16))) & 0xffffffff
    k1 = (k1 << 15) | (k1 >>> 17)
    k1 = ((((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16))) & 0xffffffff

    h1 ^= k1
    h1 = (h1 << 13) | (h1 >>> 19)
    h1b = ((((h1 & 0xffff) * 5) + ((((h1 >>> 16) * 5) & 0xffff) << 16))) & 0xffffffff
    h1 = (((h1b & 0xffff) + 0x6b64) + ((((h1b >>> 16) + 0xe654) & 0xffff) << 16))
  }

  k1 = 0

  switch (remainder) {
    case 3:
      k1 ^= (key.charCodeAt(i + 2) & 0xff) << 16
    case 2:
      k1 ^= (key.charCodeAt(i + 1) & 0xff) << 8
    case 1:
      k1 ^= (key.charCodeAt(i) & 0xff)
      k1 = (((k1 & 0xffff) * c1) + ((((k1 >>> 16) * c1) & 0xffff) << 16)) & 0xffffffff
      k1 = (k1 << 15) | (k1 >>> 17)
      k1 = (((k1 & 0xffff) * c2) + ((((k1 >>> 16) * c2) & 0xffff) << 16)) & 0xffffffff
      h1 ^= k1
  }

  h1 ^= key.length

  h1 ^= h1 >>> 16
  h1 = (((h1 & 0xffff) * 0x85ebca6b) + ((((h1 >>> 16) * 0x85ebca6b) & 0xffff) << 16)) & 0xffffffff
  h1 ^= h1 >>> 13
  h1 = ((((h1 & 0xffff) * 0xc2b2ae35) + ((((h1 >>> 16) * 0xc2b2ae35) & 0xffff) << 16))) & 0xffffffff
  h1 ^= h1 >>> 16

  return h1 >>> 0
}

/*
    cyrb53 (c) 2018 bryc (github.com/bryc)
    A fast and simple hash function with decent collision resistance.
    Largely inspired by MurmurHash2/3, but with a focus on speed/simplicity.
    Public domain. Attribution appreciated.
*/
export const cyrb53hash = function (str: string, seed = 13, strLength = 0, _rippedFrom = 'https://github.com/bryc/code/blob/master/jshash/experimental/cyrb53.js') {
  if (!str?.length) return console.warn('bailing out, irrelevent string:', str)

  let h1 = 0xdeadbeef ^ seed; let h2 = 0x41c6ce57 ^ seed
  for (let i = 0, ch; i < str.length; i++) {
    ch = str.charCodeAt(i)
    h1 = Math.imul(h1 ^ ch, 2654435761)
    h2 = Math.imul(h2 ^ ch, 1597334677)
  }
  h1 = Math.imul(h1 ^ (h1 >>> 16), 2246822507) ^ Math.imul(h2 ^ (h2 >>> 13), 3266489909)
  h2 = Math.imul(h2 ^ (h2 >>> 16), 2246822507) ^ Math.imul(h1 ^ (h1 >>> 13), 3266489909)
  if (strLength) {
    const asHex = (4294967296 * (2097151 & h2) + (h1 >>> 0)).toString(16)
    return asHex.slice(-strLength).padStart(strLength, '0')
  }
  // if not specified return as 16 digit integer
  return 4294967296 * (2097151 & h2) + (h1 >>> 0)
}
