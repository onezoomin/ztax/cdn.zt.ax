import type { BygonzDexie } from '../Bygonz'
import type { Table } from 'dexie'
import { WithAppLogDB } from './AppLog'

import { WithHashID } from './Common'

import { Logger } from '@ztax/logger'
import { Mixin } from 'ts-mixer'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

const _subscriptionConfigExample = {
  type: 'wnfs',
  info: {
    appCreator: '',
    appName: '',
    accountName: '',
  },
}

export class Subscription extends Mixin(WithHashID, WithAppLogDB) {
  type: 'wnfs' | 'ipfs' | 'other'
  info: Record<string, string>

  tablePath?: string
  logCount?: number

  async setLogCount (newCount: number) {
    !this.db && await this.fetchAppLogDB()
    VERBOSE('Setting LogCount', { logDB: this.db, newCount, prevCount: !VERBOSE.isDisabled && (await this.db.Subscriptions.get(this.ID)).logCount })
    // @ts- expect-error
    await this.db.Subscriptions?.update(this.ID, { logCount: newCount })
  }

  constructor (obj: any) {
    super(obj)
    Object.assign(this, obj)
  }
}

export type SubscriptionTable = Table<Subscription, 'ID'>

export const saturateAppLogsFromAllSubscriptions = async (targetDB: BygonzDexie) => {
  const totalAdded = 0
  if (!targetDB) throw new Error('requires targetDB')
  const allSubs = await targetDB.getSubscriptionArray()

  for (const eachSub of allSubs) {
    // totalAdded += (await (eachSub as BygonzSubPub).saturate(eachSub, targetDB) as number) ?? 0 // TODO fix functionality and typings
  }
  return totalAdded
}
