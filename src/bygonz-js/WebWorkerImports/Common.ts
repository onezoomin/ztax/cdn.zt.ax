import stringify from 'json-stringify-deterministic'
import { cyrb53hash, utcMsTs } from './Utils'

import { Logger } from '@ztax/logger'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.INFO, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export class TimeStampedBase {
  created: number
  modified: number

  constructor (obj: any) {
    VERBOSE('ts cons', obj)
    obj.created = utcMsTs()
    obj.modified = obj.created

    Object.assign(this, obj)
  }
}

export class ModWho {
  creator: string
  modifier: string

  constructor (obj: any) {
    VERBOSE('modwho cons', obj)
    obj.creator = obj.creator ?? '0xdefModWho'
    obj.modifier = obj.modifier ?? obj.creator
    Object.assign(this, obj)
  }
}

// export interface WithStringID{
//   ID?: string
// }
export class WithHashID /* implements WithStringID */ {
  ID?: string
  constructor (obj: any) {
    // console.debug('WithHashID cons', obj)
    const IDobj = { ID: obj.ID ?? cyrb53hash(stringify(obj), 31, 11) }
    Object.assign(this, obj, IDobj)
  }
}

export { Logger }
