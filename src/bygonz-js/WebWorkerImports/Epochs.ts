import { Dexie } from 'dexie'

import { getAppLogDB, AppLogVM, AppLogObj } from './AppLog'
import { ripemd160 } from '@ethersproject/sha2'
import formatfp from 'date-fns/fp/format'
import format from 'date-fns/format'
import fromUnixTime from 'date-fns/fromUnixTime'
import getMinutes from 'date-fns/getMinutes'
import set from 'date-fns/set'
import sub from 'date-fns/sub'
import add from 'date-fns/add'

import { dump, load } from 'js-yaml'
import { utcMsTs } from './Utils'
import { isEqual, isEmpty, groupBy, sortBy, last, first } from 'lodash-es'

// TODO optimize date-fns https://cdn.skypack.dev/pin/date-fns@v2.28.0-zvghaYXqcEkTnsd3AFYD/mode=imports,min/optimized/date-fns.js
export class EpochObj {
  data: string
  hash?: string
  date?: string
  ts?: number

  constructor (params: EpochObj) {
    params.ts = params.ts ?? utcMsTs()
    params.date = format(params.ts, 'yyyy.MM.dd_H:mm')
    Object.assign(this, params)
  }
}
export class EpochClass extends EpochObj {
  declare ts: number
  public get hashCalc (): string {
    return ripemd160((new TextEncoder()).encode(this.data))
  }
}

let epochDB
export const getEpochDB = async () => {
  if (epochDB) return epochDB
  class EpochDB extends Dexie {
    // [x: string]: any
    // Declare implicit table properties. (just to inform Typescript. Instanciated by Dexie in stores() method)
    Epochs: Dexie.Table<EpochClass | EpochObj, number> // number = type of the priKey
    // spanMs: number
    // ...other tables go here...

    async init () {
      // console.log('init')
    }

    constructor () {
      super('Bygonz_EpochDB')

      this.version(1).stores({
        Epochs: 'ts, date',
      })

      // addTableRefs(this)
      this.Epochs.mapToClass(EpochClass)
    }
  }
  epochDB = new EpochDB()
  return epochDB as EpochDB
}
// void appLogRollup(true, true)
// const rollupInterval = setInterval(() => {
//   void appLogRollup()
// }, 10000)
// const minuteEpochDB = new EpochDB(6000, 'Minute')
// const minutesTable = minuteEpochDB.Epochs
// const modTable = modDB.Mods

const yamlOptions = {
  noArrayIndent: true,
}

export const parseYamlLog = (yamlString: string) => {
  const appLogFromYaml = load(yamlString, yamlOptions)
  // console.log(appLogFromYaml)
  return appLogFromYaml
}

export const yamlToAppLogArray = async (yamlString: string): Promise<AppLogVM[]> => {
  const appLogTable = (await getAppLogDB()).AppLogs
  const appLogForUsers = parseYamlLog(yamlString)
  const usernames = Object.keys(appLogForUsers)
  const yamlAsArray = []
  const matchingAppLogArray = []
  const missingAppLogArray = []
  const oddballAppLogArray = []
  for (const eachUser of usernames) {
    const eachUserLog = appLogForUsers[eachUser]

    const allLogIDs = Object.keys(eachUserLog)
    for (const eachTXID of allLogIDs) {
      const { tx: _tx, ...existingLogEntryWithoutID } = (await appLogTable.get(eachTXID)) ?? {}
      const shouldBeMatchingAppLogEntry = eachUserLog[eachTXID]
      const incomingAppLogVMish = { tx: eachTXID, ...eachUserLog[eachTXID] }
      yamlAsArray.push(incomingAppLogVMish)
      if (isEqual(shouldBeMatchingAppLogEntry, existingLogEntryWithoutID)) {
        matchingAppLogArray.push(incomingAppLogVMish)
      } else {
        if (isEmpty(existingLogEntryWithoutID)) {
          missingAppLogArray.push(incomingAppLogVMish)
          void appLogTable.add(incomingAppLogVMish)
        } else {
          oddballAppLogArray.push(incomingAppLogVMish)
          console.warn('existing but non-matching ', { eachUser, shouldBeMatchingAppLogEntry, existingLogEntryWithoutID })
        }
        // console.log('found non matching entry', { eachUser, shouldBeMatchingAppLogEntry, existingLogEntryWithoutID })
      }
    }
  }
  // const allAppLogsFromDB = await appLogTable.orderBy('ts').toArray()
  // console.log({ yamlAsArray, allAppLogsFromDB, matchingAppLogArray, missingAppLogArray })
  return yamlAsArray
}
export const mapAppLogArrayToEntities = async (appLogVmArray: AppLogVM[]) => {
  const entityArray = []
  const appLogsByEntityID = groupBy(appLogVmArray, 'en')
  const allEntityIDs = Object.keys(appLogsByEntityID)
  for (const eachID of allEntityIDs) {
    const orderedByTs = sortBy(appLogsByEntityID[eachID], 'ts')
    const { ts: created, ag: creator } = first(orderedByTs) // TODO - not true if the array does not include the original
    const { ts: modified, ag: modifier } = last(orderedByTs) // last modifier stays "on" the obj // TODO consider removing from entities and only use getters via applog
    const entityObject = { ID: eachID, created, creator, modified, modifier }
    for (const { at, vl } of orderedByTs) {
      entityObject[at] = vl
    }
    entityArray.push(entityObject)
  }
  // console.log(entityArray)
  return entityArray
}

export const getOrderedAppLogSince = async (since = 0, tableName = 'AppLogs') => {
  const appLogTable = (await getAppLogDB())[tableName]
  return await appLogTable.where('ts').above(since).toArray()
}

export const tsHumanTimeOnly = formatfp('HH:mm:ss.SSS')
export const tsHumanDayOnly = formatfp('yyyyMMdd')
export const tsHuman = formatfp('yyyyMMdd_HH:mm:ss.SSS')
export const tsHumanNice = formatfp('yyyy MMM dd HH:mm')

type yamlRecord = Record<string, {'@': string, at: string, ag: string, vl: string}>

export const logToYamlHumanTsKeys = ({ ts, en: _en, tx, ...logMin }: AppLogObj): string => dump({ [tx]: { '@': tsHumanTimeOnly(ts), ...logMin } }) // as dumped yamlRecord

export const yamlHumanTsKeysToAppLogObj = (yamlToParse: string): AppLogObj[] => {
  const yamlAsObj: yamlRecord = load(yamlToParse)
  const entries = Object.entries(yamlAsObj)
    .map(([tx, { '@': _hts, ...logMin }]) => {
      const [ts, en, _hash] = tx.split('_')
      return { tx, ts: +ts, en, ...logMin }
    })
  return entries
}

export const mapAppLogForFiles = async (appLogVmArray?: AppLogVM[]) => {
  const mapped = {}

  const orderedAppLog = appLogVmArray ?? await getOrderedAppLogSince()
  orderedAppLog.forEach(({ tx, ...log }: AppLogVM) => {
    mapped[tx] = logToYamlHumanTsKeys(log)
  })

  return mapped
}
export const mapAppLogForYaml = async (appLogVmArray?: AppLogVM[]) => {
  const mapped = {}

  const appLogTable = (await getAppLogDB()).AppLogs
  const orderedAppLog = appLogVmArray ?? await appLogTable.orderBy('ts').toArray()
  orderedAppLog.forEach(({ tx, ...log }: AppLogVM) => {
    mapped[tx] = log
  })

  return mapped
}
export const createYamlLog = async (agentID) => {
  const mapped = await mapAppLogForYaml()

  const yaml = dump({ [agentID]: mapped }, yamlOptions)
  // console.log(yaml)
  return yaml
}

const EPOCHS_PER_DAY = 24 * 60 // (eg 24*60*3 = 20s epochs)
const _EPOCH_LENGTH_MS = (24 * 60 * 60 * 1000) / EPOCHS_PER_DAY
const _EPOCH_5M_EX = 57 - (57 % 5) // 55 "rounding down to the most recent 5"
const EPOCH_LENGTH_MIN = 10
export interface EpochInfo {
  ts: number
  hr: string
  dir?: {directory: string[]}
}
export const getHourlyEpochTsObj = (asOfMsTs = utcMsTs()): EpochInfo => {
  const nowDate = fromUnixTime(asOfMsTs / 1000)
  const dailyEpochStart = set(nowDate, { minutes: 0, seconds: 0, milliseconds: 0 }).getTime()
  return { ts: dailyEpochStart, hr: tsHuman(dailyEpochStart) }
}

export const getDailyEpochTsObj = (asOfMsTs = utcMsTs()): EpochInfo => {
  const nowDate = fromUnixTime(asOfMsTs / 1000)
  const dailyEpochStart = set(nowDate, { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 }).getTime()
  return { ts: dailyEpochStart, hr: tsHumanDayOnly(dailyEpochStart) }
}
export const getEpochTsObj = (asOfMsTs = utcMsTs()): Record<string, EpochInfo> => {
  const nowDate = fromUnixTime(asOfMsTs / 1000)
  const nowMinutes = getMinutes(nowDate)
  const thisEpochMinutes = nowMinutes - (nowMinutes % EPOCH_LENGTH_MIN)
  const thisEpochDate = set(nowDate, { minutes: thisEpochMinutes, seconds: 0, milliseconds: 0 })
  const prev3EpochStart = sub(thisEpochDate, { minutes: 3 * EPOCH_LENGTH_MIN }).getTime()
  const prev2EpochStart = sub(thisEpochDate, { minutes: 2 * EPOCH_LENGTH_MIN }).getTime()
  const prevEpochStart = sub(thisEpochDate, { minutes: EPOCH_LENGTH_MIN }).getTime()
  const nextEpochStart = add(thisEpochDate, { minutes: EPOCH_LENGTH_MIN }).getTime()
  const thisEpochStart = thisEpochDate.getTime()

  const dailyEpochStart = set(nowDate, { hours: 0, minutes: 0, seconds: 0, milliseconds: 0 }).getTime()

  return {
    '-3': { ts: prev3EpochStart, hr: tsHumanTimeOnly(prev3EpochStart) },
    '-2': { ts: prev2EpochStart, hr: tsHumanTimeOnly(prev2EpochStart) },
    '-1': { ts: prevEpochStart, hr: tsHumanTimeOnly(prevEpochStart) },
    0: { ts: thisEpochStart, hr: tsHumanTimeOnly(thisEpochStart) },
    '+1': { ts: nextEpochStart, hr: tsHumanTimeOnly(nextEpochStart) },
    daily: { ts: dailyEpochStart, hr: tsHumanDayOnly(dailyEpochStart) },
  }
}

const _createHashFromEpochEntry = (_entry: string) => {
  // console.log('cryptoHashOf', entry)
}

const EpochEntryExample = {
  '13:37': {
    ':10.905': {
      'ActiveTasks__U__1648557321841,0x49Dda9CD9A36B90B34412CbABf687d783DA0eaa4': {
        put:
        {
          task: '49dttt',
          modifier: '0xd51e897DC57A9318390584707E39970aDab1A70b',
          modified: 1648557430905,
        },
        obj:
        {
          created: 1648557321841,
          modified: 1648557430905,
          task: '49dttt',
          status: 'Active',
          creator: '0x49Dda9CD9A36B90B34412CbABf687d783DA0eaa4',
          modifier: '0xd51e897DC57A9318390584707E39970aDab1A70b',
        },
        revert:
        {
          task: '49d',
          modified: 1648557321841,
        },
      },
    },
  },

}
const _signedEpochExample = {
  [`${Object.keys(EpochEntryExample)[0]}`]: {
    hash: _createHashFromEpochEntry(dump(EpochEntryExample)), // should use yaml string (and ensure stable deterministic sorting)
    sigs: {
      '0c0x49d': 'sig0x0x0x',
    },
  },
}
