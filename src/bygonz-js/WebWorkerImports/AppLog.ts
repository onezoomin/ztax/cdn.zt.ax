import type { BygonzDexie } from '../Bygonz'
import type { DexieConstructor, Table } from 'dexie'
import type { SubscriptionTable, Subscription } from './Subscriptions'
import type { PublicationTable, Publication } from './Publications'
import { Mixin } from 'ts-mixer'
import { Dexie } from 'dexie'
import stringify from 'json-stringify-deterministic'
import { cyrb53hash, utcMsTs } from './Utils'
import * as IpldBlock from 'multiformats/block'
import { last, first, groupBy, sortBy, pick } from 'lodash-es'
import { sha256 } from 'multiformats/hashes/sha2'
import { WithHashID } from './Common'
import * as DagJson from '@ipld/dag-json'
import { CID } from 'multiformats'
import { Logger } from '@ztax/logger'
import { tsHuman } from './Epochs'
// import { DexiePlus } from '../DexiePlus'
import { atomToDagAtom, encodeDagJson } from '../ipld'
import { DexiePlus } from '../DexiePlus'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: false }) // eslint-disable-line @typescript-eslint/no-unused-vars
const { perf: p } = Logger

type attributeName = string
type previousCID = string
type atomCID = string
type value = string
type entityID = string
type agentID = string
type hashOfatomCIDarray = string
type utcTimeStampMS = number
type operation = boolean
export interface MutMsg {
  dbName: string
  tableName: string
  ag: string
  ts: number
  en: string
  obj: Record<string, any> & {isDeleted?: boolean}
  op: string
}
export abstract class Atom {
  ag: agentID
  ts: utcTimeStampMS
  en: entityID
  at: attributeName
  vl: value
  pv: previousCID | null
  op: operation
}

export async function createAtomsFromChangesets (mutMsgs: MutMsg[], agentID?: string) { // TODO typescript and dataflow - proper modLog entries (also in consumer apps)
  DEBUG('createAtomsFromChangesets', { mutMsgs })
  const ignoredAttributes = ['ID', 'creator', 'created', 'modifier', 'modified']
  const resultingAtoms: Atom[] = []
  for (const modLogEntry of mutMsgs) {
    if (!modLogEntry.en) { ERROR('Malformed OP', { modLogEntry }); throw new Error('Missing entity') }
    if (!modLogEntry.ag) { ERROR('Malformed OP', { modLogEntry }); throw new Error('Missing ag') }
    const { obj, tableName, en, ag = obj.modifier ?? agentID, ts = obj.modified ?? utcMsTs() } = modLogEntry
    // if (tableName) obj.tableName = tableName // this is added only for add events to the object in the middleware
    const individualVerboseTraces = []
    for (const at of Object.keys(obj)) {
      const vl = obj[at]
      if (vl === undefined || ignoredAttributes.includes(at)) continue
      const newOpObj = { // TODO decide if we want the tx hash to be part of the atom or not
        ag,
        ts,
        en,
        at,
        vl,
        op: true,
        pv: undefined,
      }
      VERBOSE('Getting previous atom for', newOpObj)

      const previousAtom = null // await getPreviousAppLogForAtom(newOpObj)
      newOpObj.pv = previousAtom?.ha ?? null

      VERBOSE('Created new atom', newOpObj)
      resultingAtoms.push(newOpObj)
      // if (writeFile) writeFile(JSON.stringify(newOpObj), newOpObj.tx, tableName)
      individualVerboseTraces.push([{ tableName, obj, newOpObj }])
    }
  }
  DEBUG('createAtomsFromChangesets result', { resultingAtoms })
  return resultingAtoms
}
export class WithHistory extends WithHashID {
  get tablePath () { return 'AppLogs' } // meant to be overridden

  async getAppLogTable (_tablePath = this.tablePath) {
    return (await getAppLogDB()).AppLogs // getTable(tablePath)
  }

  async getEntityHistory (tablePath = this.tablePath): Promise<AppLogVM[]> {
    return Dexie.Promise.newPSD(async () => {
      const appLogTableSpecific = await this.getAppLogTable(tablePath)
      return (await (appLogTableSpecific.where('en').equals(this.ID).sortBy('ts')))
        .map(obj => new AppLogVM(obj))
    })
  }

  async getEntityHistoryAsof (until = undefined, tablePath = this.tablePath): Promise<AppLogVM[]> {
    return Dexie.Promise.newPSD(async () => {
      const appLogTableSpecific = await this.getAppLogTable(tablePath)
      const sortedHistory = await (appLogTableSpecific.where('en').equals(this.ID).sortBy('ts'))
      return (until ? sortedHistory.filter(eachLog => eachLog.ts <= until) : sortedHistory)
        .map(obj => new AppLogVM(obj))
    })
  }

  async getEntityAsof<EntityType> (asof, tablePath = this.tablePath): Promise<EntityType | WithHashID> {
    return Dexie.Promise.newPSD(async () => {
      const entityHistory = await this.getEntityHistoryAsof(asof, tablePath)
      const entityObject = { ID: this.ID }
      for (const { at, vl } of entityHistory) {
        entityObject[at] = vl
      }
      return entityObject
    })
  }

  public async getCreator (): Promise<agentID> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      return first(fullHistory)?.ag
    })
  }

  public async getCreated (): Promise<utcTimeStampMS> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      return first(fullHistory)?.ts
    })
  }

  public async getModifierHistoryArray (): Promise<agentID[]> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      return fullHistory.map(eachAppLog => eachAppLog.ag)
    })
  }

  public async getModificationHistoryArray (): Promise<Array<{[x: utcTimeStampMS]: agentID}>> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      return fullHistory.map(eachAppLog => ({ [eachAppLog.ts]: eachAppLog.ag }))
    })
  }

  public async getModifier (): Promise<agentID> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      return last(fullHistory)?.ag
    })
  }

  public async getModified (): Promise<utcTimeStampMS> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      return last(fullHistory)?.ts
    })
  }

  async getAttributeHistoryArray<T> (attrPath: keyof T, _otherAttrs = 'ignored - use getAttributeHistory<T>'): Promise<string[]> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      const fullAttrHistory = fullHistory.filter(eachLog => eachLog.at === attrPath)
      return fullAttrHistory.map(eachLog => eachLog.vl) // as string[]
    })
  }

  async getAttributeHistory<T> (attrPath: string, otherAttrs = ['ts']): Promise<Array<historyResponseEntry<T>>> {
    return Dexie.Promise.newPSD(async () => {
      const fullHistory = await this.getEntityHistory()
      const fullAttrHistory = fullHistory.filter(eachLog => eachLog.at === attrPath)
      if (otherAttrs.length === 1 && otherAttrs[0] === '*') { // wildcard to get array of full entries plus requested [attr]:val
        return fullAttrHistory.map(eachLog => ({ [attrPath]: eachLog.vl, ...eachLog })) as Array<historyResponseEntry<T>>
      }
      const retArrayWithCustomAttr = fullAttrHistory.map(eachLog => {
        const entry = { [attrPath]: eachLog.vl }
        for (const eachOtherAttr of otherAttrs) entry[eachOtherAttr] = eachLog[eachOtherAttr]
        return entry
      }) as Array<historyResponseEntry<T>>
      return retArrayWithCustomAttr
    })
  }
}
export type historyResponseEntry<T> = {[x in keyof T]: string} & Partial<AppLogObj>

export class AppLogObj extends Atom {
  ha: atomCID
  tx?: hashOfatomCIDarray

  static async create (atom: Atom, tx: string = undefined): Promise<AppLogObj> {
    if ((atom as any).tx) throw new Error('atoms should not contain txHash - it must be added afterwards')
    const dagAtom = atomToDagAtom(atom)
    const { cid } = await encodeDagJson(dagAtom)
    VERBOSE('constructing AppLogObj', { atom, dagAtom, cid })
    return {
      ...atom,
      tx,
      ha: cid.toString(),
    }
  }
}

export class WithAppLogDB {
  db?: AppLogDB

  async fetchAppLogDB () {
    this.db = this.db ?? await getAppLogDB()
    return this.db
  }

  constructor (obj: any) {
    Object.assign(this, obj)
  }
}

export class AppLogVM extends Mixin(AppLogObj, WithHistory) {
  get humanTs (): string {
    return tsHuman(this.ts)
  }

  // async getPreviousAtom () {
  //   return await getPreviousAppLogForAtom({ en: this.en, at: this.at })
  // }

  async getEntityHistory (tablePath = this.tablePath, entityID = this.en): Promise<AppLogVM[]> {
    const appLogTableSpecific = await this.getAppLogTable(tablePath)
    return await (appLogTableSpecific.where('en').equals(entityID).sortBy('ts')) as AppLogVM[]
  }
}
// https://dexie.org/docs/Compound-Index#matching-first-part-only
export const appLogStoresTemplate = 'ha, tx, ts, ag, [en+at], [vl+at]'

const initialStores = {
  AppLogs: appLogStoresTemplate,
  Subscriptions: 'ID',
  Publications: 'ID',
  Situations: 'ID',
}
export type AppLogTable = Table<AppLogVM | AppLogObj, 'ha'>
export type SituationTable = Table<AppLogVM | AppLogObj, 'ha'>

export interface AppLogDB extends Dexie {
  parentDB?: BygonzDexie
  AppLogs: AppLogTable
  Subscriptions: SubscriptionTable
  Publications: SubscriptionTable
  Situations: SituationTable
  addSubscription: (subObj: Subscription) => Promise<string>
  getTable: (tablePath: string) => Promise<Table<AppLogVM, string>>
  getEntitiesAsOf: (asOf?: number) => Promise<{ entityArray: any[], entitiesGroupedByTablename: Record<string, any[]> }>
  getAllLocallyKnownAppLogs: (asOf?: number) => Promise<Array<AppLogVM & {tableName: string}>>
}

type requiredOnFirstCall = undefined
let _appLogDB
export const getAppLogDB = async (parentDB?: BygonzDexie | DexiePlus | requiredOnFirstCall): Promise<AppLogDB> => {
  if (_appLogDB) return _appLogDB
  if (!parentDB) WARN('needs parentDB for first call')
  // const Dexie = await getDexieFromCDN()
  class AppLogDB extends Dexie {
    AppLogs: AppLogTable
    Subscriptions: SubscriptionTable
    Publications: PublicationTable
    parentDB?: BygonzDexie | DexiePlus

    async getTable (tablePath): Promise<Table<AppLogVM, string>> {
      if (this._allTables[tablePath] && !this[tablePath]) { DEBUG(p(1), 'AppLogDB setting table from _allTables', this[tablePath] = this._allTables[tablePath]) }
      if (this[tablePath]) { return this[tablePath] }

      this.close() // https://dexie.org/docs/Dexie/Dexie.open()#dynamic-schema-manipulation
      this.on('blocked', () => false) // Silence console warning of blocked event.
      // const existingStores = this.tables.reduce((result, { name, schema }) => {
      //   result[name] = [
      //     schema.primKey.src,
      //     ...schema.indexes.map(idx => idx.src),
      //   ].join(',')
      //   return result
      // }, {})
      const newStores = {
        // ...existingStores,
        [tablePath]: appLogStoresTemplate,
      }
      const nextVer = +this.verno + 1 // TODO bail on this dynamic table thing... i think
      DEBUG(p(), { nextVer, newStores })
      this.version(nextVer).stores(newStores)
      this[tablePath].mapToClass(AppLogVM)

      await this.open()
      return this[tablePath]
    }

    async getAllLocallyKnownAppLogs (upUntil = utcMsTs()) {
      const appLogsUpUntil = await this.AppLogs.where('ts').belowOrEqual(upUntil).toArray() as Array<AppLogVM & { tableName: string}>
      VERBOSE({ upUntil, appLogsUpUntil, allAppLogs: !VERBOSE.isDisabled && await this.AppLogs.toArray() })
      return appLogsUpUntil
    }

    // TODO docs and ts magic ?@tennox?
    async getEntitiesAsOf (asOf = utcMsTs()) {
      const allLocallyKnownAppLogs = await this.getAllLocallyKnownAppLogs(asOf)
      const mappings = this.parentDB.mappings
      DEBUG('[mapAppLogArrayToEntities] all applogs:', { allLocallyKnownAppLogs, mappings })

      const entityArray = []
      const entitiesGroupedByTablename = {}

      const appLogsByEntityID = groupBy(allLocallyKnownAppLogs, 'en')
      const allEntityIDs = Object.keys(appLogsByEntityID)
      for (const eachID of allEntityIDs) {
        const sortedApplogs = sortBy(appLogsByEntityID[eachID], 'ts')
        // const { ts: created, ag: creator } = first(orderedByTs) // removed from entities and only use getters via applog
        // const { ts: modified, ag: modifier } = last(orderedByTs) // removed from entities and only use getters via applog
        let entityObject: Record<string, any> = { ID: eachID }
        for (const { at, vl } of sortedApplogs) {
          entityObject[at] = vl
        }
        let entityObjectWithoutTableName = { ...entityObject }
        delete entityObjectWithoutTableName.tableName
        if (mappings) {
          const tableName = entityObject.tableName
          if (!tableName) {
            // tableName = this.parentDB.tables[0].name // HACK
            if (!sortedApplogs.some(applog => applog.at.startsWith(':tx'))) {
              WARN('Missing tableName on', { entityObject, sortedApplogs, setToFirstTable: tableName })
            }
            continue
            // throw new Error('Missing tableName')
          }
          const ClassToMapTo = mappings[tableName]
          if (ClassToMapTo) {
            DEBUG('[mapAppLogArrayToEntities] mapping entity obj to', { entityObject, tableName, classToMapTo: ClassToMapTo })
            entityObject = new ClassToMapTo(entityObject)
            entityObjectWithoutTableName = new ClassToMapTo(entityObjectWithoutTableName)
          } else {
            WARN('tablename not in mappings', { tableName, mappings })
          }
        }
        entityArray.push(entityObject)
        entitiesGroupedByTablename[entityObject.tableName]
          ? entitiesGroupedByTablename[entityObject.tableName].push(entityObjectWithoutTableName)
          : entitiesGroupedByTablename[entityObject.tableName] = [entityObjectWithoutTableName]
      }
      DEBUG('reduced', { fromLogs: allLocallyKnownAppLogs, entityArray, entitiesGroupedByTablename })
      return { entityArray, entitiesGroupedByTablename }
    }

    async addSubscription (subObj: Subscription): Promise<string> {
      const { Subscription: SubscriptionConst } = await import('./Subscriptions')
      this.Subscriptions?.mapToClass(SubscriptionConst)
      DEBUG('Adding subscription:', subObj)

      return await this.Subscriptions.add(new SubscriptionConst(subObj))
    }

    constructor () {
      super(`_${parentDB.name}`)
      this.parentDB = parentDB
      this.version(2).stores(initialStores)
      this.AppLogs?.mapToClass(AppLogVM)
    }
  }

  _appLogDB = new AppLogDB()
  // if (!(await Dexie.exists(_appLogDB.name))) {
  //   LOG('Creating DB for first time', _appLogDB.name)
  // _appLogDB.version(1).stores(initialStores)
  // _appLogDB.AppLogs.mapToClass(AppLogVM) // https://dexie.org/docs/Typescript#storing-real-classes-instead-of-just-interfaces
  // }
  // for (const eachTableName in initialStores) {
  //   _appLogDB[eachTableName] = _appLogDB._allTables[eachTableName]
  // }
  // await _appLogDB.open() // https://dexie.org/docs/Dexie/Dexie.open()#dynamic-schema-manipulation
  return _appLogDB
}

export async function getPreviousAppLogForAtom (atom: Pick<Atom, 'en' | 'at'>, appLogTable?: AppLogTable) {
  return Dexie.Promise.newPSD(async () => {
    if (!_appLogDB) ERROR('getPreviousAppLogForAtom called before AppLogDB was initialized... awwwkward')
    if (!appLogTable) appLogTable = (_appLogDB ?? await getAppLogDB()).AppLogs
    let returnAppLog: AppLogVM
    // DEBUG('skipping getPrevious')
    return await Dexie.waitFor((appLogTable.db as AppLogDB).transaction('r!', appLogTable, async () => {
      // Locked with exclamation mark ('r!') meaning new top-level transaction.
      // eg: return db.pets.where({owner: bert.id}).toArray();
      // DEBUG('creating inner AppLogDB tx')
      (await appLogTable.where(pick(atom, ['en', 'at'])).reverse().sortBy('ts'))[0] as AppLogVM
    }))
    // await (appLogTable.db as AppLogDB).transaction('r?', appLogTable, async () => {
    //   DEBUG('inside inner AppLogDB tx before where')
    //   returnAppLog = (await appLogTable.where(pick(atom, ['en', 'at'])).reverse().sortBy('ts'))[0] as AppLogVM
    //   DEBUG('inside inner AppLogDB tx AFTER where')
    // })
    // return returnAppLog
  })
}
