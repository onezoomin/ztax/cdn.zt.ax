import { filter, groupBy, sortBy } from 'lodash-es'
import type { Table } from 'dexie'

import { AppLogObj, WithAppLogDB } from './AppLog'
import { WithHashID } from './Common'

import { Logger } from '@ztax/logger'
import { Mixin } from 'ts-mixer'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export class Situation extends Mixin(WithHashID) {
  type: string
  info: Record<string, any>

  constructor (obj: any) {
    super(obj)
    Object.assign(this, obj)
  }
}
export class SituationVM extends Mixin(Situation, WithAppLogDB) {
  constructor (obj: any) {
    super(obj)
    Object.assign(this, obj)
  }
}

export type SituationTable = Table<Situation, 'ID'>
export type SituationFinderFx = (appLogsToCheck: AppLogObj[]) => Situation[]
export type SituationCheckerFx = (eachLog: AppLogObj, prevLog: AppLogObj) => boolean

export const defaultSequentialReportFx = (eachLog: AppLogObj, prevLog: AppLogObj) => ({ before: prevLog.vl, after: eachLog.vl })

export const findSituationsBetweenSequentialEnAtAtoms = (appLogsToCheck: AppLogObj[], finderFx: SituationCheckerFx, type, reportFx = defaultSequentialReportFx) => {
  const withEnAt = appLogsToCheck.map((eachLog: AppLogObj) => ({
    ...eachLog,
    'en/at': `${eachLog.en}/${eachLog.at}`,
  }))
  const groupedByEnThenAt = filter(groupBy(withEnAt, 'en/at'), (o) => o.length > 1)

  const situations: Situation[] = []
  let prevLog
  for (const eachEnAtPairWithMoreThanOneLog of groupedByEnThenAt) {
    sortBy(eachEnAtPairWithMoreThanOneLog, 'ts')
    for (const eachLog of eachEnAtPairWithMoreThanOneLog) {
      VERBOSE(eachLog, prevLog)
      if (prevLog && finderFx(eachLog, prevLog)) {
        VERBOSE('situation found', { type, eachLog, prevLog })
        const logsWithSituation = [eachLog.ha, prevLog.ha].sort() // eslint-disable-line @typescript-eslint/require-array-sort-compare
        const situationEntry = new Situation({ type, info: { 'en/at': eachLog['en/at'], ...reportFx(eachLog, prevLog), logsWithSituation } })
        situations.push(situationEntry)
      }
      prevLog = eachLog
    }
  }

  DEBUG('grouped Logs', { groupedByEnThenAt })
  return situations
}

export const attributeOverwriteSituation = (type: string, tsThreshold: number): SituationFinderFx => {
  const isAttributeSettingTooRecent = (eachLog, prevLog) => (Math.abs(eachLog.ts - prevLog.ts) < tsThreshold)
  return (appLogsToCheck: AppLogObj[]) => findSituationsBetweenSequentialEnAtAtoms(appLogsToCheck, isAttributeSettingTooRecent, type)
}

export const attributeTwiceInSameTxSituation = (type = 'doubleSetAttr') => {
  const isTxContainingMultiAtSetting = (eachLog, prevLog) => (eachLog.tx === prevLog.tx && eachLog.en === prevLog.en && eachLog.at === prevLog.at)
  // TODO find appropriate generic situation finder for this integrity check
  return (appLogsToCheck: AppLogObj[]) => findSituationsBetweenSequentialEnAtAtoms(appLogsToCheck, isTxContainingMultiAtSetting, type)
}
