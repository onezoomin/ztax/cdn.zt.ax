import type { Subscription } from '../Subscriptions'

import Mutex from 'await-mutex'
import { Logger } from '@ztax/logger'
import { BygonzPub, BygonzSub } from './BygonzSubPub'

import { AppLogDB, AppLogObj } from '../AppLog'

// TODO: link bygonz-rs instead of relative path
import * as BygonzWasm from '../../../bygonz-wasm/dist/'

import type * as BygonzWasmType from 'bygonz-wasm'
import WasmFile from 'bygonz-wasm/bygonz_wasm_bg.wasm'
import { Publication } from '../Publications'

const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[WW.ipfs]', performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

let wasm: BygonzWasmType.BygonzWasm
const wasmInitMutex = new Mutex()
let wasmDBName: string
let wasmError: boolean
export async function ensureWasmInit (dbName: string, options: any) {
  DEBUG('WASM INIT?', { wasm, wasmDBName })
  const unlockMutex = await wasmInitMutex.lock()
  try {
    if (wasmError) return WARN('Skipping another wasm init attempt bc. last resulted in error') // so we don't cause some costly re-initialization attempt loops
    if (!wasm) {
      VERBOSE('WASM PRE-INIT', BygonzWasm, { wasm, wasmDBName })
      await BygonzWasm.default(WasmFile) /* .initSync(module) */
      DEBUG('WASM INIT', BygonzWasm, { options })
      wasmDBName = dbName
      wasm = await BygonzWasm.init_bygonz(`_${dbName}`, {
        ipfs: options.ipfs,
      })
    } else {
      // just to make sure subsequent invocations of initWasm will not pass a different dbName. we currently assume there's only one:
      if (wasmDBName !== dbName) throw new Error(`Unsupported: createIpfsSubHandler called with different dbName '${dbName}' (last: '${wasmDBName}')`)
    }
  } catch (err) {
    ERROR('Failed to init WASM:', err)
    wasmError = true
    // TODO: if (err.message === "Failed to fetch")
    throw err
  } finally {
    unlockMutex()
  }
}

export async function createIpfsSubHandler (dbName: string, options: any, subscription: Subscription, onUpdate: (/* newLogs: AppLogObj[] */) => Promise<void> | void): Promise<IpfsSub> {
  const self = new IpfsSub()
  self.subscription = subscription
  self.updateHandler = onUpdate
  DEBUG('[IPFS.createSub]', self)

  await ensureWasmInit(dbName, options)

  await wasm.init_subscription(subscription.ID, self.handleUpdate)

  return self
}
export class IpfsSub extends BygonzSub {
  // wasm: BygonzWasm.Bygonz
  identity: object
  subscription: Subscription
  updateHandler: () => void | Promise<void>

  handleUpdate = async (msg) => {
    DEBUG('[pubsub] message:', msg)
    try {
      await this.updateHandler(/* msg.atoms */)
    } catch (err) {
      ERROR('Failed to handle pubsub message', err, msg, { sub: this.subscription })
    }
  }
}

export async function createIpfsPubHandler (dbName: string, options: any, publication: Publication): Promise<IpfsPub> {
  const self = new IpfsPub()
  self.publication = publication
  DEBUG('[IPFS.createPub]', self)

  await ensureWasmInit(dbName, options)
  await wasm.init_publication(publication.ID)

  return self
}
export class IpfsPub extends BygonzPub {
  // wasm: BygonzWasm.Bygonz
  identity: object
  publication: Publication
  mutex: Mutex

  constructor () {
    super()
    this.mutex = new Mutex()
  }

  async persist (db: AppLogDB, appLogs: AppLogObj[]) {
    DEBUG('[IPFS.persist] awaiting mutex', { appLogs })
    const unlockMutex = await this.mutex.lock()
    try {
      LOG('[IPFS.persist]', { pub: this.publication, db, appLogs })
      // await LOG.group('[IPFS.persist]', { pub: this.publication, db, appLogs }, async () => {
      await wasm.publish_applogs(appLogs)
      // })
    } finally { unlockMutex() }
  }
}
