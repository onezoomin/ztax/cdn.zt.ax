
// // import OrbitDB from 'orbitdb'
// import Log from 'ipfs-log'
// // import * as IPFS from 'ipfs'
// import { DateTime } from 'luxon'
// import IdentityProvider from 'orbit-db-identity-provider'
// // import keystore from 'keystore-idb'
// // import { multiaddr } from '@multiformats/multiaddr'
// import * as IPFS from 'ipfs-http-client'
// import { Logger } from '@ztax/logger'
// import { AppLogTable, AppLogObj } from '../AppLog'
// import { asyncFilter } from './webnative-utils'

// const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[WW.ipfs]', performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

// let ipfs: IPFS.IPFSHTTPClient

// export async function initIPFS (info) {
//   if (!info.accountName) throw new Error('missing info.accountName')
//   if (!info.accountName) throw new Error('missing info.accountName')
//   if (!info.logId) throw new Error('missing info.logId')
//   DEBUG('Initializing IPFS...')
//   // const keystore = await keystore.init({ storeName: 'keystore' })
//   const identity = await IdentityProvider.createIdentity({ id: info.accountName })
//   DEBUG({ identity })
//   if (!ipfs) {
//     ipfs = IPFS.create({
//       url: 'http://127.0.0.1:5001/',
//     })
//   }

//   /* ipfs = await IPFS.create({ / * repo: "./path-for-js-ipfs-repo" * /
//     // start: false,
//     // config: {
//     // //   Bootstrap: [
//     // //     '/dns4/ipfs-rtc-star.tam.ma/tcp/443/wss/p2p-webrtc-star',
//     // //     // Leave this blank for now. We'll need it later
//     // //   ],
//     //   Addresses: {
//     //     Swarm: [
//     //     //   '/dns4/star.thedisco.zone/tcp/9090/wss/p2p-webrtc-star',
//     //       '/dns4/ipfs-rtc-star.tam.ma/tcp/443/wss/p2p-webrtc-star',
//     //     ],
//     //   },
//     // },
//     // libp2p: {
//     //   // p2p configuration
//     // },
//     EXPERIMENTAL: {
//       ipnsPubsub: true,
//     },
//   })
//   await ipfs.bootstrap.add(multiaddr('/dns4/ipfs-rtc-star.tam.ma/tcp/443/wss/p2p-webrtc-star'))
//   await ipfs.swarm.connect(multiaddr('/dns4/ipfs-rtc-star.tam.ma/tcp/443/wss/p2p-webrtc-star')) */
//   //   ipfs.once('ready', async function () {
//   //     DEBUG('[IPFS] ready')
//   //   })
//   // Create OrbitDB instance
//   //   orbitdb = await OrbitDB.createInstance(ipfs)

//   let log: Log
//   if (!info.logHash) {
//     log = new Log(ipfs, identity, {
//       logId: info.logId,
//       // access: ...
//     })
//     await log.append({ GENESIS: DateTime.now().toISO() }) // HACK: cause we can't serialize an empty log
//   } else {
//     log = await Log.fromMultihash(ipfs, identity, info.logHash)
//   }
//   VERBOSE('log initialized:', log)
//   // // @ts-expect-error
//   // window.identity = identity
//   // // @ts-expect-error
//   // window.ipfs = ipfs
//   // // @ts-expect-error
//   // window.log = log
//   // //   window.orbitdb = orbitdb

//   //   log.events.on('replicated', (...args: any[]) => {
//   //     DEBUG('[ipfs-log] replicated', args)
//   //   })

//   //   await log.append('text')
//   VERBOSE('log:', log.values.map((e: any) => e.payload))
//   VERBOSE('heads:', log.heads.map((t: any) => t.hash))
//   const logHash = await log.toMultihash()
//   DEBUG('LOG ADDRESS:', logHash)
//   // await ipfs.pubsub.publish('manutest', Buffer.from(logHash))

//   LOG('Initialized IPFS', { identity, ipfs, log/* , orbitdb */ })

//   // eslint-disable-next-line @typescript-eslint/no-misused-promises
//   // setTimeout(async () => {
//   //   LOG('Starinting benchmark...')
//   //   const start = performance.now()
//   //   for (let i = 0; i < 1000; i++) {
//   //     if (i % 100)LOG(`[${i}]`, (performance.now() - start))
//   //     const element = { i, test: '29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd29t38gfbensd' }
//   //     await log.append(element)
//   //   }
//   //   const logHash = await log.toMultihash()
//   //   LOG('DON - TOTAL', (performance.now() - start), logHash)
//   // }, 500)

//   return { ipfs, identity, log, logHash }
// }

// export const saturateAppLogsFromIpfs = async (log: Log, appLogTable: AppLogTable) => {
//   DEBUG('saturate from', log)
//   const appLogs = log.values.map((e: any) => e.payload)
//     .filter(op => !op.GENESIS)
//   DEBUG('raw appLogs', appLogs)
//   // const mappedAppLogs = appLogs.map(((op: any) => new AppLogObj(op)))
//   DEBUG('raw appLogs', appLogs)
//   const unknownAppLogsToBulkAdd = await filterOutKnownAppLogs(appLogTable, appLogs)
//   DEBUG('new appLogs', unknownAppLogsToBulkAdd, 'adding to', appLogTable)
//   unknownAppLogsToBulkAdd.length && (await appLogTable.bulkPut(unknownAppLogsToBulkAdd))
//   return { added: unknownAppLogsToBulkAdd, found: appLogs.length }
// }

// const filterOutKnownAppLogs = async (AppLogsTable, appLogArray: AppLogObj[] = []): Promise<AppLogObj[]> => {
//   return await asyncFilter(appLogArray,
//     async (eachAppLogObj: AppLogObj) => {
//       return !(await AppLogsTable.get(eachAppLogObj.tx))
//     })
// }
