import Surreal from 'surrealdb.js'

export async function surrealTest () {
  try {
    // const { default: Surreal } = await import('surrealdb.js/dist/web')
    console.log({ Surreal })
    await Surreal.Instance.connect('https://sdb.tam.ma/rpc')
    // Signin to a scope from the browser
    await Surreal.Instance.signin({
      // NS: 'test',
      // DB: 'test',
      // SC: 'user',
      user: 'foo',
      pass: 'bar',
    })

    // Select a specific namespace / database
    await Surreal.Instance.use('test', 'test')

    // Create a new person with a random id
    const created = await Surreal.Instance.create('person', {
      title: 'Founder & CEO',
      name: {
        first: 'Tobie',
        last: 'Morgan Hitchcock',
      },
      marketing: true,
      identifier: Math.random().toString(36).substr(2, 10),
    })

    // Update a person record with a specific id
    const updated = await Surreal.Instance.change('person:jaime', {
      marketing: true,
    })

    // Select all people records
    const people = await Surreal.Instance.select('person')

    // Perform a custom advanced query
    const groups = await Surreal.Instance.query('SELECT marketing, count() FROM type::table($tb) GROUP BY marketing', {
      tb: 'person',
    })

    console.log('surreal results', { created, updated, people, groups })
  } catch (e) {
    console.error('ERROR', e)
  }
}
