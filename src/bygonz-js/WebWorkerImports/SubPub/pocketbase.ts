import PocketBase from 'pocketbase'
import { utcMsTs } from '../Utils'
import type { Subscription } from '../Subscriptions'

import { Logger } from '@ztax/logger'
import { BygonzDexie } from '../../Bygonz'
import { BygonzSub } from './BygonzSubPub'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export class PocketbaseSubPub extends BygonzSub {
  static SubcriptionHandlers = {
    wnfs: {
      async saturate (sub: Subscription, targetDB: BygonzDexie) {
        const added = []
        // const appLogTable = targetDB?.appLogDB?.AppLogs
        // if (!appLogTable) throw new Error('required: targetDB?.appLogDB?.AppLogs')
        // const { info: { appCreator, appName, accountName } } = sub
        // VERBOSE('sat', { sub, appCreator, appName, accountName })
        // const { added, found } = await saturateAppLogsViaCar(accountName, appCreator, appName, appLogTable)
        // LOG(`Added ${added.length} AppLogs from ${accountName}'s ${found.length} files`)
        // void sub.setLogCount(found.length)
        // void targetDB.appLogDB.setLogCountForSubscription(sub.ID, found.length)
        return added.length
      },
    },
  }

  static test = async () => {
    const client = new PocketBase('https://pb.tam.ma')

    const userAuthData1 = await client.users.authViaEmail('sudo@zt.ax', 'sudo123!')
    const ts = utcMsTs()
    const data = {
      ag: 'default_wnative',
      at: 'color',
      en: '1d7e8d93789',
      ts,
      tx: `${ts}_1d7e8d93789_2e8921a16a2`,
      vl: '#af8ebb',
    }
    const record = await client.records.create('applog', data)
    console.log({ userAuthData1, record })
    // fetch a paginated records list
    const resultList = await client.records.getList('applog', 1, 50, {
      filter: 'created >= "2022-01-01 00:00:00"',
    })

    // alternatively you can also fetch all records at once via getFullList:
    const records = await client.records.getFullList('applog', 200 /* batch size */, {
      sort: '-created',
    })
    console.log({ resultList, records })
    return { success: true, results: records }
  }
}
