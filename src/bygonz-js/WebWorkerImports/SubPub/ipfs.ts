// import { saturateAppLogsViaCar } from './webnative-utils'
// import type { Subscription } from '../Subscriptions'

// import { Logger } from '@ztax/logger'
// import { BygonzDexie } from '../../Bygonz'
// import { BygonzSubPub } from './BygonzSubPub'
// import { initIPFS, saturateAppLogsFromIpfs } from './ipfs-utils'
// import { IPFSHTTPClient } from 'ipfs-http-client/dist/src'
// import Log from 'ipfs-log'
// import { AppLogDB, AppLogVM } from '../AppLog'
// import { Dexie, Table } from 'dexie'
// const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[WW.ipfs]', performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

// const dexie: Dexie & {logPos: Table} = new Dexie('bygonz_ipfs_persist')
// dexie.version(1).stores({
//   logPos: 'id, hash',
// })

// export async function createIpfsSubHandler (subscription: Subscription, onUpdate: () => Promise<void> | void): Promise<IpfsSubPub> {
//   DEBUG('[IPFS.create]')
//   const self = new IpfsSubPub()
//   self.subscription = subscription

//   DEBUG('Checking logPos', dexie)
//   const logPos = await dexie.logPos
//     .where('id').equals(subscription.info.logId)
//     .first()
//   LOG('Existing log?', logPos)
//   const initResult = await initIPFS({ ...subscription.info, logHash: logPos?.hash })
//   if (!logPos) await dexie.logPos.add({ id: subscription.info.logId, hash: initResult.logHash })

//   self.ipfs = initResult.ipfs
//   self.identity = initResult.identity
//   self.log = initResult.log

//   // Listen to PubSub channel
//   await self.ipfs.pubsub.subscribe(`bygonz_${subscription.info.logId}`, async (msg) => {
//     DEBUG('[pubsub] message:', msg)
//     try {
//       const previousPos = await dexie.logPos
//         .where('id').equals(subscription.info.logId)
//         .first()
//       const newHash = Buffer.from(msg.data).toString()
//       LOG('[pubsub] update:', previousPos?.hash, '->', newHash)
//       if (previousPos?.hash === newHash) {
//         DEBUG('Already at latest - so it\'s probably from us :P')
//         return // HACK: uncomment for realtime testing on single device
//       }
//       await dexie.logPos.update(subscription.info.logId, { hash: newHash })
//       // HACK: obviously, we wouldn't just overwrite our own log pos
//       await onUpdate()
//     } catch (err) {
//       ERROR('Failed to handle pubsub message', err, { msg })
//     }
//   })

//   return self
// }
// export class IpfsSubPub extends BygonzSubPub {
//   ipfs: IPFSHTTPClient
//   identity: object
//   subscription: Subscription
//   log: Log

//   async saturate (db: AppLogDB) {
//     console.groupCollapsed('[IPFS.saturate]', { sub: this.subscription, db })
//     try {
//       const appLogTable = db?.AppLogs
//       if (!appLogTable) throw new Error('required: appLogDB?.AppLogs')

//       const { info: { logId } } = this.subscription
//       DEBUG('Which pos?:', logId)
//       const logPos = await dexie.logPos
//         .where('id').equals(logId)
//         .first()
//       if (!logPos) throw new Error('Missing logPos?!')
//       LOG('Fetching IPFS-Log:', logPos)
//       const log = await Log.fromMultihash(this.ipfs, this.identity, logPos.hash)
//       LOG('Successfully fetched IPFS-Log:', log)

//       // VERBOSE('sat', { sub: this.subscription, appCreator, appName, accountName })
//       LOG('Saturating AppLogTable:', appLogTable)
//       const { added, found } = await saturateAppLogsFromIpfs(log, appLogTable)
//       DEBUG(`Added ${added} AppLogs from ${found} IPFS-Log entries`)
//       // void this.subscription.setLogCount(found)
//       // void targetDB.appLogDB.setLogCountForSubscription(sub.ID, found.length)
//       return added
//     } finally { console.groupEnd() }
//   }

//   async persist (db: AppLogDB) {
//     console.groupCollapsed('[IPFS.persist]', { sub: this.subscription, db })
//     try {
//     // const appLogTable = db?.AppLogs
//       const locallyKnownAppLogArray = await db.getAllLocallyKnownAppLogs()
//       DEBUG('Source applogs:', locallyKnownAppLogArray, { db, log: this.log, ipfs: this.ipfs, identity: this.identity })

//       // ERROR('TODO IPFS')
//       VERBOSE('existing TXs:', this.log.values.map((e: any) => e.payload.tx), { values: [...this.log.values] })
//       let added = 0
//       const freshOpsList = [] as AppLogVM[]
//       for (const op of locallyKnownAppLogArray) {
//         if (this.log.values.some(v => v.payload.tx === op.tx)) { // HACK: TX WILL HAVE MULTIPLE OPS AT SOME POINT
//           VERBOSE('skipping existing op', op)
//         } else {
//           VERBOSE('persisting', op.tx, { op, log: this.log })
//           const entry = await this.log.append(op)
//           VERBOSE('persisted', { entry, log: this.log })
//           freshOpsList.push(op)
//           added++
//         }
//       }
//       DEBUG('Appended ', added, 'entries')

//       const logHash = await this.log.toMultihash()
//       DEBUG('log:', this.log.values.map((e: any) => e.payload))
//       DEBUG('heads:', this.log.heads.map((t: any) => t.hash))
//       DEBUG('Saving Log address:', logHash, this.subscription.info.logId)
//       await dexie.logPos.update(this.subscription.info.logId, { hash: logHash })
//       LOG('Publishing Log address:', logHash, this.log)
//       await this.ipfs.pubsub.publish(`bygonz_${this.subscription.info.logId}`, Buffer.from(logHash))
//     } finally { console.groupEnd() }
//   }
// }
