import { AppLogDB, AppLogObj } from '../AppLog'

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export abstract class BygonzSub {
  async saturate (appLogDB: AppLogDB): Promise<AppLogObj[]> {
    throw new Error('ABSTRACT')
  }
}
export abstract class BygonzPub {
  async persist (appLogDB: AppLogDB, appLogs: AppLogObj[]): Promise<void> {
    throw new Error('ABSTRACT')
  }
}
