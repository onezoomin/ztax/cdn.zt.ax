import type Twn from 'webnative'
import type { AppLogObj, AppLogDB } from '../AppLog'
import type { FileSystem as Twnfs } from 'webnative/fs/filesystem'
import type { DistinctivePath, FilePath, Path } from 'webnative/path'
import type { FileContent, IPFSPackage } from 'webnative/ipfs/index'
import type { Links } from 'webnative/fs/types'

import { isArray, debounce } from 'lodash-es'

import { allPromises, checkWorker, sleep } from '../Utils'
import { getDailyEpochTsObj, getEpochTsObj, logToYamlHumanTsKeys, yamlHumanTsKeysToAppLogObj } from '../Epochs'

import { unpackStream } from 'ipfs-car/unpack'
import { MemoryBlockStore } from 'ipfs-car/blockstore/memory'
import all from 'it-all'

import { Logger } from '@ztax/logger'
import { wnfsState } from './webnative'
const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: false }) // eslint-disable-line @typescript-eslint/no-unused-vars
const p = Logger.perf.bind(Logger, 1)

// const blockstore = new IdbBlockStore()
const blockstore = new MemoryBlockStore()

export let wn: typeof Twn
export let wnfs: Twnfs
export const setWnfs = (wnfsRef: Twnfs) => { wnfs = wnfsRef }
export const setWn = (wnRef) => { wn = wnRef }

export const getWNforWW = async () => {
  if (wn) return wn
  // wn = await import('webnative') // import and embed in webworker === bloat

  // const webnativeCDNURL = 'https://unpkg.com/webnative@0.34.2/dist/index.umd.min.js'
  // const importRes = self.importScripts(webnativeCDNURL) // works for umd global injection scripts - webnative should end up on self

  const webnativeCDNURL = 'https://unpkg.com/webnative@0.34.2/dist/index.js'
  // const webnativeCDNURL = 'https://ztax-cdn.fission.app/webnative/index.min.mjs'
  const importRes = await import(/* @vite-ignore */webnativeCDNURL)

  wn = importRes?.webnative
    ? importRes.webnative
  // @ts-expect-error
    : importRes?.initialize ? importRes : self.webnative
  DEBUG('check result vs wn', { wn, importRes })
  console.groupCollapsed(p(), ...checkWorker('first webNat load', { [`wn@${wn.VERSION}`]: wn }))
  return wn
}
export const initializeWnfsFromWorkerMsg = async (permissions?, username?): Promise<Twnfs> => {
  if (wnfs) return wnfs
  const wnRef = await getWNforWW()
  wnfsState.fissionPermissions = permissions
  wnfsState.fissionUsername = username

  // self.importScripts(wn.ipfs.DEFAULT_CDN_URL) // inserts into global scope
  // // @ts-expect-error
  // const ipfsPkg = self.IpfsCore as IPFSPackage
  // const ipfsNode = wn.ipfs.node
  // DEBUG(p(), 'Imported IpfsCore', { ipfsPkg, ipfsNode })
  // if (!ipfsNode) {
  //   wn.ipfs.set(
  //     await wn.ipfs.nodeWithPkg(ipfsPkg),
  //   )
  //   DEBUG(p(), 'Set IpfsCore', { wnIpfs: wn.ipfs })
  // }

  try {
    DEBUG(p(), 'trying loadFileSystem')
    setWnfs(await wnRef.loadFileSystem(permissions, username))
  } catch (e) {
    WARN('loadFileSystem failed', e)
  }
  console.groupEnd()
  return wnfs
}

export const handleWebnativePermissionsFromUI = async (dataFromMsg) => {
  const { permissions, username, tableNames = ['Colors'] } = dataFromMsg
  wnfs = await initializeWnfsFromWorkerMsg(permissions, username)
  DEBUG(p(), 'got wnfs - Ensuring main directories', { dataFromMsg, wn, wnfs })
  wnfsState.pubAppArray = ['public', permissions.app.creator, permissions.app.name]
  wnfsState.pubAppDir = wn.path.directory(...wnfsState.pubAppArray)

  if (!await wnfsWithRetries(wnfsState.pubAppDir, 'exists', wnfs, true)) {
    // saves 200ms but the ^check^ still takes ~180ms
    await wnfs.mkdir(wnfsState.pubAppDir)
  }

  for (const eachTableName of tableNames) {
    const eachPath = wn.path.directory(...wnfsState.pubAppArray, eachTableName)
    await wnfs.mkdir(eachPath)
  }
  await wnfs.publish()
  console.groupEnd(); LOG(p(), ...checkWorker('wnfs with main directories', { wnfs, wn }))
  return wnfs
}

// -----

export const getLsResultLinks = async (fullDirArr: string[]) => {
  const otherUserLS = await wnfsWithRetries({ directory: fullDirArr }, 'ls', wnfs)
  const lsLinks = Object.values(otherUserLS)
  return lsLinks
}
export const wnfsWithRetries = async (pathObject: DistinctivePath | DistinctivePath[], method = 'ls', wnfsRef = wnfs, silent = false, retriesRemaining = 6) => {
  // if (!('file' in pathObject || 'directory' in pathObject)) throw new Error('invalid pathObject')
  if (!wnfsRef) wnfsRef = await initializeWnfsFromWorkerMsg()
  const kncreatorrors = [
    '\'cid\'',
    'did not find any content to decode',
    'ArrayBuffer at index 0 is already detached',
    'attempting to access detached ArrayBuffer', // firefox
    'innerLinks.metadata',
    'metadata is undefined', // firefox
  ] // </ list of common errors that are harmless enough to ignore and retry

  let lsResults: {} | Links = {}
  const caughtErrors = []
  do { // try await wnfs.ls(objectWithDirProp)
    try {
      lsResults = isArray(pathObject) ? await wnfsRef[method](pathObject[0], pathObject[1]) : await wnfsRef[method](pathObject)
      retriesRemaining = 0
    } catch (e) {
      if (kncreatorrors.some((eachKnown) => e.message.includes(eachKnown))) {
        caughtErrors.push(e)
      } else {
        caughtErrors.push(e)
        console.error(e) // TODO harden to throw when wnfs upstream stabalizes enough
      }
      retriesRemaining--
    } // </ only
  } while (retriesRemaining) // </ continue trying until retries run out
  if (!silent) {
    console.groupCollapsed(method, ' for ', ('directory' in pathObject ? pathObject.directory : (pathObject as FilePath).file).join('/'), { neededRetries: caughtErrors.length })
    for (const eachCaughtE of caughtErrors) {
      DEBUG(eachCaughtE)
    }
    console.groupEnd()
  } // </ only trace to console if !silent
  return lsResults
}

// deprecated for car file strategy
const _fetchAndDecodeSingleFile = async (url: string, wnfs?: Twnfs, filePath?: FilePath): Promise<AppLogObj[]> => {
  DEBUG('fetching', url)
  const fetchResponse: Response | FileContent = !wnfs
    ? await fetch(url)
    : await wnfs.cat(filePath)
  VERBOSE('directFetchResponse', fetchResponse)
  const retVal = !wnfs
    ? fetchResponse ? yamlHumanTsKeysToAppLogObj(await (fetchResponse as Response).text()) : [] // fetch parsing
    : fetchResponse ? yamlHumanTsKeysToAppLogObj(new TextDecoder().decode(fetchResponse as Uint8Array)) : [] // wnfs.cat parsing
  DEBUG({ toGetAndParseEntry: retVal })
  return retVal
}

export const getEpochsWithDirectories = (fullDirArr: string[], asOf?: number, entity = 'Colors') => {
  const epochs = getEpochTsObj(asOf)
  const { daily } = epochs
  for (const eachEpoch of Object.values(epochs)) {
    const { ts, hr } = eachEpoch
    eachEpoch.dir = { directory: [...fullDirArr, entity, daily.hr, `${ts.toFixed(0)}_${hr}`] }
  }
  VERBOSE({ epochs })
  return epochs
}

const _moveMinutesIntoDays = async (fullDirArr: string[]) => {
  DEBUG('getting top level ls', fullDirArr)
  const allSubdirs = await getLsResultLinks(fullDirArr)
  const moveMap = new Map()
  for (const eachSubdir of allSubdirs) {
    if (eachSubdir.name.includes('_')) {
      const eachTs = +eachSubdir.name.split('_')[0]
      const appropriateEpoch = getDailyEpochTsObj(eachTs)
      moveMap.set(eachSubdir.name, {
        fromPath: { directory: [...fullDirArr, eachSubdir.name] },
        toPath: { directory: [...fullDirArr, appropriateEpoch.hr, eachSubdir.name] },
      })
    }
  }
  // const mvPromises = []
  DEBUG('moving', moveMap)
  for (const { fromPath, toPath } of [...moveMap.values()]) {
    const dirArr = toPath.directory.slice(0, -1)
    const dirPath = { directory: dirArr }
    const dirPathAsFile = { file: dirArr }
    // DEBUG('checking', dirPathAsFile)
    const dirExists = !await wnfs.exists(dirPathAsFile)
    if (dirExists) {
      await wnfsWithRetries(dirPath, 'mkdir', wnfs, true)
    }
    DEBUG('moving', { fromPath, toPath })
    await wnfsWithRetries([fromPath, toPath], 'mv', wnfs, true)
    await wnfs.publish()
    await sleep(100)
  }

  // await allPromises(mvPromises)
  // await wnfs.publish()
  DEBUG('done moving')
}
export const fetchAndUnpackFolderAsCar = async (userName = '1zoom', orgFolderName, appFolderName, entity = 'Colors', subPathStartingWithForwardSlash = '') => {
  // const defaultExampleURL = 'https://ipfs.io/ipfs/bafkreigh2akiscaildcqabsyg3dfr6chu3fgpregiymsck7e7aqa4s52zy?format=car'
  // const defaultExampleURL = 'https://ipfs.io/ipfs/bafybeighi6upe3gbxouopzkdxdpzwzi2ka52tqu3z5l6cygnpvzzqtzvsq?format=car'
  // const getsRedirectedTo = `https://1zoom-files-fission-name.ipns.dweb.link/p/${appNameObj.org}/${appNameObj.name}/Colors?format=car`
  // const gatewayIPNSurls = [`https://ipfs.io/ipns/${userName}.files.fission.name`, `https://ipfs.runfission.com/ipns/${userName}.files.fission.name`, `https://nftstorage.link/ipns/${userName}.files.fission.name`, `https://${userName}-files-fission-name.ipns.dweb.link`]
  const gatewayIPNSurls = [`https://nftstorage.link/ipns/${userName}.files.fission.name`, `https://${userName}-files-fission-name.ipns.dweb.link`]
  let retriesRemaining = (gatewayIPNSurls.length * 2) - 1
  let decodedAppLogArray = []

  const knownErrors = [
    'resolve',
  ] // </ list of common errors that are harmless enough to ignore and retry

  const caughtErrors = []

  while (retriesRemaining) {
    try {
      const baseURL = gatewayIPNSurls[Math.floor(retriesRemaining / 2)] // 2 tries each
      const defaultExampleURL = `${baseURL}/p/${orgFolderName}/${appFolderName}/${entity}${subPathStartingWithForwardSlash}?format=car`
      const res = await fetch(defaultExampleURL)
      VERBOSE({ res, carFetchBody: res.body })
      if (res.status !== 200) { // TODO are there other successful codes?
        retriesRemaining--
        await sleep(500)
        continue
      }
      const files = new Set<string>()
      const directories = []

      let duplicates = 0
      let mergedYamlLog = ''

      for await (const file of unpackStream(res.body, { blockstore })) {
        // Iterate over files
        if (file.type === 'directory') {
          directories.push(file)
        } else {
          const unwrappedIterator = await all(file.content())
          const decodedContents = new TextDecoder().decode(unwrappedIterator[0])
          files.has(decodedContents) ? duplicates++ : files.add(decodedContents)
        }
      }
      for (const eachLog of files) {
        mergedYamlLog += eachLog
      }

      decodedAppLogArray = mergedYamlLog.length ? yamlHumanTsKeysToAppLogObj(mergedYamlLog) : []
      DEBUG(p(), { duplicates, directories, files, blockstore, asAppLog: decodedAppLogArray })
      retriesRemaining = 0
    } catch (e) {
      if (knownErrors.some((eachKnown) => e.message.includes(eachKnown))) {
        caughtErrors.push(e)
      } else {
        caughtErrors.push(e)
        console.error(e) // TODO harden to throw when wnfs upstream stabalizes enough
      }
      await sleep(500)
      retriesRemaining--
    }
  } // try each tld twice

  return decodedAppLogArray
  // blockstore.destroy()
}
const filterOutKnownAppLogs = async (AppLogsTable, appLogArray: AppLogObj[] = []): Promise<AppLogObj[]> => {
  return await asyncFilter(appLogArray,
    async (eachAppLogObj: AppLogObj) => {
      return !await AppLogsTable.get(eachAppLogObj.tx)
    })
}
export const saturateAppLogsViaCar = async (userName: string, appCreator: string, appName: string, AppLogsTable) => {
  const appLogsfromCar = await fetchAndUnpackFolderAsCar(userName, appCreator, appName)
  const unknownAppLogsToBulkAdd = await filterOutKnownAppLogs(AppLogsTable, appLogsfromCar)
  unknownAppLogsToBulkAdd.length && await AppLogsTable.bulkPut(unknownAppLogsToBulkAdd)
  return { added: unknownAppLogsToBulkAdd, found: appLogsfromCar }
}

export const asyncFilter = async (arr, predicate) => {
  const results = await allPromises(arr.map(predicate))
  return arr.filter((_v, index) => results[index])
}

export declare interface NonDistinctivePath {
  directory?: Path
  file?: Path
}
