import { fetchAndUnpackFolderAsCar, getEpochsWithDirectories, saturateAppLogsViaCar } from './webnative-utils'
import type { Subscription } from '../Subscriptions'
import type Twn from 'webnative'
import type { AppLogObj, AppLogDB } from '../AppLog'
import type { FileSystem as Twnfs } from 'webnative/fs/filesystem'
import type { DistinctivePath, FilePath, Path } from 'webnative/path'
import type { FileContent, IPFSPackage } from 'webnative/ipfs/index'
import type { Links } from 'webnative/fs/types'

import { isArray, debounce } from 'lodash-es'

import { allPromises, checkWorker, sleep } from '../Utils'
import { getDailyEpochTsObj, getEpochTsObj, logToYamlHumanTsKeys, yamlHumanTsKeysToAppLogObj } from '../Epochs'

import { unpackStream } from 'ipfs-car/unpack'
import { MemoryBlockStore } from 'ipfs-car/blockstore/memory'
import all from 'it-all'

import { Logger } from '@ztax/logger'
import { BygonzDexie } from '../../Bygonz'

const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

export const wnfsState = {
  pubAppArray: null,
  pubAppDir: null,
  fissionUsername: null,
  fissionPermissions: null,
}
const tracker = {
  adds: [],
  addsAvg: 0,
  pubs: [],
  pubsAvg: 0,
}

// export async function createWnfsSubHandler (subscription: Subscription): Promise<WnfsSubPub> {
//   DEBUG('[WNFS.create]')
//   const self = new WnfsSubPub()
//   self.subscription = subscription
//   return self
// }

// export class WnfsSubPub extends BygonzSubPub {
//   // static wnfsState = null
//   subscription: Subscription

//   async saturate (db: AppLogDB) {
//     const appLogTable = db?.AppLogs
//     if (!appLogTable) throw new Error('required: targetDB?.appLogDB?.AppLogs')
//     const { info: { appCreator, appName, accountName } } = sub
//     VERBOSE('sat', { sub: this.subscription, appCreator, appName, accountName })
//     const { added, found } = await saturateAppLogsViaCar(accountName, appCreator, appName, appLogTable)
//     LOG(`Added ${added.length} AppLogs from ${accountName}'s ${found.length} files`)
//     void this.subscription.setLogCount(found.length)
//     // void targetDB.appLogDB.setLogCountForSubscription(sub.ID, found.length)
//     return added.length
//   }

//   async persist (appLogDB: AppLogDB) {
//     const appLogTable = appLogDB.AppLogs
//     LOG('Source applogs:', appLogTable)

//     if (!wnfs) { return WARN('cant persist without initialized wnfs') }
//     const { 0: { dir: currentEpochDir } } = getEpochsWithDirectories(wnfsState.pubAppArray)
//     await wnfs.mkdir(currentEpochDir)

//     const appLogArrayFromCar = await fetchAndUnpackFolderAsCar(
//       wnfsState.fissionUsername,
//       wnfsState.fissionPermissions.app.creator,
//       wnfsState.fissionPermissions.app.name,
//     )
//     if (!appLogArrayFromCar?.length) WARN('fetchAndUnpackFolderAsCar returned bum result', appLogArrayFromCar)

//     const appLogsFromCarIds = appLogArrayFromCar.map(l => l.tx)
//     const locallyKnownAppLogArray = await appLogDB.getAllLocallyKnownAppLogs()
//     const locallyKnownNeedingToBePublished = locallyKnownAppLogArray.filter(eachLocalLog => !appLogsFromCarIds.includes(eachLocalLog.tx))
//     DEBUG({ appLogDB, locallyKnownNeedingToBePublished, locallyKnownAppLogArray, appLogArrayFromCar })

//     let pf = performance.now()
//     const locallyKnownCount = locallyKnownAppLogArray.length
//     const remotelyKnownCount = appLogsFromCarIds.length

//     if (locallyKnownNeedingToBePublished.length) {
//       console.groupCollapsed(p(), 'writing logs to wnfs', { locallyKnownCount, remotelyKnownCount, locallyKnownNeedingToBePublished })

//       const addPromises = []
//       tracker.adds = [] // reset for each run
//       for (const log of locallyKnownNeedingToBePublished) {
//         const fileName = `${log.tx}`
//         const { dir: appropriateEpochDir } = getEpochsWithDirectories(wnfsState.pubAppArray, log.ts)[0]
//         await wnfs.mkdir(appropriateEpochDir)
//         const filePath = wn.path.file(...appropriateEpochDir.directory, fileName)
//         const fileContents = logToYamlHumanTsKeys(log)
//         const unParsedAsAppLog = yamlHumanTsKeysToAppLogObj(fileContents)[0] // for checking
//         DEBUG({ fileName, unParsedAsAppLog })
//         addPromises.push(wnfs.add(filePath, fileContents))
//       }
//       await allPromises(addPromises)

//       tracker.adds.push(performance.now() - pf)
//       pf = performance.now()
//       await wnfs.publish()
//       tracker.pubs.push(performance.now() - pf)
//       tracker.addsAvg = tracker.adds.reduce((a: number, b: number) => a + b, 0) / addPromises.length // each run
//       tracker.pubsAvg = tracker.pubs.reduce((a: number, b: number) => a + b, 0) / tracker.pubs.length // cumulative

//       console.groupEnd(); LOG(p(), 'finished writing logs to wnfs ', { tracker })
//     } else {
//       VERBOSE('zero locallyKnownNeedingToBePublished')
//     }
//   }
// }
