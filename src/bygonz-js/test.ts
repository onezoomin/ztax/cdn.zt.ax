import { Logger } from '@ztax/logger'
import type { FriendsDB } from './Friends'
import { ConnectionVM, Friend, FriendVM, friendsDB } from './Friends'
import { sleep } from './WebWorkerImports/Utils'
import { liveQuery } from './index'
import type * as Tbygonz from './index'

// import { surrealTest } from './WebWorkerImports/SubPub/surreal'
// import { PocketbaseSubPub } from './WebWorkerImports/SubPub/pocketbase'

const { WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { performanceEnabled: true }) // eslint-disable-line @typescript-eslint/no-unused-vars

let runOnce
const doTest = async (_bygonz?: typeof Tbygonz, force = false, doLiveQuery = false) => {
  if (runOnce && !force)
    return console.warn('already runOnce')
  runOnce = true
  // await PocketbaseSubPub.test()
  // await surrealTest()
  if (doLiveQuery) {
    const friendsObservable = liveQuery(() => friendsDB.Friends.toArray())
    friendsObservable.subscribe({
      next: (result) => {
        LOG('Got  result:', { result })
      },
      error: error => console.error(error),
    })
  }
  let newbobage, bob
  const DELAY = 500
  const currentCount = await friendsDB.Friends.count()
  if (!currentCount) {
    await friendsDB.setup('friendMaker')
    let joesID, billsID
    await friendsDB.transaction('rw!', [friendsDB.Friends, friendsDB.Connections], async () => {
      const joe = new Friend({ name: 'Joe', age: 78 })
      LOG({ joe })
      joesID = await friendsDB.Friends.add(joe)
      const bill = new Friend({ name: 'Bill', age: 88 })
      billsID = await friendsDB.Friends.add(bill)
      LOG({ bill, billsID })
      await friendsDB.Friends.update(billsID, { name: 'Bills Getting Younger', age: 55 })
      await friendsDB.Friends.update(joesID, { name: 'The Artist Previously Known As Joe' })

      await friendsDB.Connections.add(new ConnectionVM({ fromFriend: billsID, toFriend: joesID }))
      LOG(await friendsDB.Connections.toArray())
    })
  } // </ only if there are no friends yet
  else if (currentCount < 8) {
    await friendsDB.transaction('rw!', friendsDB.Friends, async () => {
      const recentFriend = (await friendsDB.Friends.toArray())[1]
      LOG({ recentFriend })
      // const { ageClass, created = await recentFriend.getCreated(), nameHistory = await recentFriend.getNameHistory() } = recentFriend as FriendVM & {nameHistory: any, created: number}
      // LOG({ ageClass, created, nameHistory, prevNames: await recentFriend.getPreviousNamesArray(), recentFriend })
      newbobage = 28 + Math.round(Math.random() * 30) - 15
      bob = new FriendVM({ name: `bob-${newbobage}`, age: newbobage })
      const newsallyage = 32 + Math.round(Math.random() * 30) - 15
      const sally = new FriendVM({ name: `sally-${newsallyage}`, age: newsallyage })
      const newIds = await friendsDB.Friends.bulkAdd([bob, sally])
      DEBUG({ newIds, bob, sally })
      let bobage = +bob.age + 2
      // mixed put one new add and two updates
      const affected = await friendsDB.Friends.bulkPut([new FriendVM({ name: 'newbie', age: 28 + Math.round(Math.random() * 30) - 15 }), new FriendVM({ ...bob, age: bobage++ }), new FriendVM({ ...sally, age: sally.age - 2 })], { allKeys: true })
      // const affected = await db.Friends.bulkUpdateWouldBeNTH ([new FriendVM({ ID: bob.ID, age: bob.age + 2 }), new FriendVM({ ID: sally.ID, age: sally.age - 2 })], { allKeys: true })
      DEBUG({ affected })
      friendsDB.activeAgent = 'diffAgent007'
      const updated = await friendsDB.Friends.update(bob.ID, { name: 'Bobs Getting Older', age: ++bobage })
      DEBUG({ updated })
    })
    await sleep(500) // waiting to ensure bob's age change landed in the log
    const bobAgeHistory = await (new FriendVM(bob)).getAttributeHistory<FriendVM>('age')
    LOG({ bob, bobAgeHistory })
    const { entityArray, entitiesGroupedByTablename } = await friendsDB.getEntitiesAsOf()
    LOG({ entityArray, entitiesGroupedByTablename })
  }
  /* </ only if there are friends already */ else {
    const allFriendIDs = (await friendsDB.Friends.toArray()).map(eachF => eachF.ID)
    const rID1 = allFriendIDs[Math.floor(Math.random() * allFriendIDs.length)]
    const rID2 = allFriendIDs[Math.floor(Math.random() * allFriendIDs.length)]
    const newCnx = new ConnectionVM({ fromFriend: rID1, toFriend: rID2 })
    LOG('Add new Connection', { allFriendIDs, newCnx })

    await friendsDB.Connections.add(newCnx)
  } /* </ only if there  are >7 friends already */
  return { friendsDB, FriendVM, Friend, ConnectionVM }
}
type FriendsDB = typeof friendsDB
export { doTest, Logger, type FriendsDB }
