import type { DBCore, DBCoreAddRequest, DBCoreDeleteRangeRequest, DBCoreDeleteRequest, DBCoreMutateRequest, DBCorePutRequest, DBCoreTransaction, DbCoreTransactionOptions, IndexSpec, Middleware, Table } from 'dexie'

import Dexie from 'dexie'
import base85 from 'base85'
import keystore from 'keystore-idb'
import { Logger } from '@ztax/logger'
import { AppLogObj, createAtomsFromChangesets } from './WebWorkerImports/AppLog'
import type { Atom, MutMsg } from './WebWorkerImports/AppLog'
import type { GenericObject } from './WebWorkerImports/Utils'
import type { BygonzDexie } from './Bygonz'
// import { rsaVerify } from 'webnative/crypto/browser'
import { BYGONZ_MUTATION_EVENT_NAME, allPromises, cyrb53hash, frozen, utcMsTs } from './WebWorkerImports/Utils'
const { ERROR, WARN, LOG, DEBUG, VERBOSE } = Logger.setup(Logger.DEBUG, { prefix: '[bygMW]', performanceEnabled: false }) // eslint-disable-line @typescript-eslint/no-unused-vars
// @ts-expect-error
const { Promise: DexiePromise, Promise: { PSD } } = Dexie

type DBCoreTransactionBygonz = DBCoreTransaction & IDBTransaction & { tsCommon?: number; atomsToHash?: Atom[] }

const createUpdateMsg = function createUpdateMsg(en: IndexSpec['keyPath'], changeSpec: any, tableName: string, agentID: string, dbName: string, tsCommon: number) {
  VERBOSE('Creating Update Message', { en, ...changeSpec })

  const {
    modified: ts = tsCommon ?? utcMsTs(), // timestamp - if the entity includes modified it will trump setting it here
    modifier: ag = agentID, // if the entity includes modifier it will trump the agentID from the DB instance
  } = changeSpec
  const mutMsg = {
    op: 'update',
    ts,
    ag,
    en,
    obj: changeSpec,
    tableName,
    dbName,
  }
  DEBUG('4. created update msg', mutMsg)
  return mutMsg
}
const createAddMsg = function createAddMsg(en: string, entityObj: any, tableName: string, agentID: string, dbName: string, tsCommon: number) {
  VERBOSE('Creating Add Message', entityObj)

  const {
    creator,
    created = tsCommon ?? utcMsTs(),

    modified: ts = created, // timestamp - if the entity includes modified or created they will trump setting it here
    modifier: ag = creator ?? agentID, // if the entity includes modifier or creator they will trump the agentID from the DB instance
  } = entityObj
  entityObj.tableName = tableName // ADD ONLY - including tablename so that it gets included in the appLog
  entityObj[':en/type'] = tableName // TODO discuss tableName vs. :en/type
  const mutMsg: MutMsg = {
    op: 'add',
    ts,
    ag,
    en,
    obj: entityObj,
    tableName,
    dbName,
  }
  DEBUG('4. create(d)AddMsg', mutMsg)
  return mutMsg
}
const addAtomsForMsg = async (msg, reqCopy) => {
  const atoms = await createAtomsFromChangesets([msg])
  DEBUG('5. addAtomsForMsg', { msg, atoms, reqCopy })
  // Dexie.Promise.PSD.atomsToHash.push(...atoms)
  ;(reqCopy.trans as DBCoreTransactionBygonz).atomsToHash.push(...atoms)
}
const bc = new BroadcastChannel(BYGONZ_MUTATION_EVENT_NAME)
const getAppLogEffectsFor = (tableName, dbName) => {
  // const bc = new BroadcastChannel(BYGONZ_MUTATION_EVENT_NAME)
  VERBOSE('getAfterEffectsFor', tableName, bc)

  // these functions build anAppLogEntry to send over the broadcast channel
  return {
    async add(mutReq: (DBCoreAddRequest) & { primaryKeyPath: IndexSpec['keyPath']; keys: any[] }, tableRef: Table) {
      const agentID = (tableRef?.db as BygonzDexie)?.activeAgent ?? 'missingAgent_BygMW'
      const tsCommon = (mutReq.trans as DBCoreTransactionBygonz).tsCommon ?? utcMsTs()
      DEBUG('add', { agentID, tableRef, reqCopy: mutReq })
      for (const eachObj of mutReq.values) {
        const eachPriKey = eachObj[mutReq.primaryKeyPath.toString()]
        if (!mutReq.primaryKeyPath || !eachPriKey) { ERROR('Missing primary key||definintion', mutReq.primaryKeyPath, { reqCopy: mutReq, eachPriKey, tableRef }); throw new Error('Missing primary key||definintion') }
        const addMessage = createAddMsg(eachPriKey, eachObj, tableName, agentID, dbName, tsCommon)
        await addAtomsForMsg(addMessage, mutReq)

        // bc.postMessage(addMessage)
      }
    },
    async put(mutReq: DBCorePutRequest & { primaryKeyPath: IndexSpec['keyPath']; previousValueMap; previousValues: any[]; keys: any[] }, tableRef: Table) {
      // NOTE: this could be from update, put(as update/replace), put(as add), bulkPut(with adds, updates, or mixed)

      const agentID = (tableRef?.db as BygonzDexie)?.activeAgent ?? 'missingAgent_BygMW'
      const { primaryKeyPath, changeSpec, changeSpecs, previousValueMap } = mutReq
      const tsCommon = (mutReq.trans as DBCoreTransactionBygonz).tsCommon ?? utcMsTs()
      const isSingleUpdate = !!(changeSpec) // TODO check reliability
      if (isSingleUpdate) {
        DEBUG('put as single update by', { agentID, tableRef, reqCopy: mutReq })
        // TODO check with compound keys
        const whichID = mutReq.criteria.range.lower
        const updateMsg = createUpdateMsg(whichID, changeSpec, tableName, agentID, dbName, tsCommon)
        await addAtomsForMsg(updateMsg, mutReq)
        // bc.postMessage(updateMsg) // TODO kill bc
        return
      }
      if (changeSpecs)
        WARN('TODO unhandled plural changeSpecs')

      DEBUG('put without changeSpec', { agentID, tableRef, reqCopy: mutReq })
      for (const [_idx, eachObj] of mutReq.values.entries()) {
        const eachPriKey = eachObj[primaryKeyPath.toString()]
        if (previousValueMap[eachPriKey]) {
          const strippedPutEntity = { ...eachObj }
          for (const eachAttrName in eachObj) {
            if (previousValueMap[eachPriKey][eachAttrName] === eachObj[eachAttrName]) {
              delete strippedPutEntity[eachAttrName]
              VERBOSE('stripping', eachAttrName)
            }
          }
          const updateMsg = createUpdateMsg(eachPriKey, strippedPutEntity, tableName, agentID, dbName, tsCommon)
          await addAtomsForMsg(updateMsg, mutReq)

          // bc.postMessage(updateMsg)
        }
        else {
          const addMsg = createAddMsg(eachPriKey, eachObj, tableName, agentID, dbName, tsCommon)
          await addAtomsForMsg(addMsg, mutReq)

          // bc.postMessage(addMsg)
        }
      }
    },
    async delete(mutReq: DBCoreDeleteRequest & { primaryKeyPath: IndexSpec['keyPath']; previousValueMap; previousValues: any[]; keys: any[] }, tableRef: Table) {
      // TODO distinguish between mark as deleted and purge
      const ts = (mutReq.trans as DBCoreTransactionBygonz).tsCommon ?? utcMsTs()
      const en = mutReq.keys[0] // TODO ensure this will never be more than one
      const obj = mutReq.previousValues[0]
      const agentID = (tableRef?.db as BygonzDexie)?.activeAgent ?? 'missingAgent_BygMW'
      // console.log(' ae delete', reqCopy, obj)
      const appLogEntry: MutMsg = {
        ts,
        tableName,
        en,
        ag: obj.modifier ?? agentID ?? obj.creator ?? '??',
        obj,
        dbName,
        op: 'delete',
      }
      DEBUG('delete', appLogEntry)
      // bc.postMessage(appLogEntry)
    },
    deleteRange(mutReq: DBCoreDeleteRangeRequest) {
      DEBUG(' deleteRange - not broadcasting...yet', mutReq)
    },
  }
}
export const getBygonzMiddlwareFor = (instantiatedDBRef: BygonzDexie & { _middlewares: GenericObject }): Middleware<DBCore> => {
  const { activeAgent = '' } = instantiatedDBRef

  if (!activeAgent)
    WARN('bygonzDB instantiated without activeAgent', instantiatedDBRef.options)
  VERBOSE('creating middleware for', instantiatedDBRef)
  return {
    stack: 'dbcore', // The only stack supported so far.
    name: 'bygonz', // Optional name of your middleware
    create(core) {
      VERBOSE('creating middleware', core)
      // hooks middleware was causing problems, so we remove it via unuse in DexiePlus
      // instantiatedDBRef._middlewares.dbcore = instantiatedDBRef._middlewares.dbcore.filter(eachMW => eachMW.name !== 'HooksMiddleware')
      let appLogEffects

      const bygonzCore = {
        // Copy default implementation.
        ...core,

        // ripped/adapted from https://github.com/dexie/Dexie.js/blob/1ed96685dbb71639655cfdcf82f7670554538174/addons/dexie-cloud/src/middlewares/createMutationTrackingMiddleware.ts#L62
        transaction: (tableNames, mode, options: DbCoreTransactionOptions) => {
          const tx: DBCoreTransactionBygonz = core.transaction(
            [...tableNames],
            mode,
          ) as DBCoreTransactionBygonz
          VERBOSE(`1. tx init ${tx.mode}`, { tx, PSD, frozenPSD: frozen(PSD) })

          if (mode === 'readwrite' || mode.includes('rw')) {
            tx.atomsToHash = []
            tx.tsCommon = utcMsTs()
            VERBOSE('1. rw tx w commonts', { options, tx })

            const txComplete = async (evt: { target: DBCoreTransactionBygonz }) => {
              const { target: completedTx } = evt
              DEBUG(`tx ${tx.mode} - complete 1.`, { evt, completedTx, jsonTx: JSON.parse(JSON.stringify(tx)), frozenCompletedTx: frozen(completedTx), sameRef: (tx === completedTx) })

              if (!completedTx.atomsToHash?.length)
                return WARN('txComplete - no atomsToHash', completedTx)

              const appLogDB = await instantiatedDBRef.getAppLogDB()
              const { AppLogs: AppLogsTable, Situations: SituationsTable } = appLogDB

              const appLogs = await Dexie.waitFor(allPromises(completedTx.atomsToHash.map(
                eachAtom => AppLogObj.create({ ...eachAtom }),
              )))

              const sortedTxAtomCIDs = appLogs.map(at => at.ha).sort()
              const sortedTxAtomCIDsAsString = JSON.stringify(sortedTxAtomCIDs)

              const txHash = hashAtomsArray(appLogs)
              for (const applog of appLogs) {
                // DECIDED: we want the tx hash to be part of the atom.
                // this is meaningful as it binds together all atoms of the transaction
                applog.tx = txHash
              }

              // sign the deterministic hash using the private key of the activeAgent
              const keyStore = instantiatedDBRef.activeAgentKeystore ?? null // TODO decide if creating a keystore is needed/appropriate
              /* (await keystore.init({
                storeName: instantiatedDBRef.activeAgent,
                // @ ts-expect-error
                type: 'rsa',
              })) */
              VERBOSE(`tx ${tx.mode} - complete 1b. keystore`, { keyStore, isFromDB: instantiatedDBRef.activeAgentKeystore === keyStore, instantiatedDBRef })

              const { ag, ts } = completedTx.atomsToHash[0]

              const nonMatchingAtom = completedTx.atomsToHash.find(eachAtom => eachAtom.ts !== ts || eachAtom.ag !== ag)
              if (nonMatchingAtom)
                ERROR('found', { nonMatchingAtom }, 'what to do?')

              const signedTxHash = keyStore && await Dexie.waitFor(keyStore.sign(txHash)) // will be null if we have no keystore

              if (signedTxHash) {
                const encSig = base85.encode(Buffer.from(signedTxHash))
                DEBUG({ signedTxHash, encSig })
                const txSig = await Dexie.waitFor(AppLogObj.create({
                  ts,
                  ag,
                  en: txHash,
                  at: ':tx/sig',
                  vl: encSig,
                  pv: null,
                  op: true,
                }, txHash))
                appLogs.push(txSig)
              }

              const txAtomsLog = await Dexie.waitFor(AppLogObj.create({
                ts,
                ag,
                en: txHash,
                at: ':tx/atoms',
                vl: sortedTxAtomCIDsAsString,
                pv: null,
                op: true,
              }, txHash))
              appLogs.push(txAtomsLog)

              DEBUG(`tx ${tx.mode} - complete 2. inner applog `, { appLogDB, appLogs })
              await Dexie.waitFor(AppLogsTable.bulkAdd(appLogs))

              DEBUG(`tx ${tx.mode} - complete 3.`, { tx: completedTx, appLogs, signedTxHash, keyStore, activeAgent })
              // DEBUG({ writeKey: await Dexie.waitFor(keyStore?.publicWriteKey()) })

              bc.postMessage({ tx: txHash })

              const { options: { situationFinders } } = instantiatedDBRef

              if (situationFinders?.length) {
                const atomsToCheck = await AppLogsTable.orderBy('ts').toArray()
                for (const eachFinder of situationFinders) {
                  const foundSituations = await eachFinder(atomsToCheck)
                  DEBUG({ foundSituations })
                  if (foundSituations.length)
                    await SituationsTable.bulkPut(foundSituations)
                }
              }
              else {
                WARN('no situation founders found for', instantiatedDBRef)
              }

              // if (!DEBUG.isDisabled) {
              //   let atomsFromDB
              //   try {
              //     atomsFromDB = (await AppLogsTable.where({ tx: txHash }).toArray())
              //       .filter(atom => !atom.at.startsWith(':tx'))
              //       .map(atom => omit(atom, ['ha', 'tx']))
              //     await verifyTransaction(atomsFromDB, txHash, signedTxHash, keyStore)
              //   } catch (err) {
              //     ERROR('Failed to verify own transaction:', err, { appLogs, atomsFromDB, txHash, signedTxHash, keyStore })
              //   }
              // }
            } // </ txComplete function
            // @ts-expect-error
            tx.addEventListener('complete', txComplete, {})
            // tx.addEventListener('error', removeTransaction)
            // tx.addEventListener('abort', removeTransaction)
          }
          /* </only if mode is rw */ else {
            tx.addEventListener('complete', ({ target: completedTx }) => {
              const VERorBUG = tx.mode === 'readonly' ? VERBOSE : DEBUG
              VERorBUG(`tx ${tx.mode} - complete`, completedTx)
            }, {})
          }
          VERBOSE(`1c. Returning bygonzed ${tx.mode} tx:`, tx)
          return tx
        }, // </define custom transaction method

        // Override table method
        table(tableName) {
          // Call default table method

          const dbName = instantiatedDBRef.name
          const tableRef: Table = instantiatedDBRef[tableName]

          const downlevelTable = core.table(tableName)
          const primaryKeyPath = tableRef.schema.primKey.keyPath
          appLogEffects = getAppLogEffectsFor(tableName, dbName)

          DEBUG('Creating New Table', { downlevelTable, primaryKeyPath, appLogEffects })
          const mutate = async (incomingMutReq: DBCoreMutateRequest | any) => {
            // extract info from the request
            const { type, values, criteria, changeSpec, keys, trans: { tsCommon, db: { name: dbName } } } = incomingMutReq

            for (const eachObj of values) {
              for (const [key, value] of Object.entries(eachObj)) {
                if (typeof value === 'function')
                  delete eachObj[key]
              }
            }
            // Copy the request object and tag on bygonz schtuff
            const mutRequest = { ...incomingMutReq, tableRef, primaryKeyPath, tsCommon }

            const reqObjectExcerpt = { type, values, criteria, changeSpec, keys, dbName, primaryKeyPath, ':en/type': tableName, tsCommon }
            // PSD.trans = PSD.trans ?? mutRequest.trans
            DEBUG(`2. mut ${type}`, { myRequest: mutRequest, reqObjectExcerpt, PSD })

            // const afterEffectCallback = (completeEvent: Event) => {
            //   VERBOSE('ae callback', type, { myRequest, completeEvent, tableRef })
            //   appLogEffects[type](myRequest, tableRef)
            // }

            if (['put', 'delete'].includes(type)) {
              const previousValues = []
              const previousValueMap = {}
              for (const eachNewEntityBeforeUpdate of mutRequest.values) {
                const entityPK = eachNewEntityBeforeUpdate[primaryKeyPath.toString()]
                DEBUG('2b [put, delete] getting', { tableRef, entityPK })
                const existingEntity = await tableRef.get(entityPK)
                previousValues.push(existingEntity)
                previousValueMap[entityPK] = existingEntity
              }
              mutRequest.previousValues = previousValues
              mutRequest.previousValueMap = previousValueMap
            }

            DEBUG('3. REQUEST before calling appLogEffects', { myRequest: mutRequest, instantiatedDBRef, DexiePromise, jsonPSD: JSON.parse(JSON.stringify(PSD)), PSD, isPSDtransSame: PSD?.trans === mutRequest.trans })
            await appLogEffects[type](mutRequest, tableRef)

            DEBUG('6. AFTER appLogEffects, before calling other middleware mutate', { downlevelTable, myRequest: mutRequest, instantiatedDBRef, DexiePromise, PSD, isPSDtransSame: PSD?.trans === mutRequest.trans })

            // call downlevel mutate:
            // ! important not to await (i think) !
            return downlevelTable.mutate(mutRequest).then((res) => {
              // Do things after mutate

              const myResponse = { ...res }
              DEBUG('6. after other middleware mutate', { myResponse, instantiatedDBRef })

              if (mutRequest.trans.mode === 'readwrite') {
                // we are not quite through yet, tx could still fail or revert, so we can add a onComplete listener
                // we don't as we add one from the beginning of the tx
                DEBUG('6b. after calling downlevel table', { myRequest: mutRequest, instantiatedDBRef })
                // DEBUG('4.adding appLogs to tx AFTER calling other middleware mutate', { myRequest: mutRequest, instantiatedDBRef })
                // await appLogEffects[type](mutRequest, tableRef)
                // myRequest.trans.addEventListener('complete', afterEffectCallback)
              }
              // Then return your response:
              return myResponse
            })
          }

          // Return your extended table:
          return {
            // Copy default table implementation:
            ...downlevelTable,

            // adding extensible afterEffects
            // afterEffects: appLogEffects,

            // Override the mutate method:
            mutate,
          }
        },
      }
      DEBUG('created middleware', core, bygonzCore)
      // instantiatedDBRef._middlewares.dbcore = instantiatedDBRef._middlewares.dbcore.filter(eachMW => eachMW.name !== 'HooksMiddleware')

      // Return your own implementation of DBCore:
      return bygonzCore
    },
  }
}

const checkSig = async (msg, sig, keyStore) => {
  const writeKey1 = await keyStore.publicWriteKey()
  const ks2 = await keystore.init({
    storeName: 'bygonz_verify',
    // @ts-expect-error
    type: 'rsa',
  })
  const msgAsArray = new TextEncoder().encode(msg)
  DEBUG('Checking RSA signature:', { msg, msgAsArray, sig, keyStore, ks2 })
  const { rsaVerify } = await import('webnative/crypto/browser')

  const valid = await rsaVerify(msgAsArray, sig, writeKey1)
  // const valid = await ks2.verify(msg, sig, writeKey1) // ? why a second store? for testing?
  return valid
}

function hashAtomsArray(appLogs: AppLogObj[]) {
  const sortedTxAtomCIDs = appLogs.map(at => at.ha).sort()
  const sortedTxAtomCIDsAsString = JSON.stringify(sortedTxAtomCIDs)
  // const { cid: txCid } = await encodeDagJson(
  //   sortedTxAtomCIDs,
  // )
  return cyrb53hash(sortedTxAtomCIDsAsString, 31, 12) as string
}

async function verifyTransaction(
  atoms: Atom[],
  hashExpected: string,
  signedTxHash: string | null,
  keyStore,
) {
  const applogsForTx = await Dexie.waitFor(Promise.all(
    atoms.map(atom => AppLogObj.create(atom)),
  ))

  const hashCheck = hashAtomsArray(applogsForTx)
  if (hashCheck !== hashExpected) {
    ERROR('Invalid hash', { atoms, applogsForTx, hashCheck, hashExpected })
    throw new Error(`Invalid hash for ${signedTxHash} (${hashExpected})`)
  }

  if (signedTxHash && !await checkSig(hashCheck, signedTxHash, keyStore)) {
    ERROR('Invalid sig', { applogsForTx, signedTxHash, hashCheck })
    throw new Error(`Invalid sig for ${signedTxHash} (${hashExpected})`)
  }
  DEBUG('check txHash & sig success', { atoms, applogsForTx, hashExpected, signedTxHash })
}
