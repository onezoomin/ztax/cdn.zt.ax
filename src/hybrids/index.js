
import * as hybridsMod from 'hybrids'
import { doTest } from './test'
const hybrids = { ...hybridsMod, doTest }

// console.debug({ doTest, hybrids })
export { doTest, hybrids }
