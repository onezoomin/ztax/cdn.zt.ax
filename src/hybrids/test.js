let runOnce
const doTest = ({hybrids}, force = false) => {
    if (runOnce && !force) return console.warn('already runOnce')


    /**
     * hybrids
     */
    const { define, html } = hybrids
    define({
    tag: "h-w",
    name: 'world',
    content: ({ name }) => html`
        <div style="width:400px;height:50px;border:1px black solid">Hello ${name}!</div>
    `,
    });

    const HelloWorldElement = customElements.get("h-w")
    const el = new HelloWorldElement()
    var target = document.querySelector('#test')
    target.appendChild(el)

    const el2 = new HelloWorldElement()
    el2.setAttribute('name', 'If this block says something other than world, hybrids is working')
    target.appendChild(el2)



    runOnce = true
}

export {doTest}