let runOnce
const doTest = (packageObject) => {
    if (runOnce) return console.warn('already runOnce')

    // get expected packages from the bundle
    const {unocss,hybrids} = packageObject

    for (const eachPack of Object.keys(packageObject)) {
        packageObject[eachPack].doTest({ [eachPack]: packageObject[eachPack] })
    }

    // const {doTest:hybridsTest} = hybrids
    // const {doTest:unocssTest} = unocss

    // hybridsTest(hybrids)
    // unocssTest(unocss)
    /**
     * unocss + hybrids 
     */
    const { define, html } = hybrids
    define({
    tag: "h-w-u",
    name: 'world',
    content: ({ name }) => html`
        <div class="w-screen" un-bg="purple-${(Math.round(Math.random()*9)*100).toFixed(0)}">Hello ${name}</div>
    `,
    });

    var target = document.querySelector('#test')
 
    const HelloWorldElementUno = customElements.get("h-w-u")
    const el = new HelloWorldElementUno()
    el.setAttribute('name', 'Hybrids with unocss test:')
    target.appendChild(el)

    const el2 = new HelloWorldElementUno()
    el2.setAttribute('name', 'If these blocks are shades of purple, then hybrids with unocss is working !!')
    target.appendChild(el2)

    runOnce = true
}

export {doTest}