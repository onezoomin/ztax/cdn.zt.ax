#![allow(unused_imports)]
#![feature(is_some_and, async_closure, slice_pattern)]

use bygonz_rs::datalog::AppLog;
use bygonz_rs::datalog::Atom;
use bygonz_rs::datalog::DagAppLog;
use bygonz_rs::datalog::DagAtom;
use bygonz_rs::db::ApplogDB;
use bygonz_rs::ipfs;
use bygonz_rs::ipfs::IpfsDag;
use bygonz_rs::ipfs::IpfsOptions;
use bygonz_rs::ipfs::PublicationUpdate;
use bygonz_rs::Bygonz;
use core::fmt;
use db::RexieDB;
use futures_util::future::join_all;
use futures_util::stream;
use futures_util::stream::Collect;
use futures_util::Future;
use rand::{self, Rng};
use send_wrapper::SendWrapper;
use serde_json::Value;
use std::cell::RefCell;
use std::marker::PhantomData;
use std::panic;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::RwLock;
use std::thread::spawn;

use anyhow::{anyhow, bail, Context, Result};
use async_once_cell::{self, OnceCell};
use cid::Cid;
use futures_util::lock::Mutex;
use futures_util::StreamExt;
use futures_util::TryFutureExt;
use js_sys;
use log::{debug, error, info, warn, trace, Level};
use once_cell;
use serde::{Deserialize, Serialize};
use thiserror;
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::spawn_local;

use crate::db::create_rexie_db;
use crate::db::RexieDBConfig;
use crate::utils::ToJsError;

mod db;
mod utils;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen(typescript_custom_section)]
const TS_APPEND_CONTENT: &'static str = r#"
type Publication = { 
    name: string,
    query: string,
}
"#;

// #[wasm_bindgen]
// pub struct BygonzConfig {
//     pub db_name: String,
//     pub on_update: js_sys::Function,
// }
#[wasm_bindgen]
pub struct BygonzWasm {}
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Options {
    pub ipfs: Option<IpfsOptions>,
}

//TODO: cleanup those numerous attempts
// thread_local! {
// static BYGONZ: OnceCell<Arc<Mutex<Box<Bygonz>>>> = OnceCell::new();
// static BYGONZ: OnceCell<RwLock<Box<dyn ApplogDB + 'static>>> = OnceCell::new();
// static BYGONZ: OnceCell<Arc<Mutex<Box<Bygonz<'static>>>>> = OnceCell::new();
// static BYGONZ: OnceCell<Bygonz<'static>> = OnceCell::new();
// static BYGONZ: OnceCell<Arc<RwLock<Bygonz<'static>>>> = OnceCell::new();
// static BYGONZ: RefCell<Option<Box<Bygonz<'static>>>> = RefCell::new(None);
static BYGONZ: OnceCell<SendWrapper<Bygonz<'static>>> = OnceCell::new();
// }

static DB: OnceCell<RexieDB> = OnceCell::new();
// static IPFS: OnceCell<IpfsDag> = OnceCell::new();
// static MUTEX: once_cell::sync::OnceCell<Mutex<()>> = once_cell::sync::OnceCell::new();
// static INSTANCE: Bygonz = Bygonz {
//     db: OnceCell::new(),
//     ipfs: OnceCell::new(),
// };
// }
// fn get_bygonz() -> Option<&'static Box<Bygonz<'static>>> {
//     BYGONZ.with(|bygonz| {
//         if let Some(ref db) = *bygonz.borrow() {
//             Some(db)
//         } else {
//             None
//         }
//     })
// }

//? #[wasm_bindgen(start)]
#[wasm_bindgen]
pub async fn init_bygonz(db_name: String, options: JsValue) -> Result<BygonzWasm, JsValue> {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init_with_level(Level::Debug).expect("Failed to init logger");
    info!("🦀 Initializing Bygonz WASM");
    let options = serde_wasm_bindgen::from_value::<Options>(options)?; //.expect("options should deserialize to options schema");

    // if MUTEX.get().is_some() {
    //     return Err(JsError::new("Double initialization").into());
    // }
    // MUTEX.get_or_init(|| Mutex::new(()));

    let db = DB
        .get_or_init(async {
            create_rexie_db(RexieDBConfig { name: db_name })
                .await
                .expect("db init")
        })
        .await;
    // BYGONZ.with_async(|bygonz| {}).await;
    BYGONZ
        .get_or_init(async {
            SendWrapper::new(
                bygonz_rs::init_bygonz(bygonz_rs::Options { ipfs: options.ipfs }, db)
                    .await
                    .context("Init bygonz")
                    .expect("init_bygonz"),
            )
        })
        .await;
    // let bygonz_instance = bygonz_rs::init_bygonz(
    //     bygonz_rs::Options { ipfs: options.ipfs },
    //     &Arc::new(RwLock::new(db)),
    // )
    // .await
    // .context("Init bygonz")
    // .expect("init_bygonz");

    Ok(BygonzWasm {})
}

#[wasm_bindgen]
impl BygonzWasm {
    pub async fn get_applogs(&self) -> Result<JsValue, JsValue> {
        let atoms: Vec<AppLog> = vec![];
        Ok(serde_wasm_bindgen::to_value(&atoms)?) //TODO: consider https://github.com/rustwasm/wasm-bindgen/issues/111#issuecomment-527380864
    }

    pub async fn publish_applogs(&self, value: JsValue) -> Result<(), JsValue> {
        trace!("[publish_applogs] Received new applogs: {:#?}", value);
        // let lock = MUTEX.get().expect("mutex uninitialized").lock().await;
        let applogs: Vec<AppLog> = serde_wasm_bindgen::from_value(value)?; //.expect("value should deserialize to Array<AppLog>");
        debug!("Publishing new applogs: {:#?}", applogs);

        // let db = DB.get().expect("DB should be initialized");
        let bygonz = BYGONZ.get().expect("bygonz cell");
        bygonz
            .publish_applogs(applogs)
            .await
            .context("publish_applogs")
            .js_error()?;

        Ok(())
    }

    // #[wasm_bindgen(typescript_type = "Publication")]
    pub async fn init_publication(&mut self, pub_id: JsValue) -> Result<(), JsError> {
        let pub_id = pub_id.as_string().expect("pub_id is string");
        let bygonz = BYGONZ.get().expect("bygonz cell");
        bygonz
            .init_publication(&pub_id)
            .await
            .with_context(|| format!("init_publication {}", pub_id))
            .js_error()?;
        Ok(())
    }

    pub async fn init_subscription(
        &self,
        sub_id: JsValue,
        on_update: js_sys::Function,
    ) -> Result<(), JsValue> {
        let db = DB.get().expect("DB not initialized");
        let sub = db
            .get_sub(
                &sub_id
                    .as_string()
                    .expect(format!("sub_id is not a valid string: {:?}", sub_id).as_str()),
            )
            .await
            .expect("should be able to lookup sub in DB")
            .expect("should find sub in DB");
        debug!("Init subscription: {:#?}", sub);

        let bygonz = BYGONZ.get().expect("bygonz cell");

        let (rx, sub_job) = bygonz
            .init_subscription("1".into())
            .await
            .expect("Init subscription");

        spawn_local(sub_job);
        spawn_local(async move {
            loop {
                // debug!("Waiting for pubsub in wasm init_subscription");
                match rx.recv() {
                    Ok(_) => {
                        info!("SUB {} UPDATED, calling on_update", sub.ID);
                        on_update
                            .call0(&JsValue::UNDEFINED)
                            .expect("call on_update");
                    }
                    Err(err) => {
                        error!("Subscription update receive error {}", err);
                        break;
                    }
                }
            }
        });

        Ok(())
    }
}

// #[wasm_bindgen]
// #[derive(Debug)]
// pub struct BygonzError {
//     display: String,
// }
// impl std::error::Error for BygonzError {}

// impl fmt::Display for BygonzError {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         f.write_str(self.display.as_str())
//     }
// }
// impl From<anyhow::Error> for BygonzError {
//     fn from(value: anyhow::Error) -> Self {
//         BygonzError {
//             display: value.to_string(),
//         }
//     }
// }

// // #[wasm_bindgen]
// #[derive(thiserror::Error, Debug)]
// #[error(transparent)]
// pub struct BygonzError(#[from] anyhow::Error);
