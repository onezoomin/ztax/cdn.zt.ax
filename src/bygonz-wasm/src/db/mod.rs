use crate::utils::Anyhowable;
use anyhow::{anyhow, bail, ensure, Context, Error, Result};
use async_once_cell::{Lazy, OnceCell};
use async_trait::async_trait;
use bygonz_rs::{datalog::AppLog, db::ApplogDB, Publication, Subscription};
use cid::Cid;
use futures_util::{future::AbortHandle, future::FutureExt, join, Future, StreamExt, TryFutureExt};
use log::{debug, trace};
// use once_cell::sync::OnceCell;
// use anyhow::{Context, Result};
use gloo_utils::format::JsValueSerdeExt;
use rexie::{Index, ObjectStore, Rexie, Transaction, TransactionMode};
use serde::{de::DeserializeOwned, Deserialize};
use serde_json::Value;
use std::{
    borrow::BorrowMut,
    collections::HashMap,
    fmt::{Debug, Display},
    ops::Deref,
    pin::Pin,
    sync::{atomic, Arc, Mutex, MutexGuard},
};
use wasm_bindgen::JsValue;

const LOG_TABLE: &str = "AppLogs";
const SUBS_TABLE: &str = "Subscriptions";
const PUBS_TABLE: &str = "Publications";

pub struct RexieDBConfig {
    pub name: String,
}

// thread_local! {
pub struct RexieDB {
    db_name: String, // rexie: OnceCell<Rexie>,
}
// }

// pub struct SafeRexie {
//     rexie: Rexie,
// }
// impl Debug for SafeRexie {
//     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
//         f.write_str("SAFE_REXIE")
//     }
// }

// thread_local! {
// static SAFE_REXIE: OnceCell<Arc<Mutex<SafeRexie>>> = OnceCell::new();
// unsafe impl Send for SafeRexie {} // HACK after 3 hours of trying... - from https://users.rust-lang.org/t/solved-how-to-move-non-send-between-threads-or-an-alternative/19928/5

pub async fn create_rexie_db(config: RexieDBConfig) -> Result<RexieDB> {
    // let instance = DB {
    //     rexie: OnceCell::new(),
    // };
    // instance.
    /* REXIE
    .with(async move |db_cell| { */
    // SAFE_REXIE
    //     // .lock()
    //     // .expect("should aquire lock on SAFE_REXIE")
    //     .get_or_try_init::<SafeRexie>(async {
    //         let rexie = Rexie::builder(&config.name)
    //             // Set the version of the database to 1.0
    //             // .version(10) // actually set by Dexie, we just copied
    //             // .add_object_store(
    //             //     ObjectStore::new(LOG_TABLE)
    //             //         .key_path("ha") // ≈ primary key
    //             //         // Ensure indexes exist
    //             //         .add_index(Index::new("ha", "ha").unique(true))
    //             //         .add_index(Index::new("tx", "tx")),
    //             // )
    //             // Build the database
    //             .build()
    //             .await
    //             .expect("should build rexie");

    //         Ok(Arc::pin(Mutex::new(SafeRexie { rexie })))
    //     })
    //     .await
    //     .expect("should init rexie");
    /* ()
    })
    .await; */
    // static INSTANCE: OnceCell<Result<Rexie>> = Lazy::new(async move {
    //     Mutex::new(DB {
    //     });
    // });

    Ok(RexieDB {
        db_name: config.name,
    })
    // unimplemented!()
}

impl RexieDB {
    async fn get_rexie(&self) -> Result<Rexie> {
        Ok(Rexie::builder(&self.db_name)
            // Set the version of the database to 1.0
            // .version(10) // actually set by Dexie, we just copied
            // .add_object_store(
            //     ObjectStore::new(LOG_TABLE)
            //         .key_path("ha") // ≈ primary key
            //         // Ensure indexes exist
            //         .add_index(Index::new("ha", "ha").unique(true))
            //         .add_index(Index::new("tx", "tx")),
            // )
            .add_object_store(
                ObjectStore::new(PUBS_TABLE)
                    .key_path("ID") // ≈ primary key
                    // Ensure indexes exist
                    .add_index(Index::new("name", "name").unique(true)),
            )
            // Build the database
            .build()
            .await
            .expect("should build rexie"))
    }

    async fn read_trx<'func_life, T, F, RFuture, FuncReturn>(
        &self,
        tables: &[T],
        trx_func: F,
    ) -> Result<FuncReturn, Error>
    where
        T: AsRef<str>,
        RFuture: 'func_life + Future<Output = Result<FuncReturn, Error>>,
        F: 'func_life + FnOnce(/* &'func_life  */ Transaction) -> RFuture,
    {
        let rexie = self.get_rexie().await?;
        open_trx(&rexie, tables, TransactionMode::ReadOnly, trx_func).await
    }
    async fn get_sub_raw<T>(&self, sub_id: &str) -> Result<Option<T>>
    where
        T: ?Sized + DeserializeOwned,
    {
        trace!("Getting sub state for '{:?}'", sub_id);

        let sub_id_for_context = sub_id.clone();
        self.read_trx(&[SUBS_TABLE], async move |trx| {
            let sub_store = trx
                .store(SUBS_TABLE)
                .expect(format!("should open store '{}'", SUBS_TABLE).as_str());

            // let sub_in_db = sub_store
            //     .get(&JsValue::from(subl.id))
            //     .await
            //     .to_anyhow(format!("get sub '{}' from db", name).as_str())?;
            // debug!("Get sub? {:?}", sub_in_db);

            let sub = sub_store
                .get(&JsValue::from(sub_id))
                .await
                .to_anyhow("Get sub from db")?;
            trace!("Found sub: {:#?}", sub);

            Ok(if sub.is_undefined() {
                None
            } else {
                let subl: T = sub
                    .into_serde()
                    .with_context(|| format!("deserialize sub: {:?}", sub))?;
                Some(subl)
            })
        })
        .await
        .with_context(|| format!("get_sub transaction for {:?}", sub_id_for_context))
    }
}

#[async_trait(?Send)]
impl ApplogDB for RexieDB {
    async fn get_pub(&self, publ: Option<&str>) -> Result<Option<Publication>> {
        trace!("Getting pub state for '{:?}'", publ);

        self.read_trx(&[PUBS_TABLE], async move |trx| {
            let pub_store = trx
                .store(PUBS_TABLE)
                .expect(format!("should open store '{}'", PUBS_TABLE).as_str());

            // let pub_in_db = pub_store
            //     .get(&JsValue::from(name))
            //     .await
            //     .to_anyhow(format!("get pub '{}' from db", name).as_str())?;
            // debug!("Get pub? {:?}", pub_in_db);

            let all_pubs = pub_store
                .get_all(
                    None,
                    Some(2 /* enough to know there's more than 1 */),
                    None,
                    None,
                )
                .await
                .map_err(|err| anyhow!("Test: {:?}", err))?;
            // .to_anyhow("get all pubs")?;
            ensure!(
                all_pubs.len() == 1,
                "{} pubscriptions found",
                all_pubs.len()
            );
            let first = all_pubs
                .get(0)
                .expect("get(0) failed after ensuring len() == 1");
            let (key, publ) = first;
            trace!("Found pub: #{:?} {:#?}", key, publ);

            Ok(if publ.is_undefined() {
                None
            } else {
                let publ: Publication = publ
                    .into_serde()
                    .with_context(|| format!("deserialize pub '{:?}'", publ))?;
                Some(publ)
            })
        })
        .await
        .with_context(|| format!("get_pub transaction for {:?}", publ))
    }

    async fn get_sub(&self, sub_id: &str) -> Result<Option<Subscription>> {
        self.get_sub_raw(sub_id).await
    }

    async fn update_pub(
        &self,
        pub_id: &str,
        updater: Box<dyn FnOnce(HashMap<String, Value>) -> HashMap<String, Value>>,
    ) -> Result<()> {
        debug!("Updating pub '{}'", pub_id);
        let rexie = self.get_rexie().await?;

        open_trx(
            &rexie,
            &[PUBS_TABLE],
            TransactionMode::ReadWrite,
            async move |trx| {
                let result = async {
                    let pub_store = trx
                        .store(PUBS_TABLE)
                        .expect(format!("should open store '{}'", PUBS_TABLE).as_str());
                    let all_pubs = pub_store
                        .get_all(
                            None,
                            Some(2 /* = enough to know there's more than 1 */),
                            None,
                            None,
                        )
                        .await
                        .map_err(|err| anyhow!("Test: {:?}", err))?;
                    // .to_anyhow("get all pubs")?;
                    ensure!(
                        all_pubs.len() == 1,
                        "{} pubscriptions found",
                        all_pubs.len()
                    );
                    let first = all_pubs
                        .get(0)
                        .expect("get(0) failed after ensuring len() == 1");
                    let (key, pub_encoded) = first;

                    trace!("Found pub: #{:?} {:#?}", key, pub_encoded);

                    let mut publ: HashMap<String, Value> =
                        JsValueSerdeExt::into_serde(pub_encoded)?; //HACK didn't figure out how to add key to Value without JsValue type
                    trace!("Deserialized pub: {:#?}", publ);
                    ensure!(
                        publ.get("ID").is_some_and(|id| id == pub_id),
                        "DB pub has different ID: {:?} != {}",
                        publ.get("ID"),
                        pub_id
                    );
                    publ = updater(publ);

                    let value = serde_json::to_value(&publ)?;
                    pub_store
                        .put(&JsValue::from_serde(&value)?, None)
                        .await
                        .map_err(|err| anyhow!("Test: {:?}", err))?;
                    // .to_anyhow("re-encode pub")?;
                    Ok(())
                }
                .await;
                match &result {
                    Ok(_) => close_trx(trx, TrxAction::Commit).await,
                    Err(_) => close_trx(trx, TrxAction::Abort).await,
                }?;
                result
            },
        )
        .await
        .context("update_pub transaction")?;
        Ok(())
    }

    async fn update_sub(
        &self,
        sub_id: &str,
        updater: Box<dyn FnOnce(HashMap<String, Value>) -> HashMap<String, Value>>,
        // updater: Box<dyn FnOnce(HashMap<String, Value>) -> HashMap<String, Value> + 'a>,
    ) -> Result<()> {
        debug!("Updating sub '{}'", sub_id);
        let rexie = self.get_rexie().await?;

        let sub_id_2 = sub_id.clone();
        open_trx(
            &rexie,
            &[SUBS_TABLE],
            TransactionMode::ReadWrite,
            async move |trx| {
                let sub_id_2 = sub_id_2.clone();
                let result = async {
                    let sub_store = trx
                        .store(SUBS_TABLE)
                        .expect(format!("should open store '{}'", SUBS_TABLE).as_str());

                    //HACK: Used HashMap as I didn't figure out how to add key to Value without JsValue type

                    let sub = sub_store
                        .get(&JsValue::from(sub_id_2.clone()))
                        .await
                        .to_anyhow("Get sub from db")?;
                    trace!("Found sub: {:#?}", sub);

                    let mut sub: HashMap<String, Value> = if sub.is_undefined() {
                        bail!("Did not find sub to update positon: {}", sub_id_2)
                    } else {
                        sub.into_serde()
                            .with_context(|| format!("deserialize sub: {:?}", sub))?
                    };
                    trace!("Deserialized sub: {:#?}", sub);
                    sub = updater(sub);

                    let value = serde_json::to_value(&sub)?;
                    sub_store
                        .put(&JsValue::from_serde(&value)?, None)
                        .await
                        .map_err(|err| anyhow!("Test: {:?}", err))?;
                    // .to_anyhow("re-encode sub")?;
                    Ok(())
                }
                .await;
                match &result {
                    Ok(_) => close_trx(trx, TrxAction::Commit).await,
                    Err(_) => close_trx(trx, TrxAction::Abort).await,
                }?;
                result
            },
        )
        .await
        .context("update_sub transaction")?;
        Ok(())
    }

    async fn save_applogs(&self, applogs: &Vec<AppLog>) -> Result<Vec<AppLog>> {
        debug!("Saving to DB: {:?}", applogs.len());
        let rexie = self.get_rexie().await?;

        let inserted = open_trx(
            &rexie,
            &[LOG_TABLE],
            TransactionMode::ReadWrite,
            async move |trx| {
                let result = async {
                    let log_store = trx
                        .store(LOG_TABLE)
                        .expect(format!("should open store '{}'", LOG_TABLE).as_str());
                    let mut inserted: Vec<AppLog> = vec![];
                    for applog in applogs {
                        trace!("Checking applog {:?}", applog);
                        let hash = serde_wasm_bindgen::to_value(&applog.ha)
                            .expect("should get 'ha' from serialized applog");
                        trace!("Checking applog #{:?} {:?}", hash, applog);
                        let result = log_store
                            .get(&hash)
                            .await
                            .expect("should be able to check logstore if applog exists");
                        if result.is_undefined() {
                            trace!("Need to insert: {}", applog.ha);
                            let serialized = serde_wasm_bindgen::to_value(applog)
                                .expect("should serialize applog to JsValue");
                            trace!("Serialized: {:?}", serialized);
                            let id = log_store
                                .add(&serialized, None)
                                .await
                                .expect("should insert applog to IDB");
                            debug!("Inserted: {:?}", id);
                            inserted.push(applog.clone());
                        } else {
                            trace!("Already in DB?! {:?}" /*, applog.ha */, result)
                        }
                    }
                    Ok(inserted)
                }
                .await;
                match &result {
                    Ok(_) => close_trx(trx, TrxAction::Commit).await,
                    Err(_) => close_trx(trx, TrxAction::Abort).await,
                }?;
                result
            },
        )
        .await
        .context("save transaction")?;
        Ok(inserted)
    }
}

// fn single<T>(name: &str, vec: Vec<T>) -> Result<T>
// where
//     T: Copy,
// {
//     ensure!(vec.len() == 1, "{} {}s found", vec.len(), name);
//     Ok(*vec.get(0).expect("get(0) failed after ensuring len() == 1"))
// }

async fn open_trx<'func_life, T, F, RFuture, FuncReturn>(
    rexie: &Rexie,
    tables: &[T],
    mode: TransactionMode,
    trx_func: F,
) -> Result<FuncReturn, Error>
where
    T: AsRef<str>,
    RFuture: 'func_life + Future<Output = Result<FuncReturn, Error>>,
    F: 'func_life + FnOnce(/* &'func_life  */ Transaction) -> RFuture,
{
    let trx = rexie.transaction(tables, mode);
    match trx {
        Err(err) => Err(anyhow!("Failed to open transaction: {:?}", err)),
        Ok(trx) => {
            async move {
                let trx: Transaction = trx;
                let result = trx_func(/* & */ trx)
                    /* .then(async move |result| {
                        let trx_end_result = match result {
                            Ok(_) => trx.done().await,
                            Err(_) => trx.abort().await,
                        };
                        if let Err(err) = trx_end_result {
                            bail!(
                                "Failed to {} transaction: {:?}",
                                if result.is_err() { "abort" } else { "commit" },
                                err
                            )
                        };
                        result
                    }) */
                    .await;
                result
            }
            .await
        }
    }
}

#[derive(Debug)]
enum TrxAction {
    Abort,
    Commit,
}

async fn close_trx(trx: Transaction, action: TrxAction) -> Result<()> {
    let trx_end_result = match &action {
        TrxAction::Commit => trx.done().await,
        TrxAction::Abort => trx.abort().await,
    };
    if let Err(err) = trx_end_result {
        bail!("Failed to {:?} transaction: {:?}", action, err)
    }
    Ok(())
}

// fn rexie_mutex() -> std::sync::MutexGuard<'static, OnceCell<SafeRexie>> {
//     let rexie_mutex = SAFE_REXIE;
//     // .lock()
//     // .expect("should aquire mutex on SAFE_REXIE");
//     rexie_mutex.get().expect("DB not initialized")
// }
// fn get_rexie<'life>(rexie_mutex: &'life MutexGuard<'static, OnceCell<SafeRexie>>) -> &'life Rexie {
//     let rexie = &rexie_mutex.get().expect("DB not initialized").rexie;
//     rexie
// }

impl<Return> Anyhowable<Return> for Result<Return, rexie::Error> {
    fn to_anyhow(self, action: &str) -> Result<Return, anyhow::Error> {
        // self.map_err(|err| anyhow!("Failed to {}: {:?}", action, err))
        match self {
            Ok(result) => Ok(result),
            Err(err) => Err(anyhow!("Failed to {}: {:?}", action, err)), //TODO: is Error::msg better?
        }
    }
}
