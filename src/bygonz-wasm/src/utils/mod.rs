use anyhow::{anyhow, Result};
use std::fmt::{Debug, Display};
use wasm_bindgen::JsError;

// pub fn map_vec_try_into<'input, I: 'input, O>(vec: &'input Vec<I>) -> Result<Vec<O>>
// where
//     // I: Into<O> - same as:
//     O: TryFrom<&'input I>,
// {
//     let iter_map = vec.into_iter().map(|item| item.try_into()?);
//     iter_map.collect()
// }

pub trait Anyhowable<R> {
    fn to_anyhow(self, msg: &str) -> Result<R, anyhow::Error>;
}
impl<R> Anyhowable<R> for Result<R, serde_wasm_bindgen::Error> {
    fn to_anyhow(self, action: &str) -> Result<R, anyhow::Error> {
        match self {
            Ok(result) => Ok(result),
            Err(err) => Err(anyhow!("Failed to {}: {:?}", action, err)), //TODO: is Error::msg better?
        }
    }
}

pub trait ToJsError<R> {
    fn js_error(self) -> Result<R, JsError>;
}
impl<R> ToJsError<R> for Result<R, anyhow::Error> {
    fn js_error(self) -> Result<R, JsError> {
        match self {
            Ok(result) => Ok(result),
            Err(err) => Err(JsError::new(err.to_string().as_str())),
        }
    }
}
