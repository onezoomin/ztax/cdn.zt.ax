let runOnce
const doTest = ({ unocss }) => {
  if (runOnce) return console.warn('already runOnce')

  /**
     * initUnocssRuntime
     */
  const { initUnocssRuntime, presetMini, presetAttributify, presetIcons, injectTailwindCSSreset } = unocss
  injectTailwindCSSreset()

  const unocssConfig = {
    defaults: {
      // rules: [
      //     // custom rules...
      // ],
      presets: [
        presetMini(),
        presetAttributify({
          /* options  https://github.com/unocss/unocss/tree/main/packages/preset-attributify */
          prefix: 'un-',
        }),
      ],
      // ...
    },
  }
  initUnocssRuntime(unocssConfig)
  // console.log(initResult,presetAttributify,initUnocssRuntime)

  const target = document.querySelector('#test')
  target.insertAdjacentHTML('beforeend', '<div class="w-1/2 m-5 p-3" flex="~ row" bg="teal-400"><div>unocss works if this is a half width teal div</div></div>')
  setTimeout(() => {
    unocssConfig.defaults.presets.push(presetIcons({
      customizations: {
        trimCustomSvg: true,
      },
      collections: {
        'logos-atomic': async (iconName) => {
          // your custom loader here. Do whatever you want.
          // for example, fetch from a remote server:
          const iconURL = `https://f.cdn.zt.ax/@iconify-json-atomic/logos/atomic/${iconName}.json`
          const fetched = await fetch(iconURL)
          const textVal = await fetched.text()
          console.log({ iconURL, textVal })
          const { body } = JSON.parse(textVal)
          console.log({ body })

          return `<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" viewBox="0 0 256 256" width="1em" height="1em">
            ${body}
          </svg>`
        },
      },
    }))
    initUnocssRuntime(unocssConfig)
    target.insertAdjacentHTML('beforeend', '<div class="w-1/2 m-5 p-3" flex="~ row" bg="teal-300"><div class="i-logos-atomic-unocss text-3xl mr-6"></div><div>icon from cdn atomic</div><div class="i-logos-atomic-figma text-3xl ml-6"></div><div class="i-logos-atomic-html-5 text-xl ml-6"></div></div>')
  })

  setTimeout(() => {
    unocssConfig.defaults.presets.push(presetIcons({
      cdn: 'https://esm.sh/',
    }))
    initUnocssRuntime(unocssConfig)
    target.insertAdjacentHTML('beforeend', '<div class="w-1/2 m-5 p-3" flex="~ row" bg="teal-200"><div class="i-logos-unocss text-2xl mr-6"></div><div>icon from cdn</div></div>')
  })
  runOnce = true
}

export { doTest }
