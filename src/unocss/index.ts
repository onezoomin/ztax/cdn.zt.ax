
import initUnocssRuntime from '@unocss/runtime' // const unocss = require('@unocss/runtime/mini.global.js')
import presetAttributify from '@unocss/preset-attributify'
import presetIcons from '@unocss/preset-icons/browser'
import transformerDirective from '@unocss/transformer-directives'

// used to break build cuz cant find @unocss/preset-mini/utils - seems fixed as of 0.45.12
import presetUno from '@unocss/preset-uno'
import presetMini from '@unocss/preset-mini'

import presetDaisy from 'unocss-preset-daisy'

import tailwindCSSreset from './tailwind.min.css' // .css will be inlined via esbuild
// import daisyCSS from '@kidonng/daisyui/index.css'

import { doTest } from './test.js'

const daisyCSS = 'https://cdn.jsdelivr.net/npm/@kidonng/daisyui@1.7.0/full.css'

const injectTailwindCSSreset = () => {
  document.head.innerHTML += `<link id="tw-reset" rel="stylesheet" href="${tailwindCSSreset as string}" type="text/css"/>`
}
const injectDaisyUnoCSS = () => {
  document.head.innerHTML += `<link id="daisy-uno" rel="stylesheet" href="${daisyCSS as string}" type="text/css"/>`
}

const injectDaisyCDNCSS = () => {
  document.head.innerHTML += '<link id="daisy-cdn" rel="stylesheet" href="https://cdn.jsdelivr.net/npm/daisyui@2.24.0/dist/full.css" type="text/css"/>'
}

const unocss = { initUnocssRuntime, presetAttributify, presetUno, presetMini, presetIcons, transformerDirective, injectTailwindCSSreset, presetDaisy, injectDaisyUnoCSS, injectDaisyCDNCSS, doTest }

// console.debug({ unocss, doTest })
export { unocss, doTest }
