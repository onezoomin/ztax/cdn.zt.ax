Please Feel welcome to join our collective endeavor by:
- Opening Issues
- Creating PRs
- Engaging in RFCs

We are in the early formation phase of setting up ztax as a collective. 

Part of the vision is to arrange for funding opportunities for all contributors, 
so that we can all be compensated for writing the code that we anyway want to write.

We are not hiring, but we want to find reliable collaborators and find enough money
that you can all quit your annoying PHP jobs and write amazing modern code with us.
