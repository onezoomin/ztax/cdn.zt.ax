{
  description = "virtual environments";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
  };

  outputs = { self, flake-utils, devshell, nixpkgs, rust-overlay }:
    flake-utils.lib.eachDefaultSystem (system: {
      devShell =
        let
          pkgs = import nixpkgs {
            inherit system;

            overlays = [
              (import rust-overlay)
              (final: prev: {
                nodejs = prev.nodejs-18_x; # <== Set desired node version here
              })
              devshell.overlays.default
            ];
          };
          rust = pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
            extensions = [ "rust-src" ];
            targets = [ "wasm32-unknown-unknown" ];
          });
        in
        pkgs.devshell.mkShell {
          devshell.packages = with pkgs; [
            go-task
            nodejs
            gcc
            nodePackages.pnpm
            nodePackages.typescript-language-server
            dprint
            rust
            wasm-pack
            cargo-edit
            cargo-watch
            wasm-bindgen-cli
            # TODO: replace with cargo-run-bin
            protobuf
            # protobuf-codegen
            sccache

            nixpkgs-fmt
            watchexec
          ];
        };
    });
}
