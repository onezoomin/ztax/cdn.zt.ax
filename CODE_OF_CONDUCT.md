# Code of Conduct

## Purpose
To make far edge fully decentralized tech available to developers and users with approachable DX patterns and enjoyable UX.

## Inclusion
Onezoomin/ztax collective aims to be inclusive to the largest number of people. We are committed to providing a friendly, safe and welcoming environment for all, regardless of gender, sexual orientation, ability, ethnicity, socioeconomic status and religion (or lack thereof).

## Scope

This Code of Conduct applies within all community spaces, and also applies when an individual is officially representing the community in public spaces. Examples of representing our community include using an official e-mail address, posting via an official social media account, or acting as an appointed representative at an online or offline event.

## Policy

- Please respect each other. Do not dismiss, abuse, harass, attack, insult, or discriminate against others.
- Likewise, any spamming, trolling, flaming, baiting or other attention-stealing behavior is not welcome.
- Respect that people have differences of opinion and that every design or implementation choice carries a trade-off and numerous costs. There is seldom a right answer.
- If you feel being harassed or made uncomfortable by a community member, please report the incident(s) either by contacting the team members on GitHub, or GitLab. We will work with you to resolve the issue promptly.
