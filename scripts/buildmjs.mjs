// import path from 'path'
import fs from 'fs'
import { build } from 'esbuild'
import { COMMON_OPTS } from './common.mjs'
import { warn } from 'console'
// import { NodeModulesPolyfillPlugin } from '@esbuild-plugins/node-modules-polyfill'
// import InlineWorkerPlugin from 'esbuild-plugin-inline-worker'
// import { NodeGlobalsPolyfillPlugin } from '@esbuild-plugins/node-globals-polyfill'
// import gzipPlugin from '@luncheon/esbuild-plugin-gzip'

const commonMin = {
  ...COMMON_OPTS,
  // write: false, // write must be false for gzipPlugin
}

const commonMjs = {
  ...COMMON_OPTS,
}

/** @type {import("esbuild").BuildOptions} */
// const inlineWorkerConfig = {
//   target: 'esnext',
//   sourcemap: 'both',

//   // loader: {
//   //   '.wasm': 'dataurl',
//   // },

//   plugins: [
//     NodeModulesPolyfillPlugin(),

//     NodeGlobalsPolyfillPlugin({
//       // process: true,
//       buffer: true,
//       define: {
//         // 'process.env.var': '"hello"',
//         window: globalThis,
//       }, // inject will override define, to keep env vars you must also pass define here https://github.com/evanw/esbuild/issues/660
//     }),
//   ],
// }
// commonMin.plugins.push(InlineWorkerPlugin(inlineWorkerConfig))
// commonMin.plugins.push(gzipPlugin())
// const inlineWorkerConfigMjs = {
//   ...inlineWorkerConfig,
//   minify: false,
// }
// commonMjs.plugins.push(InlineWorkerPlugin(inlineWorkerConfigMjs))

const buildMinMjs = function buildMinMjs (entryPoints) {
  if (!entryPoints?.length) console.warn('no good array', process.exit(1))

  return build({
    entryPoints,
    ...commonMin,
    entryNames: '[dir]/[name].min',
    minify: true,
  }).catch((e) => warn('minifying error', e)) // process.exit(1)
}

const buildMjs = function buildMjs (entryPoints, skipMin = false) {
  if (!entryPoints?.length) console.warn('no good array', process.exit(1))
  const promises = []
  skipMin || promises.push(buildMinMjs(entryPoints))
  promises.push(build({
    entryPoints,
    ...commonMjs,
  }).catch((e) => warn('building error', e))) // process.exit(1)
  return promises
}
const myArgs = process.argv
// console.log(myArgs)
const entryPoints = myArgs.slice(2)
// && path.basename(myArgs[1]) === path.basename(__filename)
if (entryPoints?.length) {
  const baseMod = entryPoints[0].split('/')[1]
  void Promise.all(buildMjs(entryPoints, process.env.SKIP_MIN)).then(results => results.forEach(eachRes => {
    eachRes?.metafile && fs.writeFileSync(`dist/meta_${baseMod}.json`, JSON.stringify(eachRes.metafile))
    eachRes?.metafile && delete eachRes.metafile // remove the long metafile info after writing it to disk
    console.log(eachRes)
  }))
}

export { buildMjs, buildMinMjs }
