
import { build, serve } from 'esbuild'
import { sh } from './common.mjs'

import format from 'date-fns/fp/format'
import InlineWorkerPlugin from 'esbuild-plugin-inline-worker'
// import { format } from 'date-fns/esm/fp'
const inlineWorkerConfig = {
  target: 'esnext',
  minify: false,
}

// TODO consider pure esbuild solution rather than create serve - eg https://github.com/evanw/esbuild/issues/802#issuecomment-819578182
const timeNow = () => format('HH:mm:ss')(new Date())
const color = ['\x1b[32m', '\x1b[33m']
let altColor

export const isWatch = process.argv.includes('-w')
const esbuildServe = async (serveOptions = {}, buildOptions = {}) => {
  build({
    ...buildOptions,
    watch: isWatch && {
      onRebuild (err) {
        // serve.update()
        err ? console.error('\x1b[31m%s\x1b[0m', '× Failed') : console.log(altColor = color.shift(), '✓ Updated', timeNow()) || color.push(altColor) // blatently abusive one-liner
      },
    },
  })
    .then(eachRes => {
      eachRes?.metafile && delete eachRes?.metafile
      eachRes && console.log(eachRes)

      serve(
        serveOptions,
        {
          ...buildOptions,
        },
      ).catch((e) => console.warn('serve error', e, process.exit(1)))
      return eachRes
    })
    .catch((e) => console.warn('build error', e, process.exit(1)))
}

const myArgs = process.argv
// console.log(myArgs)
let entryPoints = myArgs.slice(isWatch ? 3 : 2)
if (!entryPoints?.length) console.warn('no good array:', entryPoints, 'defaulting to:', entryPoints = ['apps/color-schemes/index.mjs'])
const [base, dirName] = entryPoints[0].split('/') ?? ['apps', 'color-schemes']
sh(`mkdir -p dist/apps/${dirName}`)
sh(`cp apps/${dirName}/*.html dist/apps/${dirName}/`)
import('./common.mjs').then(({ COMMON_OPTS }) => {
  COMMON_OPTS.plugins.push(InlineWorkerPlugin(inlineWorkerConfig))
  const builldOpt = {
    entryPoints,
    ...COMMON_OPTS,
    logLevel: 'info',
    outdir: `dist${base === 'apps' ? '/apps' : ''}`,
    outbase: base,
  }
  console.log(builldOpt)
  esbuildServe(
    {
      servedir: 'dist',
      port: 7000,
    }, builldOpt,
  )
    .then((eachRes) => {
      eachRes?.metafile && delete eachRes.metafile
      eachRes && console.log(eachRes)
    })
    .catch((e) => console.log(e))
}).catch((e) => console.log(e))
