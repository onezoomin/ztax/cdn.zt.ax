const { serve } = require('esbuild')
const { buildAppMjs } = require('./buildapp')
const { COMMON_OPTS } = require('./common')

const extraOptionDefaults = {
  watch: {
    onRebuild (error, result) {
      if (error) console.error('watch build failed:', error)
      else console.log('watch build succeeded:', result)
    },
  },
}
const serveMjs = function serveMjs (entryPoints, extraOptions = extraOptionDefaults) {
  if (!entryPoints?.length) console.warn('no good array', process.exit(1))

  // sh(`esbuild ./src/${dirName}/index.js --bundle --outdir=./dist --outbase=./src --format=esm --loader:.css=dataurl --out-extension:.js=.mjs`)
  return serve({
    servedir: 'dist',
    port: 8080,
    onRequest: console.log,
  }, {
    entryPoints,
    ...COMMON_OPTS,
    outdir: 'dist/apps',
    outbase: 'apps',
    // ...extraOptions,
  })
}

const myArgs = process.argv
// console.log(myArgs)
const entryPoints = myArgs.slice(2)
if (entryPoints?.length) {
  // buildAppMjs(entryPoints, false, extraOptionDefaults)
  //   .then(result => {
  //     console.log('watching...', result)
  //   })
  //   .catch(() => process.exit(1))
  serveMjs(entryPoints)
    .then(server => {
      // server.stop()  // Call "stop" on the web server to stop serving
      console.log(server, `Server started for entryPoints:\n\n${entryPoints.join('\n')}`)
    })
    .catch(() => process.exit(1))
}
module.exports = { serveMjs }
