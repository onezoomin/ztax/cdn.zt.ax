import fs from 'fs'
import { sh } from './common.mjs'

import { buildMjs } from './buildmjs.mjs'
import { buildAppMjs } from './buildapp.mjs'
import { log } from 'console'

// Cleanup
sh('rm -rf dist/')

const startPath = sh('pwd')
log(startPath)

const srcdir = fs.readdirSync('./src', { withFileTypes: true })
  .filter(dirent => dirent.isDirectory())

// **********
// for each directory use build strategy in this priority:
// 1. build.mjs -> run `node build.mjs`
// 2. build.pnpm exists -> run `pnpm build`
// 3. run the default buildMjs with common options
for (const dirent of srcdir) {
  const { name: dirName } = dirent
  const distPath = `./dist/${dirName}`
  const srcPath = `./src/${dirName}`
  sh(`mkdir -p "${distPath}"`)
  const eachDirLs = fs.readdirSync(srcPath)

  if (eachDirLs.includes('build.mjs')) {
    sh(`cd ${srcPath} && node ./build.mjs && cd ${startPath}`)
  } else if (eachDirLs.includes('build.pnpm')) {
    sh(`cd ${srcPath} && pnpm build && cd ${startPath}`)
  } else {
    // await Promise.all(buildMjs([`./src/${dirName}/index.js`]))
    buildMjs([`./src/${dirName}/index.js`])
    sh('pwd')
    // sh(`cp "./src/${dirName}/index.html" "./dist/${dirName}/index.html" || cp "./static/index.html" "./dist/${dirName}/index.html"`)
  }
  sh(`cp ./src/${dirName}/*.html ./dist/${dirName}/ || cp ./static/*.html ./dist/${dirName}/`)
}

const appdir = fs.readdirSync('./apps', { withFileTypes: true })
  .filter(dirent => dirent.isDirectory())

for (const dirent of appdir) {
  const { name: dirName } = dirent
  const eachDirLs = fs.readdirSync(`./apps/${dirName}`)
  // sh(`rm -rf "./dist/apps/${dirName}"`)
  if (eachDirLs.includes('build.pnpm')) {
    sh(`cd apps/${dirName} && pnpm build`)
    sh(`mv apps/${dirName}/dist dist/apps/${dirName}`)
  } else {
    sh(`mkdir -p "./dist/apps/${dirName}"`)
    void buildAppMjs([`apps/${dirName}/index.mjs`])
    sh(`cp apps/${dirName}/*.html dist/apps/${dirName}/`)
  }
}

sh('cp static/*.png ./dist/')
sh('cp static/fav* ./dist/')
