import { NodeModulesPolyfillPlugin } from '@esbuild-plugins/node-modules-polyfill'
import { execSync } from 'child_process'
// import { polyfillNode } from 'esbuild-plugin-polyfill-node'
import EsmExternals from '@esbuild-plugins/esm-externals'
const { EsmExternalsPlugin } = EsmExternals
// import 'core-js/actual'

const sh = cmd => {
  console.log('->', cmd)
  const res = execSync(cmd).toString()
  console.log('-->', res)
  return res
}

const COMMON_OPTS = {
  bundle: true,
  logLevel: 'info', // 'verbose', 'debug', 'info', 'warning', 'error',
  format: 'esm',
  target: 'esnext',
  // sourcemap: 'both', // i.e. normal sidecar file and inline
  sourcemap: true, // i.e. only sidecar file
  metafile: true,
  outdir: 'dist',
  outbase: './',
  loader: {
    '.css': 'dataurl',
    '.wasm': 'file',
    // '.wjs': 'file',
    // '.wjs': 'dataurl',
    // '.svg': 'text',
  },
  // outbase: './src',
  outExtension: {
    '.js': '.mjs',
    // '.wjs': '.js',
  },
  plugins: [
    NodeModulesPolyfillPlugin(),
    // polyfillNode({
    //  // Options (optional)
    // }),
    EsmExternalsPlugin({ externals: ['webnative/crypto/browser'] }),
  ],
}

export { COMMON_OPTS, sh }
