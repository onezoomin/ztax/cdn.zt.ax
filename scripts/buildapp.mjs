import path from 'path'
import { build } from 'esbuild'
import { COMMON_OPTS } from './common.mjs'

const buildAppMinMjs = function buildAppMinMjs (entryPoints, extraOptions = {}) {
  if (!entryPoints?.length) console.warn('no good array', process.exit(1))

  build({
    entryPoints,
    ...COMMON_OPTS,
    outdir: './dist/apps',
    outbase: './apps',
    entryNames: '[dir]/[name].min',
    minify: true,
    sourcemap: true,
    ...extraOptions,
  }).catch(() => process.exit(1))
}

const buildAppMjs = function buildAppMjs (entryPoints, skipMin = false, extraOptions = {}) {
  if (!entryPoints?.length) console.warn('no good array', process.exit(1))
  console.log('building', { entryPoints, extraOptions })

  skipMin || buildAppMinMjs(entryPoints, extraOptions)
  return build({
    entryPoints,
    ...COMMON_OPTS,
    outdir: './dist/apps',
    outbase: './apps',
    ...extraOptions,
  })
}

const myArgs = process.argv
// console.log(myArgs)
const entryPoints = myArgs.slice(2)

if (entryPoints?.length && path.basename(myArgs[1]) === path.basename(__filename)) buildAppMjs(entryPoints).catch(() => process.exit(1))

export { buildAppMjs, buildAppMinMjs }
